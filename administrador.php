<!DOCTYPE html>
<html>
<head>
	<title>ADMINISTRADOR</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!-- No escala -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!--<link rel="icon" href="../../favicon.ico">-->
	<!-- Bootstrap core CSS -->
	<link href="lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="lib/bootstrap-checkbox-styled/build.css"/>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

	<!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">	
	<!--jquery-->
	<script src="lib/jquery/jquery-3.2.1.min.js"></script>	

	<!--controller-->
	<script type='text/javascript' src="js/administrador.js"></script>	

	<!--pagination-->
	<script src="lib/jquery/jquery-latest.js"></script>
	<script src="js/jquery.easyPaginate.js"></script>
		<!--overlay-->
	<script src="lib/jquery-loading-overlay-1.5.3/js/loadingoverlay.min.js"></script>
	<script src="lib/jquery-loading-overlay-1.5.3/js/loadingoverlay_progress.min.js"></script>
</head>

<body onload="first()">

	<!--NAV BAR-->
	<?php include('navbar.php') ?>
    <div class="nav-separador"></div> 
    <!-- -->    

    <div id="contenido">     
     <!--LOGO WORKCAFE-->
    <?php include('logobar.php') ?>
  	<!-- -->  
  			<div class="container">		
			<div>
				<h4 class="italic">Sucursales:</h4>
				<input hidden="true" type="text" name="privilegio" id="priv" value = <?php echo $_SESSION["PRIVILEGIOS"]?>> </input>
				<button style="visibility: hidden; float: right;" id="newSuc" type="submit" class="btn btn-success" onclick="gotoAddSuc()" >Agregar</a></button>
				<div class="cabecera-separator"></div>
				<br/>
				<div id="sucursales">
					<div class="listaCabecera">
						<div class="row">
							<div class='col-xs-2 col-xs-offset-1'>ID</div>
							<div class='col-xs-4'>Sucursal</div>
							<div class='col-xs-3'>Detalles</div>
							<div class='col-xs-2'></div>
						</div>
					</div>
					<div id="listaContainerSucursales"></div>
				</div>
			</div>
			<br/>
			<div>
				<h4 class="italic">Tecnologías:</h4>
				<button style="visibility: hidden; float: right;" id="newDisp" type="submit" class="btn btn-success" onclick="gotoAddDisp()" >Agregar</a></button>
				<div class="cabecera-separator"></div>
				<br/>
				<div id="dispositivos">
					<div class="listaCabecera">
						<div class="row">
							<div class='col-xs-2 col-xs-offset-1'>ID</div>
							<div class='col-xs-4'>Dispositivo</div>
							<div class='col-xs-3'>Detalles</div>
							<div class='col-xs-2'></div>
						</div>
					</div>
					<div id="listaContainerDispositivos"></div>
				</div>
			</div>
			<br/>
		</div>	
	</div>


	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>
	<script defer>
		if(document.getElementById("priv").value > 1){
            document.getElementById("newSuc").style = "visibility: visible; float:right;";
            document.getElementById("newDisp").style = "visibility: visible; float:right;";
            
        }

        function gotoAddSuc(){
        	window.location.href = 'agregarsucursal';
        }

        function gotoAddDisp(){
        	window.location.href = 'agregarDispositivo';
        }

		function first(){
			setTimeout(function() { paginateSucursales(); }, 100);
			setTimeout(function() { paginateDispositivos(); }, 100);
		}

		function paginateSucursales() {
			console.log("test1");
			$('#listaContainerSucursales').easyPaginate({
	        paginateElement: 'span',
	        elementsPerPage: 10,
	        effect: 'default',
	        elemId: '#listaContainerSucursales'
	    	});
		}

		function paginateDispositivos() {
			console.log("test1");
			$('#listaContainerDispositivos').easyPaginate({
	        paginateElement: 'span',
	        elementsPerPage: 10,
	        effect: 'default',
	        elemId: '#listaContainerDispositivos'
	    	});
		}

    </script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
</html>
