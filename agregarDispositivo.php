<!DOCTYPE html>
<?php 
error_reporting(0);
session_start();

?>
<html>
<head>
	<title>Agregar Dispositivo</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!-- No escala -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!--<link rel="icon" href="../../favicon.ico">-->
	<!-- Bootstrap core CSS -->
	<link href="lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">

	<!--plugin style css radio button checkboxes-->
	<link rel="stylesheet" href="lib/font-awesome-4.7.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="lib/bootstrap-checkbox-styled/build.css"/>

	<!-- Bootstrap core CSS -->
	<link href="lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="lib/bootstrap-checkbox-styled/build.css"/>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

	<!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">


	<script src="lib/jquery/jquery-3.2.1.min.js"></script>
	<!--Plugins - Validador de FORMULARIOS JQuery -->
	<script src="lib/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="lib/jquery-form/dist/jquery.form.min.js"></script>
	<script type='text/javascript' src="js/dispositivo.js"></script>

		<!--overlay-->
	<script src="lib/jquery-loading-overlay-1.5.3/js/loadingoverlay.min.js"></script>
	<script src="lib/jquery-loading-overlay-1.5.3/js/loadingoverlay_progress.min.js"></script>


	<script type="text/javascript">

		$(function(){

			$('#addNewDispositivo').validate({ 
			 	errorClass: "error-form",
	    		validClass: "valid-form",
		        rules: {
		        	uid: {
		                required: true
		            },
		            idDisp:{
		            	required: true
		            },
		            nombreDisp:{
		            	required: true
		            }

		        },
		        submitHandler: function(form) { 	           
		           //return false; // for demo
					
					
					//agregar una sucursal (sin dispositivos)
		           console.log('submit dispositivo before ajax');
		           $(form).ajaxSubmit({           	
			            type: 'POST',
			            cache: false,
			            url: './php/addDispositivo.php',
			            //url: 'http://localhost/web/Monitoreo/json/monitor.json',
			            contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
			            dataType : 'json',
			            data: $(form).serialize(),
			            success: function(result) {
			            	var resultado = result.result;
			            	if(resultado == "existe") {
								console.log("el id ya existe");
			            		$("#myModalIdKnown").modal();
			            	}
			            	else{
			            		if(resultado =="ok"){
			            			console.log(result);       
					            	console.log("Tecnologia agregada");  
					            	var $idDisp = document.getElementById('idDisp').value;
					            	console.log($idDisp);  
					            	log("addSucursal", "Add new Tecnologia ", $idDisp + " ");  	
						           	$("#myModalSucDataSuccess").modal();
						           //deberia haber un if para saber si regresa a administrador o a otro tipo de usuario
			            		}
			            	}
			            	
			            },
			            error: function(result){
			            	console.log(result);
			            	console.log("error datos");  

			            }
			        });

					
		           
		           
		           
		           //arreglo para sacar los dispositivos

		           	var checked = $("input[name='dispositivo']:checked").length;
					console.log('checked', checked);
					var array = [];
					if(checked>0){
						$('input[name="dispositivo"]:checked').each(function() {
						    array.push(this.value);
						});
					}
					console.log(array);

					//halla el valor de idsucursal
					//$("#idCod").value

					
					//agregar los dispositivos a la sucursal
					var sendData ={};
					sendData = {
						"codDisp": document.getElementById('idDisp').value,
						"contenidos":array
					}

					console.log(sendData);

					
					
					$.ajax({  
						type:'POST',
						cache:false,
						url: './php/editContToDispositivo.php',
						contentType: "application/x-www-form-urlencoded; charset=UTF-8",
						dataType: 'json',
						data: sendData,
						success: function(result) {
							
			            	console.log(result);       
			            	console.log("contenidos agregados");   
			            	//modal para redireccionar a algun sitio
			            },
			            error: function(result){
			            	console.log(result);
			            	console.log("error datos");  
			            }

					});
					

					
					
	        	}

		    });

		});


	</script>	
</head>
<body>

	<!--NAV BAR-->
	<?php include('navbar.php') ?>
    <div class="nav-separador"></div>   	
     <!-- -->   
     <!--LOGO WORKCAFE-->
    <?php include('logobar.php') ?>
  	<!-- -->  

	<div class="container">
	
		<div id="contenedorOpcion">
				
			<div id="playlist">
				<h4 class="italic">Usuario: <?php echo $_SESSION["USUARIO"] ?></h4>
				<div class="cabecera-separator"></div>
				
				<div id="alertMessagePL" class="alert alert-success hidden"></div>
				<div id="newElement"/>		

					<div id="formNewElement" class="formNewElement well">
						<form id="addNewDispositivo" method="" action="">
							<div><label class="control-label" style=''>Agregar Dispositivo:</label><p></p></div>

								<input hidden="true" type="text" name="uid" value = <?php echo $_SESSION["IDUSUARIO"] ?>> </input>
								<p></p>
								<p></p>
								<label class="control-label" style=''>ID Dispositivo:</label>
						    	<div><input class="form-control" type="text" name="idDisp" id="idDisp" value=""></div>
						    	<p></p>
								<p></p>
								<label class="control-label" style=''>Nombre Dispositivo:</label>
						    	<div><input class="form-control" type="text" name="nombreDisp" value=""></div>
						    	<p></p>
								<p></p>
								<label class="control-label" style=''>Detalles:</label>
						    	<div><input class="form-control" type="text" name="detallesDisp" value=""></div>
						    	<p></p>
								<p></p>

								<div id="dispositivoCHECK" class="form-group ">
									<label class="control-label" style='color: #868686;'>Dispositivos:</label>
									<div name="listsuc" id="listaContenidosClear"></div>
								</div>
								<button type="submit" class="btn btn-success">Agregar</button>
						</form>				
					</div>	

				</div>
			</div>
		</div>
		<br/>


		<!-- Trigger the modal with a button -->
		<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" 
		data-target="#myModal">Open Modal</button> -->

		<!-- Modal Message-->

	
	</div>	
		
		<!--Modal-->
		<div id="myModalPswFailure" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Error al ingresar la contraseña actual</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataFailure()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="myModalIdKnown" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Error</h4>
		      </div>
		      <div class="modal-body">
		        <p>El id ya existe</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataFailure()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="myModalPswMissmatch" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Error al ingresar la nueva contraseña</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataFailure()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="myModalPswSuccess" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Contraseña actualizada</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataSuccess()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="myModalDataSuccess" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Datos actualizados</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataSuccess()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="myModalSucDataSuccess" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Sucursal Creada</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataSuccess()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>

	<script src="lib/file-upload/js/vendor/jquery.ui.widget.js"></script>

	<script src="lib/file-upload/js/jquery.iframe-transport.js"></script>

	<script type="text/javascript">
		function onClickDataFailure(){
			window.location.href = 'agregarDispositivo';
		}

		function onClickDataSuccess(){
			window.location.href = 'administrador';
		}





		function log(type, evento, idsucursal, detalle = "") {
			$.ajax({
		        type: 'POST',
		        cache: false,
		        url: './php/log.php',
		        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
		        data: {"type":type, "evento":evento, "idsucursal":idsucursal, "detalle":detalle},
		        success: function(result) {   	
		    		console.log(result);
		        },
		    });
		}








	</script>
	<!-- The basic File Upload plugin -->

</body>
</html>
