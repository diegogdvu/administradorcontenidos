<!DOCTYPE html>
<?php 
session_start();

?>
<html>
<head>
	<title>Register</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!-- No escala -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!--<link rel="icon" href="../../favicon.ico">-->
	<!-- Bootstrap core CSS -->
	<link href="lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

	<!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">


	<script src="lib/jquery/jquery-3.2.1.min.js"></script>
	<!--Plugins - Validador de FORMULARIOS JQuery -->
	<script src="lib/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="lib/jquery-form/dist/jquery.form.min.js"></script>
	<script type="text/javascript">

	$(function(){
		//Ready
		// Initialize the plugin
		 $('#registerForm').validate({ 
		 	errorClass: "error-form",
    		validClass: "valid-form",
	        rules: {
	            u: {
	                required: true,
	            },
	            p: {
	                required: true,
	                minlength: 4
	            },
	            ul:{
	            	required:true,
	            },
	            un:{
	            	required:true,
	            }
	        },
	        submitHandler: function(form) { 	           
	           //return false; // for demo
	           var $alert = $("#alertMessage");
	           $alert.removeClass('show').addClass('hidden');

	           console.log('submit');
	           $(form).ajaxSubmit({           	
		            type: 'POST',
		            cache: false,
		            url: './php/insertNewUser.php',
		            //url: 'http://localhost/web/Monitoreo/json/monitor.json',
		            contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
		            dataType : 'json',
		            data: $(form).serialize(),
		            success: function(result) {
		            	console.log(result);

		            	$("#myModalSuccess").modal();	
		            	//gotoLogin();
		            	          	
		            	//window.location.href = "administrador";           
		            },
		            error: function(result) {
		            	console.log(result);
		            },
		        });
	           //window.alert("Registro Exitoso");
			   
        	}



	    });

		 $("#btnLogin").on('click', gotoLogin);

		

		function log(userData) {
			var idUsuario = userData.idUsuario;
			var usuario = userData.usuario;
			console.log("usuario:",usuario,"idUsuario:",idUsuario);

			$.ajax({
		        type: 'POST',
		        cache: false,
		        url: './php/log.php',
		        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
		        data: {"type":"login", "evento":"Login", "idusuario":idUsuario},
		        success: function(result) {   	
		    		console.log(result);
		    		window.location.replace("administrador");
		        },
		    });
		}
	});

	</script>
</head>
<body class="body-login">
	<div class="login">
	    <h1>Register</h1>
	    <br/>
	    <form id="registerForm" method="" action="">
	    	<input type="text" name="u" placeholder="User" required/>
	    	<input type="text" name="un" placeholder="Name" required/>
	    	<input type="text" name="ul" placeholder="Last Name" required/>
	        <input type="password" name="p" placeholder="Password"/>
	        <button type="submit" class="btn btn-primary btn-block btn-large">Registrar</button>
	    </form>
	    <p></p>
	    <p align="center">¿Ya tiene cuenta?</p>
	    <button id="btnLogin" type="login" class="btn btn-primary btn-block btn-large">Conectarse</button>
	    <br>
	    <div id="alertMessage" class="alert alert-danger hidden"></div>
	</div>

	<div id="myModalSuccess" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Usuario creado</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="gotoLogin()" >OK</button>
		      </div>
		    </div>

		  </div>
	</div>


	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		function gotoLogin() {
			//contgotoRegister
			console.log("gotoLogin");
			window.location.href = 'index';
		}
	</script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
</html>
