<!DOCTYPE html>
<?php 
session_start();

?>
<html>
<head>
	<title>Perfil</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!-- No escala -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!--<link rel="icon" href="../../favicon.ico">-->
	<!-- Bootstrap core CSS -->
	<link href="lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

	<!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">


	<script src="lib/jquery/jquery-3.2.1.min.js"></script>
	<!--Plugins - Validador de FORMULARIOS JQuery -->
	<script src="lib/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="lib/jquery-form/dist/jquery.form.min.js"></script>
	<script type="text/javascript">
		$(function(){

			$('#updateNameForm').validate({ 
			 	errorClass: "error-form",
	    		validClass: "valid-form",
		        rules: {
		        	uid: {
		                required: true
		            }

		        },
		        submitHandler: function(form) { 	           
		           //return false; // for demo
		           var $alert = $("#alertMessage");
		           $alert.removeClass('show').addClass('hidden');

		           console.log('submit');
		           $(form).ajaxSubmit({           	
			            type: 'POST',
			            cache: false,
			            url: './php/updateUser.php',
			            //url: 'http://localhost/web/Monitoreo/json/monitor.json',
			            contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
			            dataType : 'json',
			            data: $(form).serialize(),
			            success: function(result) {
			            	console.log(result);       
			            	console.log("datos actualizados");  
			            	//log
			            	var $iduser = document.getElementById('uid').value;
			            	log("updateUser", "Update user data", $iduser + " ");

				           	$("#myModalDataSuccess").modal();
				           //deberia haber un if para saber si regresa a administrador o a otro tipo de usuario
			            },
			            error: function(result){
			            	console.log(result);
			            	console.log("error datos");  
			            }
			        });


	        	}

		    });



			$('#updatePswForm').validate({ 
			 	errorClass: "error-form",
	    		validClass: "valid-form",
		        rules: {
		        	uid: {
		                required: true
		            },


		        },
		        submitHandler: function(form) { 	           
		           //return false; // for demo
		           var $alert = $("#alertMessage");
		           $alert.removeClass('show').addClass('hidden');

		           console.log('submit');
		           $(form).ajaxSubmit({           	
			            type: 'POST',
			            cache: false,
			            url: './php/updatePsw.php',
			            //url: 'http://localhost/web/Monitoreo/json/monitor.json',
			            contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
			            dataType : 'json',
			            data: $(form).serialize(),
			            success: function(result) {
			            	console.log(result);
			            	var resultado = result.result;
			            	if(resultado =="ok"){
				            	console.log("modal psw success");
				            	//log
				            	//log
				            	var $iduser = document.getElementById('uid').value;
				            	log("updateUser", "Update user password", $iduser + " ");

				            	$("#myModalPswSuccess").modal();	
			            	}    
			            	else {
			            		if(resultado=="nopsw"){
			            		console.log("modal psw failure");
			            		$("#myModalPswFailure").modal();	
			            		}
			            		else if(resultado=="diffpsw"){
			            		console.log("modal psw, different new passwords");
			            		$("#myModalPswMissmatch").modal();	
			            		} 
			            	}
			            	//different new psw
			            	       	

				           //deberia haber un if para saber si regresa a administrador o a otro tipo de usuario
				           //window.location.href = 'administrador';          
			            },
			            error: function(result){
			            	console.log(result);
			            	//no the same psw

			            	
			            	//window.alert("Error al ingresar las contraseñas");				            	
			            	//window.location.href = 'profile';
			            },
			        });

		           
	        	}

		    });





		});


	</script>	
</head>
<body>

	<!--NAV BAR-->
	<?php include('navbar.php') ?>
    <div class="nav-separador"></div>   
     <!-- -->   
     <!--LOGO WORKCAFE-->
    <?php include('logobar.php') ?>
  	<!-- -->  

	<div class="container">
	
		<div id="contenedorOpcion">
				
			<div id="playlist">
				<h4 class="italic">Usuario: <?php echo $_SESSION["USUARIO"] ?></h4>
				<div class="cabecera-separator"></div>
				

				<div id="alertMessagePL" class="alert alert-success hidden"></div>
				<div id="newElement"/>		



					<div id="formNewElement" class="formNewElement well">
						<form id="updateNameForm" method="" action="">
							<div><label class="control-label" style=''>Cambiar Datos:</label><p></p></div>

								<input hidden="true" type="text" name="uid" id="uid" value = <?php echo $_SESSION["IDUSUARIO"] ?>> </input>
								<p></p>
								<p></p>
								<label class="control-label" style=''>Nombre:</label>

						    	<div><input class="form-control" type="text" name="un" value=<?php echo $_SESSION["NOMBRE"] ?>></div>
						    	<p></p>
								<p></p>
								<label class="control-label" style=''>Apellidos:</label>
						    	<div><input class="form-control" type="text" name="ul" value=<?php echo $_SESSION["APELLIDOS"] ?>></div>
						    	<p></p>
								<p></p>
								<button type="submit" class="btn btn-success">Actualizar</button>
						</form>	
						<hr></hr>

						<form id="updatePswForm" method="" action="">	
								<div><label class="control-label" style=''>Cambiar contraseña:</label><p></p></div>
								<input hidden="true" type="text" name="uid" value = <?php echo $_SESSION["IDUSUARIO"] ?>> </input>
								<label class="control-label" style=''>Contraseña Actual:</label>
								<div><input class="form-control" type="password" name="p"  required/></div>
								<p></p>
								<p></p>
								<label class="control-label" style=''>Nueva Contraseña:</label>
								<div><input class="form-control" type="password" name="np1"  required/></div>
								<p></p>
								<p></p>
								<label class="control-label" style=''>Reingrese la nueva contraseña:</label>
								<div><input class="form-control" type="password" name="np2"  required/></div>
								<p></p>
								<p></p>
								<button type="submit" class="btn btn-success">Actualizar</button>
								<hr></hr>
				        		
				    	</form>					
					</div>	

				</div>
			</div>
		</div>
		<br/>


		<!-- Trigger the modal with a button -->
		<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" 
		data-target="#myModal">Open Modal</button> -->

		<!-- Modal Message-->

	
	</div>	
		
		<!--Modal-->
		<div id="myModalPswFailure" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Error al ingresar la contraseña</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataFailure()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="myModalPswMissmatch" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Error al ingresar la nueva contraseña</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataFailure()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="myModalPswSuccess" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Contraseña actualizada</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataSuccess()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="myModalDataSuccess" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Datos actualizados</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataSuccess()">OK</button>
		      </div>
		    </div>

		  </div>
		</div>
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>

	<script src="lib/file-upload/js/vendor/jquery.ui.widget.js"></script>

	<script src="lib/file-upload/js/jquery.iframe-transport.js"></script>

	<script type="text/javascript">
		function onClickDataFailure(){
			window.location.href = 'profile';
		}

		function onClickDataSuccess(){
			window.location.href = 'administrador';
		}

		function log(type, evento, uid,detalle = "") {
			$.ajax({
		        type: 'POST',
		        cache: false,
		        url: './php/log.php',
		        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
		        data: {"type":type, "evento":evento, "uid":uid, "detalle":detalle},
		        success: function(result) {   	
		    		console.log(result);
		        },
		    });
		}


	</script>
	<!-- The basic File Upload plugin -->

</body>
</html>
