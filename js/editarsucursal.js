"use strict";
var jsonData = "";
var totalElements = 0;
var visualList = "grid";

$(function(){
	//$.LoadingOverlay("show");
	awake();	
});

function awake() {

    //Inicio
	getData();
}

function getData() {
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        dataType : 'json',
        data: {"type":"get"},
        success: function(result) {   	
    		console.log(result);
    		jsonData = result;
    		var jsonDis = jsonData.dispositivo;
    		//getListPlaylist(jsonDis.nombre);
    		//console.log(jsonData.data.dispositivos);
    		//setSucursal();
    		//getVideosData();
    		//setSelectContenidos();
    		showDispositivosList()
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
        	console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
    	}
    });
}

function showDispositivosList() {
	//Checkboxes
	var $foundit=0;
	var $list = $("#dispositivosList");
	for(var i=0; i<jsonData.dispositivosList.length; i++) {
		var nombreDis = jsonData.dispositivosList[i].nombre;
		var idDis = jsonData.dispositivosList[i].codigo;


		for(var j=0; j<jsonData.sucursal.dispositivos.length;j++){
			var nombreDisChecked = jsonData.sucursal.dispositivos[j].nombre;
			if(nombreDisChecked == nombreDis){
				var str = '<div class="checkbox checkbox-danger inline-block">';				
					str += '<input type="checkbox" name="dispositivo" tag="'+ idDis +'"';
					str += 'value="'+nombreDis+'" id="check'+trimSpaces(nombreDis)+'" checked >';									
					str += '<label for="check'+trimSpaces(nombreDis)+'">'+nombreDis+'</label>';
					str += '</div>';
				$list.append(str);	
				$foundit = 1;
				break;
			}
		}

		if($foundit==1){
			$foundit=0;
		}
		else{
			var str = '<div class="checkbox checkbox-danger inline-block">';				
				str += '<input type="checkbox" name="dispositivo" tag="'+ idDis +'"';
				str += 'value="'+nombreDis+'" id="check'+trimSpaces(nombreDis)+'">';
				str += '<label for="check'+trimSpaces(nombreDis)+'">'+nombreDis+'</label>';
				str += '</div>';
			$list.append(str);
		}
}

function trimSpaces(str) {
	return str.replace(/ +/g, "");
}

}