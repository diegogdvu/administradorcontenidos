"use strict";
var jsonData = "";

/*
$(document).ajaxStart(function(){
    $.LoadingOverlay("show");
});

$(document).ajaxStop(function(){
    $.LoadingOverlay("hide");
});
*/

$(function(){
	// Show full page LoadingOverlay
	$.LoadingOverlay("show");
	getData();
});

function getData() {
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        dataType : 'json',
        data: {"type":"get"},
        success: function(result) {   	
    		console.log(result);
    		jsonData = result;
    		setSucursal();
    		$.LoadingOverlay("hide");  
        },
    });
}

function setSucursal() {
	var json = jsonData.sucursal;
	console.log("SUCURSAL", json.codigosuc, json.nombre);

	var codigosuc = json.codigosuc;
	var nombre = json.nombre;
	var pais = json.pais;
	var dispositivos = json.dispositivos;
	var idSuc = "Suc"+codigosuc;

	//$("#tituloSucursal").text('Sucursal: '+nombre+' - '+codigosuc);
	$("#tituloSucursal").html('<font color="#a0a0a0">Sucursal:</font> '+nombre);

	setDispositivos();
}

function setDispositivos() {
	for(var i=0; i<jsonData.sucursal.dispositivos.length; i++) {
		var json = jsonData.sucursal.dispositivos[i];
		console.log(">> DISPOSITIVO", json.nombre);
		//break;
		var codigo = json.codigo;
		var nombre = json.nombre;
		var detalles = json.detalles;
		var idDis = "Suc"+codigo;

		//Agregamos el dispositivo
		var strSuc = "<span pos="+i+" idDispositivo="+codigo+" id="+idDis+" class='listaDispositivo'>";
				strSuc += "<div class='row'>";
					strSuc += "<div class='col-xs-2 col-xs-offset-1'>" + codigo +"</div>";
					strSuc += "<div class='col-xs-4'>"+ nombre +"</div>";
                    strSuc += "<div class='col-xs-3 '>"+ detalles +"</div>";
					strSuc += "<div class='col-xs-2'>"
						strSuc += "<div class='iconClick floatRight'>";
                            strSuc += "<img src='./css/images/flecha.png' height='20px' alt='flecha.png'/>";
                        strSuc += "</div>"; 
					strSuc += "</div>";	
				strSuc += "</div>";
			strSuc += "</span>";
		$("#listaContainerDispositivos").append(strSuc);	

		//Agregamos el listener
		//$("#"+idDis+" .iconClick").on('click', clickDispositivo);	
        $("#listaContainerDispositivos #"+idDis).on('click', clickDispositivo);

	}
}

function clickDispositivo() {
	var target = this;
	//var idDispositivo = $(target).parent().parent().parent().attr("idDispositivo");
	//var posDispositivo = $(target).parent().parent().parent().attr("pos");
    var idDispositivo = $(target).attr("idDispositivo");
    var posDispositivo = $(target).attr("pos");
	var opcion = $(target).attr("value"); 
	//Seteamos la opcion que se eligio (upload o playlist)
	jsonData.sucursal.dispositivos[posDispositivo].opcion = opcion;
	var data = jsonData.sucursal.dispositivos[posDispositivo];
	console.log("iconClick:: idDispositivo->",idDispositivo," posDispositivo->",posDispositivo);
	console.log("DISPOSITIVOS >>",jsonData.sucursal.dispositivos[posDispositivo]);
	goDispositivo(data);
}

function goDispositivo(jsonData) {
	console.log(jsonData);
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
       // dataType : 'string',
        data: {"type":"set", "dataKey":"dispositivo", "dataValue":jsonData},
        success: function(result) {   	
    		console.log(result);
    		window.location.href = "./dispositivo";

        },
    });
}

