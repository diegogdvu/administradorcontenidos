"use strict";
var jsonEquipos_old = "old";
var jsonEquipos = "";
var jsonDispositivos_old = "old";
var jsonDispositivos = "";
var count = 0;

/*
$(document).ajaxStart(function(){
    $.LoadingOverlay("show");
});

$(document).ajaxStop(function(){
    $.LoadingOverlay("hide");
});
*/

$(function(){
	// Show full page LoadingOverlay
	$.LoadingOverlay("show");
    //$(".contenido").LoadingOverlay("show");
	getDataSucursales();	
});

function getDataSucursales() {
	$.ajax({
        type: 'POST',
        cache: false,
        //url: 'http://stats.px-gp.com/betaws/monitoreodone/equiposestado.php',
        url: './php/sucursales.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        dataType : 'json',
        data: '',
        success: function(result) {            	
        	jsonEquipos = result;
    		console.log(result, count);
    		jsonEquipos_old = jsonEquipos;    		
    		count = 0;
    		getDataDispositivos();  
        },
    });    
}

function getDataDispositivos() {
	$.ajax({
        type: 'POST',
        cache: false,
        //url: 'http://stats.px-gp.com/betaws/monitoreodone/equiposestado.php',
        url: './php/dispositivos.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        dataType : 'json',
        data: '',
        success: function(result) {            	
        	jsonDispositivos = result;
    		console.log(result, count);
    		jsonDispositivos_old = jsonDispositivos;
    		setSucursales();
    		setDispositivos();
    		count = 0;    		 
			$.LoadingOverlay("hide");         
        },
    });
}

function setSucursales() {
	for(var i=0; i<jsonEquipos.sucursales.length; i++) {
		var json = jsonEquipos.sucursales[i];
		console.log("SUCURSAL", json.codigosuc);
		//break;
		var codigosuc = json.codigosuc;
		var nombre = json.nombre;
		var pais = json.pais;
		var dispositivos = json.dispositivos;
		var idSuc = "Suc"+codigosuc;

		//Agregamos la sucursal
		var strSuc = "<span pos="+i+" idSucursal="+codigosuc+" id="+idSuc+" class='listaSucursal'>";
    			strSuc += "<div class='row'>";
        			strSuc += "<div class='col-xs-2 col-xs-offset-1 '>"+ codigosuc +"</div>";
        			strSuc += "<div class='col-xs-4'>"+ nombre +"</div>";
                    strSuc += "<div class='col-xs-3'></div>";
        			strSuc += "<div class='col-xs-2'>";
                        strSuc += "<div class='iconClick floatRight'>";
                            strSuc += "<img src='./css/images/flecha.png' height='20px' alt='flecha.png'/>";
                        strSuc += "</div>"; 
                    strSuc += "</div>";			
    			strSuc += "</div>";
            strSuc += "</span>";
		if(nombre != "TODOS") {
			$("#listaContainerSucursales").append(strSuc);
			//$("#listaContainerSucursales #"+idSuc).hide();
			//$("#listaContainerSucursales #"+idSuc).fadeTo(200,1) //show

			//Aregamos el listener
			//$("#"+idSuc+" .iconClick").on('click', clickSucursal);
            $("#listaContainerSucursales #"+idSuc).on('click', clickSucursal);
		}						
	}

	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
       // dataType : 'string',
        data: {"type":"set", "dataKey":"sucursalesList", "dataValue":jsonEquipos.sucursales},
        success: function(result) {   	
    		console.log(result);

        },
    });	
}

function setDispositivos() {
	for(var i=0; i<jsonDispositivos.dispositivos.length; i++) {
		var json = jsonDispositivos.dispositivos[i];
		console.log(">> DISPOSITIVO", json.nombre);
		//break;
		var codigo = json.codigo;
		var nombre = json.nombre;
        var detalles = json.detalles;
		var idDis = "Dis"+codigo;

		//Agregamos el dispositivo
		var strSuc = "<span pos="+i+" idDispositivo="+codigo+" id="+idDis+" class='listaDispositivo'>";
				strSuc += "<div class='row'>";
					strSuc += "<div class='col-xs-2 col-xs-offset-1 '>" + codigo +"</div>";
					strSuc += "<div class='col-xs-4 '>"+ nombre +"</div>";
                    strSuc += "<div class='col-xs-3 '>"+ detalles +"</div>";
					strSuc += "<div class='col-xs-2 '>"
						strSuc += "<div class='iconClick floatRight'>";
                            strSuc += "<img src='./css/images/flecha.png' height='20px' alt='flecha.png'/>";
                        strSuc += "</div>"; 
					strSuc += "</div>";	
				strSuc += "</div>";
			strSuc += "</span>";
		$("#listaContainerDispositivos").append(strSuc);
		//$("#listaContainerDispositivos #"+idDis).hide();
		//$("#listaContainerDispositivos #"+idDis).fadeTo(200,1) //show

		//Aregamos el listener
		//$("#"+idDis+" .iconClick").on('click', clickDispositivo);	
        $("#listaContainerDispositivos #"+idDis).on('click', clickDispositivo);
	}	

	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
       // dataType : 'string',
        data: {"type":"set", "dataKey":"dispositivosList", "dataValue":jsonDispositivos.dispositivos},
        success: function(result) {   	
    		console.log(result);

        },
    });
}

function clickSucursal() {
	var target = this;
	//var idSucursal = $(target).parent().parent().parent().attr("idSucursal");
	//var posSucursal = $(target).parent().parent().parent().attr("pos");
    var idSucursal = $(target).attr("idSucursal");
    var posSucursal = $(target).attr("pos");
	var data = jsonEquipos.sucursales[posSucursal];
	console.log("clickSucursal:: idSucursal->",idSucursal," posSucursal->",posSucursal);
	console.log("DISPOSITIVOS >>",jsonEquipos.sucursales[posSucursal]);
	goSucursal(data);
}

function goSucursal(jsonData) {
	//var jsonDataString = JSON.stringify(jsonData);
	//console.log('{"type":"set", "data":'+jsonDataString+'}');
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
       // dataType : 'string',
        data: {"type":"set", "dataKey":"sucursal", "dataValue":jsonData},
        success: function(result) {   	
    		console.log(result);
    		window.location.href = "./sucursal";

        },
    });
}

function clickDispositivo() {
	var target = this;
	//var idDispositivo = $(target).parent().parent().parent().attr("idDispositivo");
	//var posDispositivo = $(target).parent().parent().parent().attr("pos");
    var idDispositivo = $(target).attr("idDispositivo");
    var posDispositivo = $(target).attr("pos");
	var opcion = $(target).attr("value"); 
	//Seteamos la opcion que se eligio (upload o playlist)
	jsonDispositivos.dispositivos[posDispositivo].opcion = opcion;
	var data = jsonDispositivos.dispositivos[posDispositivo];
	console.log("clickDispositivo:: idDispositivo->",idDispositivo," posDispositivo->",posDispositivo);
	console.log("DISPOSITIVOS >>",jsonDispositivos.dispositivos[posDispositivo]);
	setDataDefault(data);	
}

function setDataDefault(jsonData) {
	//1. Seteamos valores para "sucursal" como "TODOS - ALL"
	var jsonDataDefault = {id:"0", codigosuc:"ALL", nombre:"TODOS", pais:"CHILE", dispositivos:[null]};
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
       // dataType : 'string',
        data: {"type":"set", "dataKey":"sucursal", "dataValue":jsonDataDefault},
        success: function(result) {   	
    		console.log(result);
    		goDispositivo(jsonData);
        },
    });	
}

function goDispositivo(jsonData) {
	console.log(jsonData);
	//2. Seteamos valores para dispositivo
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
       // dataType : 'string',
        data: {"type":"set", "dataKey":"dispositivo", "dataValue":jsonData},
        success: function(result) {   	
    		console.log(result);
    		window.location.href = "./mainDispositivo";
        },
    });
}