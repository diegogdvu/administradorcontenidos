"use strict";
var jsonData = "";
var totalElements = 0;
var visualList = "grid";

$(function(){
	//$.LoadingOverlay("show");
	awake();	
});

function awake() {

    //Inicio
	getData();
}

function getData() {
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        dataType : 'json',
        data: {"type":"get"},
        success: function(result) {   	
    		console.log(result);
    		jsonData = result;
    		showUsuariosList()
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
        	console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
    	}
    });
}

function showUsuariosList() {
	//Checkboxes
	var $list = $("#usuarioList");

	var str = '<select class="form-control" name="idUsuarios" id="idUsuarios">';
	for(var i=0; i<jsonData.USUARIOS.length; i++) {
		var id = jsonData.USUARIOS[i][0];
		var nombre = jsonData.USUARIOS[i][1];
		var apellidos = jsonData.USUARIOS[i][2];
		if(i==0){
			str += '<option value="'+id+'" checked>'+nombre+" "+''+apellidos+'</option>'	
		}
		else{
			str += '<option value="'+id+'">'+nombre+" "+''+apellidos+'</option>'	
		}		
			
	}
	str += '</select>';
	str += '<br><br>';
	$list.append(str);	

}