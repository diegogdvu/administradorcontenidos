"use strict";
var jsonData = "";
var totalElements = 0;
var visualList = "grid";

/*
$(document).ajaxStart(function(){
    $.LoadingOverlay("show");
});

$(document).ajaxStop(function(){
    $.LoadingOverlay("hide");
});
*/

$(function(){
	// Show full page LoadingOverlay
	$.LoadingOverlay("show");
	awake();	
});

function awake() {
	var $inicio = $("#inicioDATE");
	var $fin = $("#finDATE");

	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

    $inicio.val(today);
    $inicio.attr("min", today); 
    $inicio.on("change", checkDate);
    $inicio.on("change", resetCheckTiempo);
    //console.log(today);
    $fin.val(today);
    $fin.attr("min", today);
    $fin.on("change", resetCheckTiempo);
    $('#btnDeleteDefault').hide();
    	//Inicio
	start();
}

function getChecklistContenido(){
	var $list = $("#checklistContenido");
	//for de lista de todos los contendidos
	for(var i=0; i<jsonData.dispositivosList.length; i++) {
		var nombreDis = jsonData.dispositivosList[i].nombre;
		var idDis = jsonData.dispositivosList[i].codigo;


		//contenidos actual
		for(var j=0; j<jsonData.sucursal.dispositivos.length;j++){
			var nombreDisChecked = jsonData.sucursal.dispositivos[j].nombre;
			if(nombreDisChecked == nombreDis){
				var str = '<div class="checkbox checkbox-danger inline-block">';				
					str += '<input type="checkbox" name="dispositivo" tag="'+ idDis +'"';
					str += 'value="'+nombreDis+'" id="check'+trimSpaces(nombreDis)+'" checked >';									
					str += '<label for="check'+trimSpaces(nombreDis)+'">'+nombreDis+'</label>';
					str += '</div>';
				$list.append(str);	
				$foundit = 1;
				break;
			}
		}

		if($foundit==1){
			$foundit=0;
		}
		else{
			var str = '<div class="checkbox checkbox-danger inline-block">';				
				str += '<input type="checkbox" name="dispositivo" tag="'+ idDis +'"';
				str += 'value="'+nombreDis+'" id="check'+trimSpaces(nombreDis)+'">';
				str += '<label for="check'+trimSpaces(nombreDis)+'">'+nombreDis+'</label>';
				str += '</div>';
			$list.append(str);
		}
	}
}

function getChecklistLineContenido(){
	var $list = $("#checklistLineContenido");
	//for de lista de todos los contendidos
	for(var i=0; i<jsonData.dispositivosList.length; i++) {
		var nombreDis = jsonData.dispositivosList[i].nombre;
		var idDis = jsonData.dispositivosList[i].codigo;


		//contenidos actual
		for(var j=0; j<jsonData.sucursal.dispositivos.length;j++){
			var nombreDisChecked = jsonData.sucursal.dispositivos[j].nombre;
			if(nombreDisChecked == nombreDis){
				var str = '<div class="checkbox checkbox-danger inline-block">';				
					str += '<input type="checkbox" name="contendio" tag="'+ idDis +'"';
					str += 'value="'+nombreDis+'" id="check'+trimSpaces(nombreDis)+'" checked >';									
					str += '<label for="check'+trimSpaces(nombreDis)+'">'+nombreDis+'</label>';
					str += '</div>';
				$list.append(str);	
				$foundit = 1;
				break;
			}
		}

		if($foundit==1){
			$foundit=0;
		}
		else{
			var str = '<div class="checkbox checkbox-danger inline-block">';			
				str += '<input type="checkbox" name="contenido" tag="'+ idDis +'"';
				str += 'value="'+nombreDis+'" id="check'+trimSpaces(nombreDis)+'">';
				str += '<label for="check'+trimSpaces(nombreDis)+'">'+nombreDis+'</label>';
				str += '</div>';
			$list.append(str);
		}
	}
}

function getListPlaylist(nombreDis){

	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/listPlaylist.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        data: {dir: nombreDis},
        success: function(result) {   	
    		console.log("Playlist xml: " + result);
    		var listPlaylist = new Array();
    		listPlaylist = eval(result);
    		console.log("lisPlaylistlength: " + listPlaylist.length);
    		$('#btnDeleteDefault').hide();
    		if(listPlaylist.length==undefined){
    			console.log("hide playlist");
    			$("#playlistSELECT").hide();
    			
    		}else{
    			var $select = $("#playlistSELECT");
    			$select.children('option:not(:first)').remove();
    			$select.show();
	    		var str;

	    		for(var i = 0; i < listPlaylist.length; i++){
	    			var element= listPlaylist[i].substr(0, listPlaylist[i].indexOf('.')); 
	    			str = '<option value="'+element+'">';
					str += element;
					str += '</option>';
					$select.append(str);	
	    		}
    		}
    		
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
        	console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
    	}
    });

}

function resetCheckTiempo() {
	$('#checkTiempo').prop('checked', false);
}

function checkDate() {
	var $fin = $("#finDATE");

	var myDate = $(this).val();
	var myDate = myDate.split("-");
	var minimo = myDate[0]+"-"+myDate[1]+"-"+myDate[2];

	if((new Date($fin.val()).getTime() < new Date($(this).val()).getTime())) {
		$fin.val(minimo)
	}

	//console.log(minimo);
	$fin.attr("min", minimo); 
}

function start() {
	getData();
}

function getData() {
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/session.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        dataType : 'json',
        data: {"type":"get"},
        success: function(result) {   	
    		console.log(result);
    		jsonData = result;
    		var jsonDis = jsonData.dispositivo;
    		getListPlaylist(jsonDis.nombre);
    		//console.log(jsonData.data.dispositivos);
    		//setSucursal();
    		//getVideosData();
    		setSelectContenidos();
    		getContenidosList();
    		getContenidosCheckList();
    		getContenidosClearCheckList();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
        	console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
    	}
    });
}

function getContenidosList(){
	for(var i = 0; i< jsonData.dispositivo.contenidos.length; i++){
		var json = jsonData.dispositivo.contenidos[i];
		console.log(">> contenido", json.nombre);
		var idContenido = json.idContenido;
		var nombreContenido = json.Nombre;


		var strSuc = "<span pos="+i+" idContenido="+idContenido+" id="+idContenido+" class='listaSucursal'>";
				strSuc += "<div class='row'>";
					strSuc += "<div class='col-xs-2 col-xs-offset-1 '>" + idContenido +"</div>";
					strSuc += "<div class='col-xs-4 '>"+ nombreContenido +"</div>";	
				strSuc += "</div>";
			strSuc += "</span>";
		$("#listaContenidos").append(strSuc);


	}
}

function trimSpaces(str) {
	return str.replace(/ +/g, "");
}



function getContenidosClearCheckList(){

	//ajax
	/*
	var sendData = { "test" : ["test1", "test2"]};
	var a = sendData.test[1];
	console.log("sendData",a);
	*/
	var sendData = {};

	$.ajax({
		type: 'POST',
		cache: false,
		url: './php/getContenidos.php',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		dataType: 'json',
		data: sendData,
		success: function(result){
			console.log(result);
			//sendData = result;
			console.log("contenidos adquiridos");

			//todos los contenidos
			var $list = $("#listaContenidosClear");
			for(var i = 0; i< result.length; i++) {
				var jsonrow = result[i];
				var idContenido = jsonrow[0];
				var nombreContenido = jsonrow[1];

				//si es que no lo tiene pono un checkbox vacio en ese contenido

				var str = '<div class="checkbox checkbox-danger inline-block">';				
					str += '<input type="checkbox" name="dispositivo" tag="'+ idContenido +'"';
					str += 'value="'+nombreContenido+'" id="check'+trimSpaces(nombreContenido)+'">';
					str += '<label for="check'+trimSpaces(nombreContenido)+'">'+nombreContenido+'</label>';
					str += '</div>';
				$list.append(str);

			}

		},
		error: function(result){
			console.log(result);
			console.log("error al tratar de conseguir los contenidos");
		}
	});

	function trimSpaces(str) {
		return str.replace(/ +/g, "");
	}

	console.log("sendData",sendData);

}











//checklist de contenidos para actualizar los contenidos en un dispositivo
function getContenidosCheckList(){

	//ajax
	/*
	var sendData = { "test" : ["test1", "test2"]};
	var a = sendData.test[1];
	console.log("sendData",a);
	*/
	var sendData = {};

	$.ajax({
		type: 'POST',
		cache: false,
		url: './php/getContenidos.php',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		dataType: 'json',
		data: sendData,
		success: function(result){
			console.log(result);
			//sendData = result;
			console.log("contenidos adquiridos");

			//todos los contenidos
			var $foundit=0;
			var $list = $("#listaContenidos2");
			for(var i = 0; i< result.length; i++) {
				var jsonrow = result[i];
				var idContenido = jsonrow[0];
				var nombreContenido = jsonrow[1];

				//pone check en los contenidos que ya tiene el dispositivo
				for(var j = 0; j< jsonData.dispositivo.contenidos.length; j++){
					var idContenidoChecked =jsonData.dispositivo.contenidos[j].idContenido;
					var nombreContenidoChecked = jsonData.dispositivo.contenidos[j].Nombre;
					if(idContenido==idContenidoChecked){
						var str = '<div class="checkbox checkbox-danger inline-block">';				
							str += '<input type="checkbox" name="contenido" tag="'+ idContenido +'"';
							str += 'value="'+nombreContenido+'" id="check'+trimSpaces(nombreContenido)+'" checked >';									
							str += '<label for="check'+trimSpaces(nombreContenido)+'">'+nombreContenido+'</label>';
							str += '</div>';
							$list.append(str);	
							$foundit = 1;
							break;

					}				
				}

				//si es que no lo tiene pono un checkbox vacio en ese contenido
				if($foundit==1){
				$foundit=0;
				}
				else{
					var str = '<div class="checkbox checkbox-danger inline-block">';				
						str += '<input type="checkbox" name="contenido" tag="'+ idContenido +'"';
						str += 'value="'+nombreContenido+'" id="check'+trimSpaces(nombreContenido)+'">';
						str += '<label for="check'+trimSpaces(nombreContenido)+'">'+nombreContenido+'</label>';
						str += '</div>';
					$list.append(str);
				}

			}

		},
		error: function(result){
			console.log(result);
			console.log("error al tratar de conseguir los contenidos");
		}
	});

	function trimSpaces(str) {
		return str.replace(/ +/g, "");
	}

	console.log("sendData",sendData);

}

function setSelectContenidos() {
	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var codigo = jsonDis.codigo;
	var nombreDis = jsonDis.nombre;
	var contenidos = jsonDis.contenidos;

	var $select = $("#nombreSELECT");
	for(var i=0; i<contenidos.length; i++) {
		var idContenido = contenidos[i].idContenido;
		var nombre = contenidos[i].Nombre;
		var estado = contenidos[i].Estado;
		console.log("contenidos >>", nombre);

		var option = "";
		if(estado == "ACTIVO") {
		    option += "<option value='"+nombre+"'>";
		}
		else {
			option += "<option value='"+nombre+"' disabled>";
		}
			option += nombre;
			option += "</option>";

		console.log("contenidos >>", option);
		$select.append(option);
	}

	getVideosData();
}

function getVideosData() {
	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var codigo = jsonDis.codigo;
	var nombreDis = jsonDis.nombre;

	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/listFiles.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        dataType : 'json',
        data: {
        	dir: nombreDis
        },
        success: function(result) {   	
    		console.log(result);
    		//console.log(jsonData.data.dispositivos);
    		setVideoData(result);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
        	console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
    		setSucursal();
    	}
    });
}

var arrayVideos = [];
var perPage = 15;
var currentPage = 1;
var numPages = 0;

function setVideoData(result) {
	var data = result;

	for(var i=0; i<data.videos.length; i++) {
		var video = data.videos[i];
		var dir = video.dir;
		var name = video.name;
		var date = video.date;
		var hour = video.hour;
		var videoURL = video.videoURL;

		var obj = new Object();
		obj["dir"] = dir;
		obj["name"] = name;
		obj["date"] = date;
		obj["hour"] = hour;
		obj["videoURL"] = videoURL;

		arrayVideos.push(obj);		
	}
	
	var totalElements = currentPage * perPage;
	var init = totalElements - perPage;
	for(var i=init; i<totalElements; i++) {
		var video = arrayVideos[i];
		if(video) {
			var dir = video["dir"];
			var name = video["name"];
			var date = video["date"];
			var hour = video["hour"];
			var videoURL = video["videoURL"];

			if(name) {
				newVideoListElement(i, name, date, hour, videoURL, "normal");
			}
		}
	}

	addPaginationNav();

	var $page = $("#paginationNav #page"+currentPage);
	$page.addClass("current");

	setSucursal();
}

function addPaginationNav() {
	var newNumPages = Math.ceil(arrayVideos.length / perPage);

	if(newNumPages != numPages) {

		console.log("change addPaginationNav");
		var $nav = $("#paginationNav .pages");
		$nav.empty();

		numPages = Math.ceil(arrayVideos.length / perPage);
		console.log("numPages",numPages);

		for(var i=0; i<numPages; i++) {
			var num = i+1;
			var link = "<a id='page"+num+"' class='page' value="+num+">";
				link += num;
				link += "</a>";
			$nav.append(link);
			$("#page"+num).on('click', showPage);
		}
	}
}

function showPageTo(page, sort) {
	$("#nombreINPUT").empty();

	if(sort == "descendente") {
		arrayVideos.sort(function(a, b){
		    if(a.date < b.date) return -1;
		    if(a.date > b.date) return 1;
		    return 0;
		});
		arrayVideos.reverse();
	}

	addPaginationNav();

	var $page;
	$page = $("#paginationNav #page"+currentPage);
	$page.removeClass("current");

	if(page > numPages) {
		currentPage = numPages;
	}
	else {
		currentPage = page;
	}

	$page = $("#paginationNav #page"+currentPage);
	$page.addClass("current");

	var totalElements = currentPage * perPage;
	var init = totalElements - perPage;
	for(var i=init; i<totalElements; i++) {
		var video = arrayVideos[i];
		if(video) {
			var dir = video["dir"];
			var name = video["name"];
			var date = video["date"];
			var hour = video["hour"];
			var videoURL = video["videoURL"];

			if(name) {
				newVideoListElement(i, name, date, hour, videoURL, "normal");
			}
		}
	}

	console.log("visualList",visualList);
	if(visualList == "list") {
		toList();
	}

	//Volvemos a "chekear" los ADDED
	checkAddedVideos();
}

function showPage() {
	console.log("showPage");
	$("#nombreINPUT").empty();
	var $page;
	$page = $("#paginationNav #page"+currentPage);
	$page.removeClass("current");
	currentPage = $(this).attr("value");
	$page = $("#paginationNav #page"+currentPage);
	$page.addClass("current");

	var totalElements = currentPage * perPage;
	var init = totalElements - perPage;
	for(var i=init; i<totalElements; i++) {
		var video = arrayVideos[i];
		if(video) {
			var dir = video["dir"];
			var name = video["name"];
			var date = video["date"];
			var hour = video["hour"];
			var videoURL = video["videoURL"];

			if(name) {
				newVideoListElement(i, name, date, hour, videoURL, "normal");
			}
		}
	}

	console.log("visualList",visualList);
	if(visualList == "list") {
		toList();
	}

	//Volvemos a "chekear" los ADDED
	checkAddedVideos();
}

function newVideoListElement(i, name, date, hour, videoURL, type) {
	
	var $input = $("#nombreINPUT");
	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var codigo = jsonDis.codigo;
	var nombreDis = jsonDis.nombre;

	var imageSrc = './php/files/'+nombreDis+'/CONTENIDOS/thumbnail/'+cleanString(name).split(".")[0]+'.png';
	//var str = '<div class="col-xs-3 col-sm-2 col-md-1 col-lg-1 videoThumbnail">';
	var str = '<div id="vt" class="videoThumbnail inlineBlock posRelative" nombre="'+name+'">';
			//str += '<img id="videoThumbnail'+i+'" src="'+imageSrc+'" class="img-responsive" alt="'+name+'">';
			str += '<img id="videoThumbnail'+i+'" src="'+imageSrc+'" class="cover" alt="'+name+'">';
			str += '<span class="checkbox checkbox-danger maxWidth">';				
				str += '<input type="checkbox" name="videos" videoname="'+name+'"';
				str += 'value="'+name+'" id="'+name+'">';									
				str += '<label for="'+name+'"><div class="text-overflow">'+name+'</div></label>';
			str += '</span>';
			str += '<label><div class="labelDate hidden">'+date+'</div></label>';
		str += '</div>';
	
	if(type == "normal") {
		$input.append(str);
	}
	else {
		$input.prepend(str)
	}
	

	$("#videoThumbnail"+i).on('click', showVideo);

	//Agregamos la data al lemento del DOM
	var obj = {
		name: name,
		date: date,
		hour: hour,
		videoURL: videoURL
	}
	$("#videoThumbnail"+i).data("data", obj);
}

//MODAL VIDEO POPUP//
function showVideo() {
	console.log("showVideo");
	var data = $(this).data("data");
	var name = data.name;
	var date = data.date;
	var hour = data.hour;
	var videoURL = data.videoURL;

	$("#myModalVideo").off("show.bs.modal").on('show.bs.modal', setVideoURL(videoURL, name));
	$("#myModalVideo").off("hide.bs.modal").on('hide.bs.modal', stopVideo);
	$("#myModalVideo").modal();	
}

function setVideoURL(url, name) {
	console.log("setVideoURL");
	var $title = $('#myModalVideo .modal-title .title');

	/*if(name.length > 20) {
		$title.text(name.substring(0,20)+"...");
	}
	else {
		$title.text(name);
	}*/

	$title.text(name);
	
	$('#videoPopup').attr('src', url);
}

function stopVideo() {
	console.log("stopVideo");
	$('#videoPopup')[0].pause();
}

//---------------

function setSucursal() {
	var json = jsonData.sucursal;
	console.log("SUCURSAL", json.codigosuc, json.nombre);

	var codigosuc = json.codigosuc;
	var nombre = json.nombre;
	var pais = json.pais;
	var dispositivos = json.dispositivos;
	var idSuc = "Suc"+codigosuc;	

	if(nombre == "TODOS") {
		//$("#tituloSucursal").addClass("hidden");
		$("#titulo").addClass("hidden");
		showSucursalesList();
	}
	else {
		//$("#tituloSucursal").text('SUCURSAL "'+nombre+'" - '+codigosuc);
		$("#tituloSucursal").html('<font color="#a0a0a0">Sucursal:</font> '+nombre);
		setDispositivo();
	}
}

function trimSpaces(str) {
	return str.replace(/ +/g, "");
}

function showSucursalesList() {
	//Checkboxes
	var $list = $("#sucursalesList");
	for(var i=0; i<jsonData.sucursalesList.length; i++) {
		var nombreSuc = jsonData.sucursalesList[i].nombre;
		var str = '<div class="checkbox checkbox-danger inline-block">';				
				str += '<input type="checkbox" name="sucursal"';
				str += 'value="'+nombreSuc+'" id="check'+trimSpaces(nombreSuc)+'">';									
				str += '<label for="check'+trimSpaces(nombreSuc)+'">'+nombreSuc+'</label>';
			str += '</div>';
		$list.append(str);

		if(nombreSuc == "TODOS") {
			$("#check"+trimSpaces(nombreSuc)).click (function ()	{
				if($(this).is(':checked')) {
					unCheckAll();
					$(this).prop('checked', true);
				}
			}).prop('checked', true);			
		}
		else {
			$("#check"+trimSpaces(nombreSuc)).click (function ()	{
				if($('#checkTODOS').is(':checked')) {
					$('#checkTODOS').prop('checked', false);
				}
			})
		}		
	}

	//Ponemos visible checkbox de sucursales
	var $check = $("#sucursalCHECK");
	$check.removeClass('show').addClass('hidden');
	$check.removeClass('hidden').addClass('show');


	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var playlistStatus = jsonDis.playlist;

	//Select
	var $select = $("#sucursalSELECT");
	for(var i=0; i<jsonData.sucursalesList.length; i++) {
		var nombreSuc = jsonData.sucursalesList[i].nombre;
		var str;
		if(playlistStatus == "1") {
			if(nombreSuc == "TODOS") {
				str = '<option value="'+nombreSuc+'" selected>';
					str += nombreSuc;
				str += '</option>';
				$select.append(str);
			}
			else {
			    str = '<option value="'+nombreSuc+'">';
					str += nombreSuc;
				str += '</option>';
				$select.append(str);
			}
		}
		else {
			if(nombreSuc != "TODOS") {
			    str = '<option value="'+nombreSuc+'">';
					str += nombreSuc;
				str += '</option>';
				$select.append(str);
			}
		}
				
	}

	//Ponemos visible el Select
	$select.removeClass('show').addClass('hidden');
	$select.removeClass('hidden').addClass('show');
	//Agregamos on change event
	$select.on('change', changePlayList);
	//Desactivamos boton para agregar elemnto hasta que se seleccione una sucursal playlist
	//$("#addElement").addClass("hidden");

	setDispositivo();
}

function changePlayList(e) {
	var optionSelected = $("option:selected", this);
	var valueSelected = this.value;
	console.log("changePlayList",valueSelected);

	$("#addElement").removeClass('hidden').addClass('btn');

	unCheckAll();

	if(valueSelected == "NUEVO") {
		clearPlayList();
		$('#checkTODOS').prop('checked', true);
	}
	else {
		//RICARDO
		getPlaylist(valueSelected,"Playlist");
		$('#check'+trimSpaces(valueSelected)).prop('checked', true);
	}	

	$('#playlistSELECT').val('NUEVO');
	$('#btnDeleteDefault').hide();

	//$('#playlistSELECT').selectmenu('refresh');
}

function getNameTechnology(){
	var json = jsonData.dispositivo;
	var nombre = json.nombre;
	return nombre;
}

function setDispositivo() {
	var json = jsonData.dispositivo;
	console.log("DISPOSITIVO", json.codigo, json.nombre);

	var codigo = json.codigo;
	var nombre = json.nombre;
	var opcion = json.opcion;
	var opcionText;

	if(opcion == "upload") {
		opcionText = "SUBIR CONTENIDO";

	}
	else if(opcion == "playlist") {
		opcionText = "EDITAR PLAYLIST";
	}

	//$("#tituloDispositivo").text('DISPOSITIVO "'+nombre+'" '+'- '+opcionText);
	//$("#tituloDispositivo").text('DISPOSITIVO "'+nombre+'"');
	$("#tituloDispositivo").html('<font id="techName" color="#a0a0a0">Tecnología:</font> '+nombre);
	$("#tituloDispositivos").html('<font id="techName" color="#a0a0a0">Dispositivo:</font> '+nombre);
	$("#tituloContenidos").html('<font id="techName" color="#a0a0a0">Contenidos:</font> ');

	//INFO SUCURSAL
	var json = jsonData.sucursal;
	var codigosuc = json.codigosuc;
	var nombre = json.nombre;

	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var playlistStatus = jsonDis.playlist;


	if(nombre != "TODOS") {
		getPlaylist(nombre,"Playlist");
	}
	else {
		if(playlistStatus == "1") {
			getPlaylist("TODOS","Playlist");
		}
		$.LoadingOverlay("hide"); 
	}

	//Agregamos los listeners
	addListeners();	
}

function getPlaylist(nombre, namePlayList ) {
	
	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var codigo = jsonDis.codigo;
	var nombreDis = jsonDis.nombre;
	var playlistStatus = jsonDis.playlist;
	var url = "";

	if(nombre == "TODOS") {
		url = './php/files/'+nombreDis+'/' +namePlayList+'.xml';
	}
	else {
		url = './php/files/'+nombreDis+'/'+nombre+'/'+namePlayList+'.xml';
	}

	//console.log("url",cleanString(url));
	$.ajax({
        type: 'POST',
        cache: false,
        url: cleanString(url),
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        dataType : 'xml',
        data: '',
        success: function(result) {   	
    		console.log(result);
    		setPlayList(result);
    		$.LoadingOverlay("hide"); 
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
        	console.log('getPlaylist >> status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
        	//Borramos el playlist y reiniciamos el contador.
        	clearPlayList();
        	$.LoadingOverlay("hide"); 
    	}
    });    
}

function setPlayList(xml) {
	console.log(xml);

	//Borramos el playlist
	$("#listaPlaylist").empty();

	var total = $(xml).find('contenido').length;
	for(var i=0; i<total; i++) {
		var contenido = $(xml).find('contenido')[i];
		//contenido - video
		var tipo = $(contenido).find('tipo').text();
		//Tasas - ClimaHoy, ClimaSemana, Mensaje
		var url = $(contenido).find('url').text();
		//Intervalo de tiempo
		var inicio = $(contenido).find('inicio').text();
		var fin = $(contenido).find('fin').text();

		if(inicio == "") {
			inicio = "ALL";
		}
		if(fin == "") {
			fin = "ALL";
		}

		var tiempo = inicio + " / " + fin;
		//Texto de mensaje
		var mensaje = $(contenido).find('mensaje').find('texto').text();
		var fondoMensaje = $(contenido).find('mensaje').find('fondo').text();

		if(String(url) == "Mensaje") {
			console.log(tipo, url, '"'+mensaje+'"');
		}
		else {
			console.log(tipo, url);
		}

		var position = (i+1);
		if(position < 10) {
			position = "0"+position;
		}

		var p = i+1;
		//Agregamos el playlist
		newElement(p, tipo, url, mensaje, fondoMensaje, tiempo);
	}

	var $lista = $("#listaPlaylist");
	totalElements = $lista.children().length;	

	//Validamos los thumbnails
	checkAddedVideos();
}

function addListeners() {
	//Agregamos el listener para cambiar el valor del tipo
	$("#tipoSORTDATE").on('change', sortVideosDate);

	//Agregamos el listener para cambiar el orden alfabetico del los videos
	$("#tipoSORT").on('change', sortVideos);

	//Agregamos el listener para cambiar el valor del tipo
	$("#tipoSELECT").on('change', changeOpciones);

	//Agregamos el listener para cambiar el valor del tipo de CONTENIDO
	$("#nombreSELECT").on('change', changeContenido);

	//Agregamos el listener para cambiar el playlist
	$("#playlistSELECT").on('change', changePlayListSaved);

	//Agregamos el listener para agregar un nuevo elemento
	$("#addElement").on('click', addNewElement);

	//Agregamos el listener para agregar un nuevo elemento
	//$("#removeElement").on('click', removeElements);
	$("#removeElement").on('click', showModalRemoveVideo);

	//Agregamos el listener para recoger la nueva data de playlist
	$("#btnUpdate").on('click', getNewPlaylist);	

	//Agregamos el listener para recoger borrar playlist visualmente
	$("#btnClear").on('click', clearPlayList);

	//Agregamos el listener para guardar playlist visualmente
	$("#btnSave").on('click', showModalSave);

	//Agregamos el listener para borrar un playlist predeterminado
	$("#btnDeleteDefault").on('click', showModlaDeleteDefault);
	
}

function showModlaDeleteDefault(){

	console.log("showModlaDeleteDefault");
	var target = this;
	$("#myModal").modal();
	$("#myModal #btnOk").on('click', function () {
		removeDefaultElement();
    	$("#myModal #btnOk").off('click');
    	$("#myModal").modal("hide");	     
    });    

}

function removeDefaultElement() {

	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var nombreDis = jsonDis.nombre;

	var e = document.getElementById("playlistSELECT");
	var value = e.options[e.selectedIndex].text;

	var sendData = {
	        	"type":"remove", 
	        	"path":"./files/"+nombreDis+"/PREDETERMINADO/" + value +".xml", 
	}

	$.ajax({
		type: 'POST',
		cache: false,
	    url: './php/fileManager.php',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
			        //dataType : 'json',
		data: sendData,
		success: function(result) {   	
		    	console.log(result);
		    	log("remove", "Remove Default Playilist");
		    },
		    error: function(XMLHttpRequest, textStatus, errorThrown){
		        console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
		    }
	});

	setTimeout(function() {
		clearPlayList();
   		getListPlaylist(nombreDis);
	}, 500);

}


function sortVideosDate() {

	var e = document.getElementById("tipoSORTDATE");
	var value = e.options[e.selectedIndex].text;
	console.log(value);

	//Acendente 
	if (value == "Ascendente") {
		 arrayVideos.sort(function(a, b){
		    if(a.date < b.date) return -1;
		    if(a.date > b.date) return 1;
		    return 0;
		});
	}
	//Descendente
	else if (value == "Descendente") {
		arrayVideos.sort(function(a, b){
		    if(a.date < b.date) return -1;
		    if(a.date > b.date) return 1;
		    return 0;
		});
		arrayVideos.reverse();
	}

	for(var i=0; i<arrayVideos.length; i++) {
		console.log(arrayVideos[i]);
	}
	
	showPageTo(1);	
}


function sortVideos() {

	var e = document.getElementById("tipoSORT");
	var value = e.options[e.selectedIndex].text;
	console.log(value);

	//Acendente 
	if (value == "Ascendente") {
		 arrayVideos.sort(function(a, b){
		    if(a.name < b.name) return -1;
		    if(a.name > b.name) return 1;
		    return 0;
		});
	}
	//Descendente
	else if (value == "Descendente") {
		arrayVideos.sort(function(a, b){
		    if(a.name < b.name) return -1;
		    if(a.name > b.name) return 1;
		    return 0;
		});
		arrayVideos.reverse();
	}

	for(var i=0; i<arrayVideos.length; i++) {
		console.log(arrayVideos[i]);
	}
	showPageTo(1);
}


function clearPlayList() {
	//Borramos el playlist
	$("#listaPlaylist").empty();
	totalElements = 0;
	checkAddedVideos();
}

function checkAddedVideos() {
	console.log('checkAddedVideos');
	var $lista = $("#listaPlaylist");
	var total = $lista.children().length;

	var $listaVideos = $("#nombreINPUT");
	var totalVideos = $listaVideos.children().length;
	var ii = 0;

	for(ii=0; ii<totalVideos; ii++) {
		var childVideo = $($listaVideos.children()[ii]);
		var nombreVideo = childVideo.attr("nombre");
		childVideo.find(".videoTag").remove();
	}

	for(var i=0; i<total; i++) {
		var child = $($lista.children()[i]);
		var nombre = child.attr("nombre");
		for(ii=0; ii<totalVideos; ii++) {
			var childVideo = $($listaVideos.children()[ii]);
			var nombreVideo = childVideo.attr("nombre");

			if(nombre == nombreVideo) {
				console.log("MATCH >> "+ nombre);
				childVideo.append("<span class='videoTag'>ADDED</span>");
			}
		}
	}	
}





function removePlaylistElement(target) {
	$(target).parent().parent().parent().remove();
	var $lista = $("#listaPlaylist");
	var total = $lista.children().length;

	for(var i=0; i<total; i++) {
		var $child = $($lista.children()[i]);
		var $position = $child.find(".circulo p");
		var num = i + 1;

		if(num < 10) {
			num = "0"+num;
		}

		$position.text(num);
	}


	showBtnUpdate();
	checkAddedVideos();
} 

function changeContenido() {
	var target = this;
	var $input = $("#infoMensaje");
	var value = target.value;

	if(value == "Mensaje") {
		$input.removeClass('hidden').addClass('show');
	}
	else {
		$input.removeClass('show').addClass('hidden');
	}
}

function changePlayListSaved(){
	var e = document.getElementById("playlistSELECT");
	var strList = e.options[e.selectedIndex].text;
	getPlaylist("PREDETERMINADO",strList);

	$('#sucursalSELECT').val("NUEVO");
	$('#btnDeleteDefault').show();

	//$('#sucursalSELECT').selectmenu('refresh');

}

function changeOpciones() {
	//console.log("changeOpciones");
	var target = this;
	var $input = $("#nombreINPUT");
	var $select = $("#nombreSELECT");	
	var $sortAlpha = $("#tipoSORT");
	var $sortDate = $("#tipoSORTDATE");	
	var $divSelect = $("#divSelect");
	var value = target.value;

	if(value == "CONTENIDO") {
		$input.removeClass('show').addClass('hidden');
		$select.removeClass('hidden').addClass('show');
		$sortAlpha.removeClass('show').addClass('hidden');
		$sortDate.removeClass('show').addClass('hidden');
		$divSelect.removeClass('show').addClass('hidden');
	}
	else {
		$input.removeClass('hidden').addClass('show');
		$select.removeClass('show').addClass('hidden');
		$sortAlpha.removeClass('hidden').addClass('show');
		$sortDate.removeClass('hidden').addClass('show');
		$divSelect.removeClass('hidden').addClass('show');
	}
}

function showModalRemoveVideo() {

	var array = [];
	$('input[name="videos"]:checked').each(function() {
	    array.push(this.value);
	});

	if(array.length > 0) {
		$("#myModalVideoRemove").modal();
		$("#myModalVideoRemove #btnOk").on('click', function () {
			removeElements();  
	    	$("#myModalVideoRemove #btnOk").off('click');
	    	$("#myModalVideoRemove").modal("hide");	     
	    });
	} 
}

function removeElements() {
	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var codigo = jsonDis.codigo;
	var nombreDis = jsonDis.nombre;

	var tipo = $("#tipoSELECT").val();

	if(tipo == "VIDEO") {
		var array = [];
		$('input[name="videos"]:checked').each(function() {
			console.log(this.value);

			//lo borramos del arreglo::
			var index = arrayVideos.findIndex(i => i.name === this.value);
			if (index > -1) {
			    arrayVideos.splice(index, 1);
			}

			//mostramos otra ves los videos en la paginacion que quedo
			showPageTo(currentPage);
			//console.log($(this).attr("videoname"));
		    array.push(this.value);
		    //Lo eliminamos de escena (visualmente)
		    $(this).parent().parent().remove();
		});

		if(array.length > 0) {
			var pathList = [];
			for(var i=0; i<array.length; i ++) {
				pathList.push("./files/"+nombreDis+"/CONTENIDOS/"+array[i]);
			}

			var sendData = {
	        	"type":"batchRemove", 
	        	"pathList":pathList, 
	        }

			$.ajax({
		        type: 'POST',
		        cache: false,
		        url: './php/fileManager.php',
		        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
		        //dataType : 'json',
		        data: sendData,
		        success: function(result) {   	
		    		console.log(result);
		    		log("remove", "Remove Video(s) ", array.toString() + " ");
		        },
		        error: function(XMLHttpRequest, textStatus, errorThrown){
		        	console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
		    	}
		    });
		}
	}
}

function addNewElement() {
	var $alert = $("#alertMessage");
	$alert.removeClass('alert-success show').addClass('hidden');

	var tipo = $("#tipoSELECT").val();
	var url = "";

	if(tipo == "VIDEO") {
		var array = [];
		$('input[name="videos"]:checked').each(function() {
			console.log(this.value);
			//console.log($(this).attr("videoname"));
		   array.push(this.value);
		   //$('#sucursalCHECK').find('input[type=checkbox]:checked').prop('checked', false);
		   //removemos los checks
		   $(this).prop('checked', false);
		   $videoThumbnail = $(this).parent().parent();
		   $videoThumbnail.append("<span class='videoTag'>ADDED</span>");
		});
	}
	else {
		url = $("#nombreSELECT").val();
	}
	
	var mensaje = $("#tituloINPUT").val() +"//"+ $("#mensajeINPUT").val();

	
	var inicio = "";
	var fin = "";

	if($('#checkTiempo').is(':checked')) {
		inicio = "ALL";
		fin = "ALL";
	}
	else {
		inicio = $("#inicioDATE").val();
		fin = $("#finDATE").val();
	}

	var tiempo = inicio + " / " + fin;
	var fondoMensaje = $("#fondoSELECT").val();

	if(tipo == "VIDEO") {
		if(array.length <= 0) {
			$alert.html("<strong>ERROR!</strong> Seleccione un video.");
			$alert.removeClass('hidden').addClass('alert-danger show');
			return;
		}
	}
	else {
		if(url == "Mensaje"){
			if(mensaje == "" || mensaje == "//" || mensaje == " // " || mensaje == "  //  ") {
				$alert.html("<strong>ERROR!</strong> Ingrese texto para 'Mensaje'.");
				$alert.removeClass('hidden').addClass('alert-danger show');
				return;
			}

			if(fondoMensaje == null) {
				$alert.html("<strong>ERROR!</strong> Seleccione un fondo para 'Mensaje'.");
				$alert.removeClass('hidden').addClass('alert-danger show');
				return;
			}	
		}
		else if(url == null) {
			$alert.html("<strong>ERROR!</strong> Seleccione un contenido.");
			$alert.removeClass('hidden').addClass('alert-danger show');
			return;
		}
	}
		
	var $lista = $("#listaPlaylist");
	totalElements = $lista.children().length;
	var p;

	if(tipo == "CONTENIDO") {
		totalElements++;
		p = totalElements;
		//Agregamos nuevo elemento al playlist
		newElement(p, tipo, url, mensaje, fondoMensaje, tiempo);
		
	}
	else {
		for(var i=0; i<array.length; i ++) {
			totalElements++;
			p = totalElements;
			//Agregamos nuevo elemento al playlist
			newElement(p, tipo, array[i], mensaje, "", tiempo);
		}
	}

	showBtnUpdate();
}

function newElement(position, tipo, url, mensaje, fondoMensaje, tiempo) {
	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var codigo = jsonDis.codigo;
	var nombreDis = jsonDis.nombre;

	if(position < 10) {
		position = "0"+position;
	}

	var thumbnail;
	var src;

	if(tipo == "CONTENIDO") {
		var cont;
		if(url == "ClimaHoy" || url == "ClimaSemana") {
			cont = "clima.png";
		}
		else if(url == "Tasas"){
			cont = "tasas.png";
		}
		else if(url == "Mensaje"){
			cont = "mensaje.png";
		}

		thumbnail = cont;
		src = "./php/files/thumbnail/"+thumbnail;
	}
	else {
		thumbnail = url.split(".")[0]+".png";
		src = "./php/files/"+nombreDis+"/CONTENIDOS/thumbnail/"+thumbnail;
	}	

	//Agregamos nuevo elemento al playlist
	var strPl = "<div pos="+position+" id="+position+" class='listaPlaylist' value='element' nombre='"+url+"'>";
		strPl += "<div class='row flexCenter'>";
			strPl += "<div class='col-xs-6 col-sm-1 listElement' align='center'>";
				strPl += "<div class='circulo'><p>"+ position +"</p></div>";
			strPl += "</div>";
			strPl += "<div class='col-xs-6 col-sm-1 listElement '>";
				strPl += "<img src='"+src+"' width='50px' height='50px' alt='img.png' style='object-fit: cover;'/>"; //"+ mensaje +"
			strPl += "</div>";
			strPl += "<div class='col-xs-6 col-sm-2 listElement'>";
				strPl += "<div style='color: #868686;'>Tipo:</div><div>"+ (tipo).toUpperCase() +"</div>";
			strPl += "</div>";
			strPl += "<div class='col-xs-6 col-sm-4 listElement'>";
				if(url == "Mensaje") {
					strPl += "<div style='color: #868686;'>Nombre:</div><div>"+ mensaje +"</div>";
				}
				else {
					strPl += "<div style='color: #868686;'>Nombre:</div><div>"+ url +"</div>";
				}
			strPl += "</div>";					
			strPl += "<div class='col-xs-6 col-sm-2 listElement'>";
				strPl += "<div style='color: #868686;'>Tiempo:</div><div>"+ tiempo +"</div>";
			strPl += "</div>";
			strPl += "<div class='col-xs-6 col-sm-2 listElement'>";
				strPl += "<div class='swapClick inline' name='subir'>";
		            strPl += "<img src='./css/images/btn_subir_02.png' height='30px' alt='btn_subir_02.png' style='margin: 2px 5px 2px 0;'/>";
		        strPl += "</div>";
		        strPl += "<div class='swapClick inline' name='bajar'>";
		            strPl += "<img src='./css/images/btn_bajar.png' height='30px' alt='btn_bajar.png' style='margin: 2px 5px 2px 0;'/>";
		        strPl += "</div>";
		        strPl += "<div class='iconClick inline'>";
		            strPl += "<img src='./css/images/btn_eliminar.png' height='30px' alt='btn_eliminar.png' style='margin: 2px 5px 2px 0;'/>";
		        strPl += "</div>";	
			strPl += "</div>";	
		strPl += "</div>";
	strPl += "</div>";
	$("#listaPlaylist").append(strPl);

	//Agregamos el listener para remover
	//$("#"+i+" .iconClick").on('click', removePlaylistElement);
	$("#"+position+" .iconClick").on('click', showModalDelete);
	$("#"+position+" .swapClick").on('click', changePosition);

	//Agregamos la data al elemento del DOM
	var obj = {
		tipo: (tipo).toUpperCase(),
		url: url,
		mensaje: mensaje,
		fondoMensaje: fondoMensaje,
		tiempo: tiempo
	}
	
	$("#listaPlaylist #"+position).data("data", obj);
}

function showModalDelete() {
	console.log("showmodal");
	var target = this;
	$("#myModal").modal();
	$("#myModal #btnOk").on('click', function () {
		removePlaylistElement(target);   
    	$("#myModal #btnOk").off('click');
    	$("#myModal").modal("hide");	     
    });    
}

function changePosition() {
	//console.log("changePosition");
	var opcion = $(this).attr("name");
	//console.log(opcion);	
	var $listelement = $(this).parent().parent().parent();
	var position = 	$listelement.find(".circulo p").text();

	if(opcion == "subir") {
		if($listelement.prev().attr("value") == "element") {
			console.log($listelement.prev());
			var prevPosition = $listelement.prev().find(".circulo p").text();
			$listelement.find(".circulo p").text(prevPosition);
			$listelement.prev().find(".circulo p").text(position);
			$listelement.insertBefore($listelement.prev());
		}
	}
	else {	
		if($listelement.next().attr("value") == "element") {	
			console.log($listelement.next());
			var nextPosition = $listelement.next().find(".circulo p").text();
			$listelement.find(".circulo p").text(nextPosition);
			$listelement.next().find(".circulo p").text(position);
			$listelement.insertAfter($listelement.next());
		}
	}
}

function showBtnUpdate() {
	var $btnUpdate = $("#btnUpdate");
	if($btnUpdate.css("display") == "none") {
		//console.log("SHOW BTN UPDATE!");
		//Hacemos show el boton de "Update"	
		$btnUpdate.removeClass('hidden').addClass('btn');
	}
}

function showModalSave() {
	var target = this;
	$("#myModalSave").modal();
	$("#myModalSave #btnOk").on('click', function () {
		//removePlaylistElement(target); 
		var nameList = document.getElementById('nameList').value;
		if(nameList == ''){

		}else
			savePlayListXML(nameList);
    	$("#myModalSave #btnOk").off('click');
    	$("#myModalSave").modal("hide");	     
    });    
}

function savePlayListXML(nameList) {

	var $lista = $("#listaPlaylist");
	var total = $lista.children().length;


	var nombre = "PREDETERMINADO";
	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var nombreDis = jsonDis.nombre;

	var XML = '<?xml version="1.0" encoding="UTF-8"?>\n';
		XML += '<player>\n';
	for(var i=0; i<total; i++){
		var child = $($lista.children()[i]);
		var data = child.data("data");
		var tipo = data.tipo;
		var url = data.url;
		var mensaje = data.mensaje;
		var fondoMensaje = data.fondoMensaje;
		var tiempo = data.tiempo;
		var inicio = data.tiempo.split("/")[0].split(" ")[0];
		var fin = data.tiempo.split("/")[1].split(" ")[1];
		//console.log("TIPO:", data.tipo);
		//console.log("NOMBRE:", data.url);
		//console.log("MENSAJE:", data.mensaje);

		XML += '\t<contenido>\n';
			XML += '\t\t<tipo>'+tipo+'</tipo>\n';
			XML += '\t\t<url>'+url+'</url>\n';
			if(url == "Mensaje") {
				XML += '\t\t<mensaje>\n';
					XML += '\t\t\t<texto>'+mensaje+'</texto>\n';
					XML += '\t\t\t<fondo>'+fondoMensaje+'</fondo>\n'
				XML += '\t\t</mensaje>\n';
			}
			XML += '\t\t<inicio>'+inicio+'</inicio>\n';
			XML += '\t\t<fin>'+fin+'</fin>\n';
		XML += '\t</contenido>\n'
	}
	XML += '</player>';

	var sendData = {
			"type":"new", 
        	"dir":"./files/"+nombreDis+"/"+nombre, 
        	"path":"./files/"+nombreDis+"/"+nombre+"/" + nameList +".xml", 
        	"content":XML
		}

	$.ajax({
        type: 'POST',
        cache: true,
        url: './php/fileManager.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        dataType : 'json',
        data: sendData,
        success: function(result) {   	
    		console.log(result);
    		/*
    		setTimeout(function() {
			   updatePlaylist(data);
			}, 100);
			*/
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
        	console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
    	}
    });
    setTimeout(function() {
   		getListPlaylist(nombreDis);
	}, 500);
    
}

//Creacion de xml de playlist
function getNewPlaylist() {
	//console.log("updatePlaylist");
	var $alert = $("#alertMessage");
	$alert.removeClass('alert-danger show').addClass('hidden');

	var target = this;
	var $lista = $("#listaPlaylist");
	var total = $lista.children().length;

	var XML = '<?xml version="1.0" encoding="UTF-8"?>\n';
		XML += '<player>\n';
	for(var i=0; i<total; i++){
		var child = $($lista.children()[i]);
		var data = child.data("data");
		var tipo = data.tipo;
		var url = data.url;
		var mensaje = data.mensaje;
		var fondoMensaje = data.fondoMensaje;
		var tiempo = data.tiempo;
		var inicio = data.tiempo.split("/")[0].split(" ")[0];
		var fin = data.tiempo.split("/")[1].split(" ")[1];
		//console.log("TIPO:", data.tipo);
		//console.log("NOMBRE:", data.url);
		//console.log("MENSAJE:", data.mensaje);

		XML += '\t<contenido>\n';
			XML += '\t\t<tipo>'+tipo.toLowerCase()+'</tipo>\n';
			XML += '\t\t<url>'+url+'</url>\n';
			if(url == "Mensaje") {
				XML += '\t\t<mensaje>\n';
					XML += '\t\t\t<texto>'+mensaje+'</texto>\n';
					XML += '\t\t\t<fondo>'+fondoMensaje+'</fondo>\n'
				XML += '\t\t</mensaje>\n';
			}
			XML += '\t\t<inicio>'+inicio+'</inicio>\n';
			XML += '\t\t<fin>'+fin+'</fin>\n';
		XML += '\t</contenido>\n'
	}
	XML += '</player>';

	//INFO SUCURSAL
	var json = jsonData.sucursal;
	var codigosuc = json.codigosuc;
	var nombre = json.nombre;

	if(nombre != "TODOS") {
		copyPlaylist(XML);
	}
	else {
		var checked = $("input[name='sucursal']:checked").length;
		console.log('checked', checked);
		if(checked) {
			copyPlaylist(XML);
		}
		else {
			var $alert = $("#alertMessagePL");
				$alert.removeClass('hidden alert-success').addClass('alert-danger show');
				$alert.html("<strong>ERROR!</strong> Seleccione una sucursal como minimo.");
				
		}
	}	

	/*
		\n = &#xA;
		\r = &#xD;
		\t = &#x9;
		space = &#x20;
	*/
}

function unCheckAll() {
	console.log('unCheckAll');
	$('#sucursalCHECK').find('input[type=checkbox]:checked').prop('checked', false);
}

function getCheckboxSelect() {
	//console.log($('input[name="sucursal"]:checked').serialize());
	var array = [];
	var isArray = true;
	$('input[name="sucursal"]:checked').each(function() {
	   console.log(this.value);
	   array.push(this.value);

	   if(this.value == "TODOS") {
		isArray = false;
	   }
	});

	if(isArray) {
		//Retornamos el arreglo de sucursales
		return array;
	}
	else {
		//Retornamos null (no arreglo)
		return null;
	}
}

function copyPlaylist(data) {
	$("#listaXML").LoadingOverlay("show");

	console.log(data);
	//INFO SUCURSAL
	var json = jsonData.sucursal;
	var codigosuc = json.codigosuc;
	var nombre = json.nombre;
	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var codigo = jsonDis.codigo;
	var nombreDis = jsonDis.nombre;

	var sendData;

	if(nombre == "TODOS") {
		//Traemos los values checkbox:checked
		var arrayChecked = getCheckboxSelect();

		var dirList = new Array();
		var pathList = new Array();
		var newPathList = new Array();

		if(arrayChecked) {
			for(var i=0; i<arrayChecked.length; i++) {
				var nombreSuc = arrayChecked[i];
				dirList.push("./files/"+nombreDis+"/"+nombreSuc);
				pathList.push("./files/"+nombreDis+"/"+nombreSuc+"/Playlist.xml");
				newPathList.push("./files/"+nombreDis+"/"+nombreSuc+"/Playlist_"+getFecha()+".xml");
			}
		}
		else {
			for(var i=0; i<jsonData.sucursalesList.length; i++) {
				var nombreSuc = jsonData.sucursalesList[i].nombre;
				dirList.push("./files/"+nombreDis+"/"+nombreSuc);
				pathList.push("./files/"+nombreDis+"/"+nombreSuc+"/Playlist.xml");
				newPathList.push("./files/"+nombreDis+"/"+nombreSuc+"/Playlist_"+getFecha()+".xml");
			}			
		}		

		//AGREGAMOS EL PLAYLIST "TODOS";
		dirList.push("./files/"+nombreDis);
		pathList.push("./files/"+nombreDis+"/Playlist.xml");
		newPathList.push("./files/"+nombreDis+"/Playlist_"+getFecha()+".xml");

		sendData = {
			"type":"batchCopy", 
        	"dirList":dirList, 
        	"pathList":pathList, 
        	"newPathList":newPathList
		}
	}
	else {
		sendData = {
			"type":"copy", 
        	"dir":"./files/"+nombreDis+"/"+nombre, 
        	"path":"./files/"+nombreDis+"/"+nombre+"/Playlist.xml", 
        	"newPath":"./files/"+nombreDis+"/"+nombre+"/Playlist_"+getFecha()+".xml"
		}
	}

	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/fileManager.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        //dataType : 'json',
        data: sendData,
        success: function(result) {   	
    		console.log(result);
    		setTimeout(function() {
			   updatePlaylist(data);
			}, 100);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
        	console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
    	}
    });
}

function updatePlaylist(data) {
	//INFO SUCURSAL
	var json = jsonData.sucursal;
	var codigosuc = json.codigosuc;
	var nombre = json.nombre;
	//INFO DISPOSITIVO
	var jsonDis = jsonData.dispositivo;
	var codigo = jsonDis.codigo;
	var nombreDis = jsonDis.nombre;

	var sendData = {};

	if(nombre == "TODOS") {
		//Traemos los values checkbox:checked
		var arrayChecked = getCheckboxSelect();

		var dirList = new Array();
		var pathList = new Array();

		if(arrayChecked) {
			for(var i=0; i<arrayChecked.length; i++) {
				var nombreSuc = arrayChecked[i];
				dirList.push("./files/"+nombreDis+"/"+nombreSuc);
				pathList.push("./files/"+nombreDis+"/"+nombreSuc+"/Playlist.xml");
			}
			dispositivoManager("update", nombreDis, '0');
		}
		else {
			for(var i=0; i<jsonData.sucursalesList.length; i++) {
				var nombreSuc = jsonData.sucursalesList[i].nombre;
				dirList.push("./files/"+nombreDis+"/"+nombreSuc);
				pathList.push("./files/"+nombreDis+"/"+nombreSuc+"/Playlist.xml");
			}
			dispositivoManager("update", nombreDis, '1');
		}
		

		dirList.push("./files/"+nombreDis);
		pathList.push("./files/"+nombreDis+"/Playlist.xml");

		sendData = {
        	"type":"batchNew", 
        	"dirList":dirList, 
        	"pathList":pathList, 
        	"content":data
        }
	}
	else {
		sendData = {
        	"type":"new", 
        	"dir":"./files/"+nombreDis+"/"+nombre, 
        	"path":"./files/"+nombreDis+"/"+nombre+"/Playlist.xml", 
        	"content":data
        }
        dispositivoManager("update", nombreDis, '0');
	}

	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/fileManager.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        //dataType : 'json',
        data: sendData,
        success: function(result) {   	
    		console.log(result);
    		log("update", "Update Playlist(s)");
    		$("#listaXML").LoadingOverlay("hide");

    		var $alert = $("#alertMessagePL");
    		$alert.removeClass('hidden alert-danger').addClass('alert-success show');
			$alert.html("<strong>EXITO!</strong> Playlist(s) actualizado.");			

        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
        	console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
        	$("#listaXML").LoadingOverlay("hide"); 
    	}
    });
}

function log(type, evento, detalle = "") {
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/log.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        data: {"type":type, "evento":evento, "detalle":detalle},
        success: function(result) {   	
    		console.log(result);
        },
    });
}

function dispositivoManager(type, dispositivo, value) {
	$.ajax({
        type: 'POST',
        cache: false,
        url: './php/dispositivoManager.php',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
        data: {"type":type, "dispositivo":dispositivo, "value":value},
        success: function(result) {   	
    		console.log(result);
        },
    });
}


//UTILS
function getFecha() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	//today = mm+'.'+dd+'.'+yyyy;
	today = yyyy+'.'+mm+'.'+dd;
	return today;
}

//REPLACE CARACTERES RAROS
function cleanString(str) {
	var caracteres_input = ["Á","É","Í","Ó","Ú","á","é","í","ó","ú",
						"Ä","Ë","Ï","Ö","Ü","ä","ë","ï","ö","ü",
						"Ê","ê","Ñ","ñ"];
	var caracteres_output = ["A","E","I","O","U","a","e","i","o","u",
						 "A","E","I","O","U","a","e","i","o","u",
						 "E","e","N","n"];
	for(var i=0; i<caracteres_input.length; i++) {
		str = reemplazar(caracteres_input[i], caracteres_output[i], str);
		//console.log("str",str,"search",caracteres_input[i],"replace",caracteres_output[i]);
	}

	console.log("cleanString",str);
	return str;		
}

function reemplazar(searchStr, replaceStr, str) {
   return str.split(searchStr).join(replaceStr); 
}
