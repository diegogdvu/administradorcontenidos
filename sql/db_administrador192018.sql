-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-01-2018 a las 18:10:41
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_administrador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido`
--

CREATE TABLE IF NOT EXISTS `contenido` (
`idContenido` int(11) NOT NULL,
  `Nombre` varchar(45) CHARACTER SET utf8 NOT NULL,
  `Estado` varchar(45) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `contenido`
--

INSERT INTO `contenido` (`idContenido`, `Nombre`, `Estado`) VALUES
(1, 'ClimaHoy', 'ACTIVO'),
(2, 'ClimaSemana', 'ACTIVO'),
(3, 'Tasas', 'ACTIVO'),
(4, 'Mensaje', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivo`
--

CREATE TABLE IF NOT EXISTS `dispositivo` (
  `codigo` int(10) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `detalles` varchar(45) DEFAULT NULL,
  `playlist` int(3) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATUS` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivo`
--

INSERT INTO `dispositivo` (`codigo`, `nombre`, `detalles`, `playlist`, `timestamp`, `STATUS`) VALUES
(1, 'DONE', 'Modulo de consulta', 0, '2018-01-19 16:33:42', 'ACTIVO'),
(2, 'WALL 3X1', 'Videos + Climas', 1, '2018-01-17 14:32:55', 'ACTIVO'),
(3, 'DUALVIEW', 'Videos + Tasas', 0, '2017-09-28 21:56:17', 'ACTIVO'),
(4, 'WALL 1X3', 'Videos + Climas', 1, '2017-10-03 18:24:44', 'ACTIVO'),
(5, 'TOUCH', 'Pantalla interactiva', 0, '2017-09-28 22:17:34', 'ACTIVO'),
(6, 'WALL 2X2', 'Videos + Climas', 0, '2017-09-28 21:55:25', 'ACTIVO'),
(7, 'WALL 2X1', 'Videos + Climas', 0, '2017-09-28 21:55:30', 'ACTIVO'),
(8, 'NOC', 'Dashboard', 0, '2017-09-28 22:16:58', 'ACTIVO'),
(9, 'DUAL_PUBLICIDAD', 'Videos', 0, '2018-01-19 17:04:55', 'ACTIVO'),
(10, 'TASAS', 'Tasas', 0, '2017-09-29 21:20:38', 'ACTIVO'),
(11, 'DUAL_PUBLICIDAD_1', 'Videos', 0, '2018-01-19 17:02:57', 'ACTIVO'),
(12, 'DUAL_PUBLICIDAD_2', 'Videos', 0, '2018-01-19 17:03:13', 'ACTIVO'),
(13, 'WALL_2X2_BANDERA', 'Videos + Climas', 0, '2018-01-19 17:03:22', 'ACTIVO'),
(14, ' WALL_2X2_BOMBERO', 'Videos + Climas', 0, '2018-01-19 17:03:28', 'ACTIVO'),
(15, 'DONE_INTERIOR', 'Modulo de consulta', 0, '2018-01-19 17:03:57', 'ACTIVO'),
(16, 'DONE_EXTERIOR', 'Modulo de consulta', 0, '2018-01-19 17:04:03', 'ACTIVO'),
(17, 'WALL_1X3_PISO_1', 'Videos + Climas', 1, '2018-01-19 17:04:13', 'ACTIVO'),
(18, 'WALL_1X3_PISO_2', 'Videos + Climas', 1, '2018-01-19 17:04:22', 'ACTIVO'),
(19, 'NOC 1', 'Dashboard', 0, '2018-01-18 18:32:13', 'ACTIVO'),
(20, 'NOC 2', 'Dashboard', 0, '2018-01-18 18:32:13', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivo_has_contenido`
--

CREATE TABLE IF NOT EXISTS `dispositivo_has_contenido` (
  `ID_DISPOSITIVO` int(10) NOT NULL,
  `ID_CONTENIDO` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dispositivo_has_contenido`
--

INSERT INTO `dispositivo_has_contenido` (`ID_DISPOSITIVO`, `ID_CONTENIDO`) VALUES
(2, 1),
(4, 1),
(6, 1),
(7, 1),
(2, 2),
(4, 2),
(6, 2),
(7, 2),
(3, 3),
(10, 3),
(2, 4),
(3, 4),
(4, 4),
(6, 4),
(7, 4),
(9, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE IF NOT EXISTS `log` (
`idlog` int(11) NOT NULL,
  `evento` varchar(45) DEFAULT NULL,
  `detalle` varchar(200) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idusuario` int(11) NOT NULL,
  `idsucursal` varchar(45) DEFAULT NULL,
  `iddispositivo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=118 ;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`idlog`, `evento`, `detalle`, `timestamp`, `idusuario`, `idsucursal`, `iddispositivo`) VALUES
(1, 'Login', NULL, '2017-07-25 18:26:49', 1, NULL, NULL),
(2, 'Upload New File', NULL, '2017-07-25 20:10:33', 1, 'ALL', '004'),
(3, 'Upload New File', NULL, '2017-07-25 20:10:36', 1, 'ALL', '004'),
(4, 'Upload New File', NULL, '2017-07-25 20:10:40', 1, 'ALL', '004'),
(5, 'Upload New File', NULL, '2017-07-25 20:11:09', 1, 'ALL', '004'),
(6, 'Upload New File', NULL, '2017-07-25 20:11:10', 1, 'ALL', '004'),
(7, 'Upload New File', NULL, '2017-07-25 20:11:10', 1, 'ALL', '004'),
(8, 'Upload New File', NULL, '2017-07-25 20:11:11', 1, 'ALL', '004'),
(9, 'Upload New File', NULL, '2017-07-25 20:11:11', 1, 'ALL', '004'),
(10, 'Upload New File', NULL, '2017-07-25 20:11:12', 1, 'ALL', '004'),
(11, 'Upload New File', NULL, '2017-07-25 20:11:12', 1, 'ALL', '004'),
(12, 'Upload New File', NULL, '2017-07-25 20:11:14', 1, 'ALL', '004'),
(13, 'Upload New File', NULL, '2017-07-25 20:11:15', 1, 'ALL', '004'),
(14, 'Upload New File', NULL, '2017-07-25 20:11:15', 1, 'ALL', '004'),
(15, 'Upload New File', NULL, '2017-07-25 20:11:30', 1, 'ALL', '004'),
(16, 'Upload New File', NULL, '2017-07-25 20:11:30', 1, 'ALL', '004'),
(17, 'Upload New File', NULL, '2017-07-25 20:11:31', 1, 'ALL', '004'),
(18, 'Login', NULL, '2017-08-02 15:29:11', 1, NULL, NULL),
(19, 'Upload New File', NULL, '2017-08-02 15:30:05', 1, 'ALL', '004'),
(20, 'Upload New File', NULL, '2017-08-02 15:30:16', 1, 'ALL', '004'),
(21, 'Upload New File', NULL, '2017-08-02 15:30:22', 1, 'ALL', '004'),
(22, 'Upload New File', NULL, '2017-08-02 15:30:33', 1, 'ALL', '004'),
(23, 'Upload New File', NULL, '2017-08-02 15:30:34', 1, 'ALL', '004'),
(24, 'Login', NULL, '2017-08-14 19:27:17', 3, NULL, NULL),
(25, 'Login', NULL, '2017-08-14 19:35:08', 3, NULL, NULL),
(26, 'Login', NULL, '2017-08-29 17:07:51', 3, NULL, NULL),
(27, 'Login', NULL, '2017-08-31 14:37:16', 3, NULL, NULL),
(28, 'Login', NULL, '2017-09-05 13:49:43', 3, NULL, NULL),
(29, 'Upload New File', NULL, '2017-09-08 16:46:26', 3, '3013011', '003'),
(30, 'Update Playlist(s)', NULL, '2017-09-08 16:47:11', 3, '3013011', '003'),
(31, 'Remove Video(s)', NULL, '2017-09-08 16:49:38', 3, 'ALL', '003'),
(32, 'Login', NULL, '2017-09-28 22:00:55', 3, NULL, NULL),
(33, 'Login', NULL, '2017-10-02 14:25:35', 3, NULL, NULL),
(34, 'Login', NULL, '2017-10-02 14:27:16', 3, NULL, NULL),
(35, 'Login', NULL, '2017-10-03 14:03:10', 3, NULL, NULL),
(36, 'Update Playlist(s)', NULL, '2017-10-03 15:19:01', 3, 'ALL', '4'),
(37, 'Update Playlist(s)', NULL, '2017-10-03 15:20:18', 3, 'ALL', '4'),
(38, 'Update Playlist(s)', NULL, '2017-10-03 15:27:53', 3, 'ALL', '4'),
(39, 'Update Playlist(s)', NULL, '2017-10-03 15:28:17', 3, 'ALL', '4'),
(40, 'Update Playlist(s)', NULL, '2017-10-03 15:29:07', 3, 'ALL', '4'),
(41, 'Update Playlist(s)', NULL, '2017-10-03 15:43:55', 3, 'ALL', '4'),
(42, 'Update Playlist(s)', NULL, '2017-10-03 15:44:48', 3, 'ALL', '4'),
(43, 'Update Playlist(s)', NULL, '2017-10-03 17:04:25', 3, 'ALL', '4'),
(44, 'Update Playlist(s)', NULL, '2017-10-03 17:08:51', 3, 'ALL', '4'),
(45, 'Update Playlist(s)', NULL, '2017-10-03 18:24:44', 3, 'ALL', '4'),
(46, 'Login', NULL, '2017-10-03 19:24:38', 3, NULL, NULL),
(47, 'Update Playlist(s)', NULL, '2017-10-03 19:36:35', 3, 'ALL', '4'),
(48, 'Update Playlist(s)', NULL, '2017-10-03 19:36:44', 3, 'ALL', '4'),
(49, 'Update Playlist(s)', NULL, '2017-10-04 17:10:13', 3, 'ALL', '4'),
(50, 'Update Playlist(s)', NULL, '2017-10-04 18:11:50', 3, 'ALL', '4'),
(51, 'Update Playlist(s)', NULL, '2017-10-04 18:12:24', 3, 'ALL', '4'),
(52, 'Update Playlist(s)', NULL, '2017-10-04 18:15:24', 3, 'ALL', '4'),
(53, 'Update Playlist(s)', NULL, '2017-10-04 18:15:34', 3, 'ALL', '4'),
(54, 'Update Playlist(s)', NULL, '2017-10-04 18:16:25', 3, 'ALL', '4'),
(55, 'Update Playlist(s)', NULL, '2017-10-04 18:16:37', 3, 'ALL', '4'),
(56, 'Update Playlist(s)', NULL, '2017-10-04 18:17:46', 3, 'ALL', '4'),
(57, 'Update Playlist(s)', NULL, '2017-10-04 18:32:28', 3, 'ALL', '4'),
(58, 'Update Playlist(s)', NULL, '2017-10-04 18:32:49', 3, 'ALL', '4'),
(59, 'Update Playlist(s)', NULL, '2017-10-04 18:32:56', 3, 'ALL', '4'),
(60, 'Update Playlist(s)', NULL, '2017-10-04 18:33:48', 3, 'ALL', '4'),
(61, 'Update Playlist(s)', NULL, '2017-10-04 18:33:59', 3, 'ALL', '4'),
(62, 'Upload New File', NULL, '2017-10-04 18:39:31', 3, 'ALL', '4'),
(63, 'Update Playlist(s)', NULL, '2017-10-04 18:39:40', 3, 'ALL', '4'),
(64, 'Update Playlist(s)', NULL, '2017-10-04 18:41:09', 3, 'ALL', '4'),
(65, 'Update Playlist(s)', NULL, '2017-10-04 18:41:18', 3, 'ALL', '4'),
(66, 'Update Playlist(s)', NULL, '2017-10-04 18:41:25', 3, 'ALL', '4'),
(67, 'Update Playlist(s)', NULL, '2017-10-04 18:41:36', 3, 'ALL', '4'),
(68, 'Update Playlist(s)', NULL, '2017-10-04 18:41:46', 3, 'ALL', '4'),
(69, 'Update Playlist(s)', NULL, '2017-10-04 19:33:21', 3, 'ALL', '4'),
(70, 'Update Playlist(s)', NULL, '2017-10-04 19:33:40', 3, 'ALL', '4'),
(71, 'Update Playlist(s)', NULL, '2017-10-04 20:06:20', 3, 'ALL', '4'),
(72, 'Update Playlist(s)', NULL, '2017-10-04 20:08:55', 3, 'ALL', '4'),
(73, 'Update Playlist(s)', NULL, '2017-10-04 20:09:28', 3, 'ALL', '4'),
(74, 'Login', NULL, '2017-10-12 15:24:58', 3, NULL, NULL),
(75, 'Remove Video(s)', NULL, '2017-10-12 15:32:01', 3, 'ALL', '2'),
(76, 'Login', NULL, '2017-10-13 14:18:24', 3, NULL, NULL),
(77, 'Update Playlist(s)', NULL, '2017-10-13 14:21:07', 3, 'ALL', '2'),
(78, 'Login', NULL, '2017-10-13 16:59:58', 3, NULL, NULL),
(79, 'Login', NULL, '2017-10-18 17:03:38', 1, NULL, NULL),
(80, 'Login', NULL, '2017-10-23 15:24:24', 3, NULL, NULL),
(81, 'Login', NULL, '2017-10-23 15:48:41', 3, NULL, NULL),
(82, 'Login', NULL, '2017-11-22 15:03:11', 3, NULL, NULL),
(83, 'Login', NULL, '2017-11-23 14:04:33', 3, NULL, NULL),
(84, 'Login', NULL, '2017-11-23 14:18:54', 3, NULL, NULL),
(85, 'Upload New File', NULL, '2017-11-23 14:19:03', 3, 'ALL', '4'),
(86, 'Upload New File', NULL, '2017-11-23 14:45:36', 3, 'ALL', '4'),
(87, 'Upload New File', NULL, '2017-11-23 14:46:56', 3, 'ALL', '4'),
(88, 'Upload New File', NULL, '2017-11-23 14:52:50', 3, 'ALL', '4'),
(89, 'Upload New File', NULL, '2017-11-23 15:11:04', 3, 'ALL', '4'),
(90, 'Upload New File', NULL, '2017-11-23 15:18:30', 3, 'ALL', '4'),
(91, 'Remove Video(s)', NULL, '2017-11-23 15:19:18', 3, 'ALL', '4'),
(92, 'Upload New File', NULL, '2017-11-23 15:19:29', 3, 'ALL', '4'),
(93, 'Login', NULL, '2018-01-17 14:21:04', 3, NULL, NULL),
(94, 'Login', NULL, '2018-01-17 14:28:32', 3, NULL, NULL),
(95, 'Update Playlist(s)', NULL, '2018-01-17 14:31:19', 3, '516', '3'),
(96, 'Update Playlist(s)', NULL, '2018-01-17 14:31:38', 3, 'ALL', '2'),
(97, 'Update Playlist(s)', NULL, '2018-01-17 14:32:16', 3, 'ALL', '2'),
(98, 'Update Playlist(s)', NULL, '2018-01-17 14:32:40', 3, 'ALL', '2'),
(99, 'Update Playlist(s)', NULL, '2018-01-17 14:32:55', 3, 'ALL', '2'),
(100, 'Login', NULL, '2018-01-17 15:43:00', 3, NULL, NULL),
(101, 'Login', NULL, '2018-01-17 15:44:54', 3, NULL, NULL),
(102, 'Login', NULL, '2018-01-17 16:36:05', 3, NULL, NULL),
(103, 'Upload New FileWALL_3X1small.mp4', NULL, '2018-01-17 16:37:18', 3, 'ALL', '2'),
(104, 'Remove Video(s) undefined', NULL, '2018-01-17 16:49:43', 3, 'ALL', '2'),
(105, 'Upload New File WALL_3X1small.mp4', NULL, '2018-01-17 16:50:02', 3, 'ALL', '2'),
(106, 'Remove Video(s) WALL_3X1small.mp4', NULL, '2018-01-17 16:55:33', 3, 'ALL', '2'),
(107, 'Remove Video(s) WALL_3X1small (3).mp4,WALL_3X', NULL, '2018-01-17 16:55:49', 3, 'ALL', '2'),
(108, 'Upload New File WALL_3X1small.mp4', '', '2018-01-17 17:06:46', 3, 'ALL', '2'),
(109, 'Upload New File ', 'WALL_3X1small (1).mp4 ', '2018-01-17 17:10:07', 3, 'ALL', '2'),
(110, 'Remove Video(s) ', 'WALL_3X1small (1).mp4,WALL_3X1small.mp4 ', '2018-01-17 17:10:20', 3, 'ALL', '2'),
(111, 'Login', NULL, '2018-01-18 13:51:48', 3, NULL, NULL),
(112, 'Login', NULL, '2018-01-18 19:24:51', 3, NULL, NULL),
(113, 'Login', NULL, '2018-01-19 14:29:07', 3, NULL, NULL),
(114, 'Login', NULL, '2018-01-19 16:24:12', 4, NULL, NULL),
(115, 'Login', NULL, '2018-01-19 16:39:17', 3, NULL, NULL),
(116, 'Login', NULL, '2018-01-19 16:44:54', 3, NULL, NULL),
(117, 'Login', NULL, '2018-01-19 16:47:36', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE IF NOT EXISTS `sucursal` (
  `codigosuc` int(10) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `detalles` varchar(45) NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`codigosuc`, `nombre`, `detalles`, `pais`, `estado`) VALUES
(0, 'TODOS', '', 'CHILE', 'ACTIVO'),
(10, 'MANUEL MONTT', '', 'CHILE', 'ACTIVO'),
(13, 'EL GOLF', '', 'CHILE', 'ACTIVO'),
(133, 'LOS DOMINICOS', '', 'CHILE', 'ACTIVO'),
(138, 'OSORNO', '', 'CHILE', 'ACTIVO'),
(158, 'UNIVERSIDAD DE CHILE', '', 'CHILE', 'ACTIVO'),
(163, 'EL FARO', '', 'CHILE', 'ACTIVO'),
(177, 'CANDELARIA', '', 'CHILE', 'ACTIVO'),
(184, 'LA REINA', '', 'CHILE', 'ACTIVO'),
(185, 'BELLAS ARTES', '', 'CHILE', 'ACTIVO'),
(311, 'AMUNATEGUI', '', 'CHILE', 'ACTIVO'),
(325, 'IQUIQUE', '', 'CHILE', 'ACTIVO'),
(326, 'VALPARAISO', '', 'CHILE', 'ACTIVO'),
(516, 'ESCUELA MILITAR', '', 'CHILE', 'ACTIVO'),
(3017011, 'TEMUCO', '', 'CHILE', 'ACTIVO'),
(3151011, 'BANDERA', '', 'CHILE', 'ACTIVO'),
(3281011, 'VIÑA DEL MAR', '', 'CHILE', 'ACTIVO'),
(3285011, 'CURICO', '', 'CHILE', 'ACTIVO'),
(3419011, 'BOMBERO OSSA', '', 'CHILE', 'ACTIVO'),
(3437011, 'PEDRO DE VALDIVIA', '', 'CHILE', 'ACTIVO'),
(3487011, 'ESTADO', 'RAMON NIETO', 'CHILE', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal_has_dispositivo`
--

CREATE TABLE IF NOT EXISTS `sucursal_has_dispositivo` (
  `ID_SUCURSAL` int(10) NOT NULL,
  `ID_DISPOSITIVO` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursal_has_dispositivo`
--

INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(3419011, 9),
(3419011, 4),
(3419011, 8),
(3419011, 1),
(3419011, 2),
(3419011, 5),
(3151011, 8),
(3151011, 1),
(3151011, 11),
(3151011, 12),
(3151011, 13),
(3151011, 14),
(3017011, 6),
(3017011, 2),
(3017011, 5),
(3017011, 8),
(3017011, 11),
(3017011, 12),
(3017011, 15),
(3017011, 16),
(3437011, 5),
(3437011, 2),
(3437011, 1),
(3437011, 6),
(3437011, 8),
(3437011, 9),
(3487011, 1),
(3487011, 9),
(3487011, 7),
(3487011, 4),
(3487011, 8),
(3281011, 7),
(3281011, 9),
(3281011, 4),
(3281011, 8),
(3281011, 1),
(3285011, 1),
(3285011, 8),
(3285011, 5),
(3285011, 9),
(3285011, 6),
(3285011, 7),
(516, 2),
(516, 5),
(516, 1),
(516, 11),
(516, 12),
(516, 17),
(516, 18),
(516, 19),
(516, 20),
(177, 1),
(177, 9),
(177, 6),
(177, 8),
(133, 1),
(133, 9),
(133, 4),
(133, 8),
(325, 1),
(325, 8),
(325, 4),
(325, 12),
(325, 11),
(184, 1),
(184, 8),
(184, 4),
(184, 12),
(184, 11),
(185, 1),
(185, 4),
(185, 12),
(185, 11),
(185, 19),
(185, 20),
(10, 1),
(10, 8),
(10, 6),
(10, 12),
(10, 11),
(158, 1),
(158, 8),
(158, 12),
(158, 11),
(158, 6),
(158, 7),
(326, 1),
(326, 8),
(326, 12),
(326, 11),
(326, 6),
(13, 1),
(13, 8),
(13, 12),
(13, 11),
(13, 6),
(13, 2),
(13, 5),
(163, 1),
(163, 8),
(163, 9),
(163, 6),
(311, 1),
(311, 8),
(311, 9),
(311, 6),
(138, 1),
(138, 8),
(138, 11),
(138, 12),
(138, 4),
(138, 7),
(138, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`IDUSUARIO` int(11) NOT NULL,
  `USUARIO` varchar(45) DEFAULT NULL,
  `NOMBRE` varchar(45) DEFAULT NULL,
  `APELLIDOS` varchar(45) DEFAULT NULL,
  `CONTRASENA` varchar(45) DEFAULT NULL,
  `PRIVILEGIOS` varchar(10) NOT NULL,
  `ESTADO` varchar(45) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`IDUSUARIO`, `USUARIO`, `NOMBRE`, `APELLIDOS`, `CONTRASENA`, `PRIVILEGIOS`, `ESTADO`) VALUES
(1, 'PXG', NULL, NULL, '3001', '1', 'ACTIVO'),
(2, 'Felipe', NULL, NULL, '3001', '1', 'ACTIVO'),
(3, 'raop', NULL, NULL, '3001', '1', 'ACTIVO'),
(4, 'test3', 'D', 'G', '3001', '1', 'ACTIVO'),
(5, 'Avidela', NULL, NULL, 'Satander2018', '1', 'ACTIVO'),
(6, 'Arivera', NULL, NULL, 'Satander2018', '1', 'ACTIVO');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contenido`
--
ALTER TABLE `contenido`
 ADD PRIMARY KEY (`idContenido`);

--
-- Indices de la tabla `dispositivo`
--
ALTER TABLE `dispositivo`
 ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `dispositivo_has_contenido`
--
ALTER TABLE `dispositivo_has_contenido`
 ADD PRIMARY KEY (`ID_DISPOSITIVO`,`ID_CONTENIDO`), ADD KEY `fk_idContenido_dhc` (`ID_CONTENIDO`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
 ADD PRIMARY KEY (`idlog`), ADD KEY `idusuario_idx` (`idusuario`), ADD KEY `iddispositivo_idx` (`iddispositivo`), ADD KEY `idsucursal_idx` (`idsucursal`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
 ADD PRIMARY KEY (`codigosuc`);

--
-- Indices de la tabla `sucursal_has_dispositivo`
--
ALTER TABLE `sucursal_has_dispositivo`
 ADD KEY `fk_idSucursal_shd` (`ID_SUCURSAL`), ADD KEY `fk_idDispositivo_shd` (`ID_DISPOSITIVO`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`IDUSUARIO`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contenido`
--
ALTER TABLE `contenido`
MODIFY `idContenido` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
MODIFY `idlog` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
MODIFY `IDUSUARIO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `dispositivo_has_contenido`
--
ALTER TABLE `dispositivo_has_contenido`
ADD CONSTRAINT `fk_idContenido_dhc` FOREIGN KEY (`ID_CONTENIDO`) REFERENCES `contenido` (`idContenido`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `fk_idDispositivo_dhc` FOREIGN KEY (`ID_DISPOSITIVO`) REFERENCES `dispositivo` (`codigo`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `sucursal_has_dispositivo`
--
ALTER TABLE `sucursal_has_dispositivo`
ADD CONSTRAINT `fk_idDispositivo_shd` FOREIGN KEY (`ID_DISPOSITIVO`) REFERENCES `dispositivo` (`codigo`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `fk_idSucursal_shd` FOREIGN KEY (`ID_SUCURSAL`) REFERENCES `sucursal` (`codigosuc`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
