-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 17, 2018 at 03:09 PM
-- Server version: 5.6.19
-- PHP Version: 5.6.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_administrador`
--

-- --------------------------------------------------------

--
-- Table structure for table `contenido`
--

CREATE TABLE `contenido` (
  `idContenido` int(11) NOT NULL,
  `Nombre` varchar(45) CHARACTER SET utf8 NOT NULL,
  `Estado` varchar(45) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contenido`
--

INSERT INTO `contenido` (`idContenido`, `Nombre`, `Estado`) VALUES
(1, 'ClimaHoy', 'ACTIVO'),
(2, 'ClimaSemana', 'ACTIVO'),
(3, 'Tasas', 'ACTIVO'),
(4, 'Mensaje', 'ACTIVO');

-- --------------------------------------------------------

--
-- Table structure for table `dispositivo`
--

CREATE TABLE `dispositivo` (
  `codigo` int(10) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `detalles` varchar(45) DEFAULT NULL,
  `playlist` int(3) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATUS` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dispositivo`
--

INSERT INTO `dispositivo` (`codigo`, `nombre`, `detalles`, `playlist`, `timestamp`, `STATUS`) VALUES
(1, 'D-ONE', 'Modulo de consulta', 0, '2017-09-28 22:17:48', 'ACTIVO'),
(2, 'WALL 3X1', 'Videos + Climas', 1, '2017-12-26 14:14:53', 'ACTIVO'),
(3, 'DUALVIEW', 'Videos + Tasas', 0, '2017-09-28 21:56:17', 'ACTIVO'),
(4, 'WALL 1X3', 'Videos + Climas', 1, '2017-10-03 18:24:44', 'ACTIVO'),
(5, 'TOUCH', 'Pantalla interactiva', 0, '2017-09-28 22:17:34', 'ACTIVO'),
(6, 'WALL 2X2', 'Videos + Climas', 1, '2017-12-26 15:16:00', 'ACTIVO'),
(7, 'WALL 2X1', 'Videos + Climas', 1, '2017-12-26 15:12:33', 'ACTIVO'),
(8, 'NOC', 'Dashboard', 0, '2017-09-28 22:16:58', 'ACTIVO'),
(9, 'DUAL_PUBLICIDAD', 'Videos', 0, '2017-09-29 21:19:49', 'ACTIVO'),
(10, 'TASAS', 'Tasas', 0, '2017-09-29 21:20:38', 'ACTIVO');

-- --------------------------------------------------------

--
-- Table structure for table `dispositivo_has_contenido`
--

CREATE TABLE `dispositivo_has_contenido` (
  `ID_DISPOSITIVO` int(10) NOT NULL,
  `ID_CONTENIDO` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dispositivo_has_contenido`
--

INSERT INTO `dispositivo_has_contenido` (`ID_DISPOSITIVO`, `ID_CONTENIDO`) VALUES
(2, 1),
(4, 1),
(6, 1),
(7, 1),
(2, 2),
(4, 2),
(6, 2),
(7, 2),
(3, 3),
(10, 3),
(2, 4),
(3, 4),
(4, 4),
(6, 4),
(7, 4),
(9, 4);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `idlog` int(11) NOT NULL,
  `evento` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idusuario` int(11) NOT NULL,
  `idsucursal` varchar(45) DEFAULT NULL,
  `iddispositivo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`idlog`, `evento`, `timestamp`, `idusuario`, `idsucursal`, `iddispositivo`) VALUES
(1, 'Login', '2017-07-25 18:26:49', 1, NULL, NULL),
(2, 'Upload New File', '2017-07-25 20:10:33', 1, 'ALL', '004'),
(3, 'Upload New File', '2017-07-25 20:10:36', 1, 'ALL', '004'),
(4, 'Upload New File', '2017-07-25 20:10:40', 1, 'ALL', '004'),
(5, 'Upload New File', '2017-07-25 20:11:09', 1, 'ALL', '004'),
(6, 'Upload New File', '2017-07-25 20:11:10', 1, 'ALL', '004'),
(7, 'Upload New File', '2017-07-25 20:11:10', 1, 'ALL', '004'),
(8, 'Upload New File', '2017-07-25 20:11:11', 1, 'ALL', '004'),
(9, 'Upload New File', '2017-07-25 20:11:11', 1, 'ALL', '004'),
(10, 'Upload New File', '2017-07-25 20:11:12', 1, 'ALL', '004'),
(11, 'Upload New File', '2017-07-25 20:11:12', 1, 'ALL', '004'),
(12, 'Upload New File', '2017-07-25 20:11:14', 1, 'ALL', '004'),
(13, 'Upload New File', '2017-07-25 20:11:15', 1, 'ALL', '004'),
(14, 'Upload New File', '2017-07-25 20:11:15', 1, 'ALL', '004'),
(15, 'Upload New File', '2017-07-25 20:11:30', 1, 'ALL', '004'),
(16, 'Upload New File', '2017-07-25 20:11:30', 1, 'ALL', '004'),
(17, 'Upload New File', '2017-07-25 20:11:31', 1, 'ALL', '004'),
(18, 'Login', '2017-08-02 15:29:11', 1, NULL, NULL),
(19, 'Upload New File', '2017-08-02 15:30:05', 1, 'ALL', '004'),
(20, 'Upload New File', '2017-08-02 15:30:16', 1, 'ALL', '004'),
(21, 'Upload New File', '2017-08-02 15:30:22', 1, 'ALL', '004'),
(22, 'Upload New File', '2017-08-02 15:30:33', 1, 'ALL', '004'),
(23, 'Upload New File', '2017-08-02 15:30:34', 1, 'ALL', '004'),
(24, 'Login', '2017-08-14 19:27:17', 3, NULL, NULL),
(25, 'Login', '2017-08-14 19:35:08', 3, NULL, NULL),
(26, 'Login', '2017-08-29 17:07:51', 3, NULL, NULL),
(27, 'Login', '2017-08-31 14:37:16', 3, NULL, NULL),
(28, 'Login', '2017-09-05 13:49:43', 3, NULL, NULL),
(29, 'Upload New File', '2017-09-08 16:46:26', 3, '3013011', '003'),
(30, 'Update Playlist(s)', '2017-09-08 16:47:11', 3, '3013011', '003'),
(31, 'Remove Video(s)', '2017-09-08 16:49:38', 3, 'ALL', '003'),
(32, 'Login', '2017-09-28 22:00:55', 3, NULL, NULL),
(33, 'Login', '2017-10-02 14:25:35', 3, NULL, NULL),
(34, 'Login', '2017-10-02 14:27:16', 3, NULL, NULL),
(35, 'Login', '2017-10-03 14:03:10', 3, NULL, NULL),
(36, 'Update Playlist(s)', '2017-10-03 15:19:01', 3, 'ALL', '4'),
(37, 'Update Playlist(s)', '2017-10-03 15:20:18', 3, 'ALL', '4'),
(38, 'Update Playlist(s)', '2017-10-03 15:27:53', 3, 'ALL', '4'),
(39, 'Update Playlist(s)', '2017-10-03 15:28:17', 3, 'ALL', '4'),
(40, 'Update Playlist(s)', '2017-10-03 15:29:07', 3, 'ALL', '4'),
(41, 'Update Playlist(s)', '2017-10-03 15:43:55', 3, 'ALL', '4'),
(42, 'Update Playlist(s)', '2017-10-03 15:44:48', 3, 'ALL', '4'),
(43, 'Update Playlist(s)', '2017-10-03 17:04:25', 3, 'ALL', '4'),
(44, 'Update Playlist(s)', '2017-10-03 17:08:51', 3, 'ALL', '4'),
(45, 'Update Playlist(s)', '2017-10-03 18:24:44', 3, 'ALL', '4'),
(46, 'Login', '2017-10-03 19:24:38', 3, NULL, NULL),
(47, 'Update Playlist(s)', '2017-10-03 19:36:35', 3, 'ALL', '4'),
(48, 'Update Playlist(s)', '2017-10-03 19:36:44', 3, 'ALL', '4'),
(49, 'Update Playlist(s)', '2017-10-04 17:10:13', 3, 'ALL', '4'),
(50, 'Update Playlist(s)', '2017-10-04 18:11:50', 3, 'ALL', '4'),
(51, 'Update Playlist(s)', '2017-10-04 18:12:24', 3, 'ALL', '4'),
(52, 'Update Playlist(s)', '2017-10-04 18:15:24', 3, 'ALL', '4'),
(53, 'Update Playlist(s)', '2017-10-04 18:15:34', 3, 'ALL', '4'),
(54, 'Update Playlist(s)', '2017-10-04 18:16:25', 3, 'ALL', '4'),
(55, 'Update Playlist(s)', '2017-10-04 18:16:37', 3, 'ALL', '4'),
(56, 'Update Playlist(s)', '2017-10-04 18:17:46', 3, 'ALL', '4'),
(57, 'Update Playlist(s)', '2017-10-04 18:32:28', 3, 'ALL', '4'),
(58, 'Update Playlist(s)', '2017-10-04 18:32:49', 3, 'ALL', '4'),
(59, 'Update Playlist(s)', '2017-10-04 18:32:56', 3, 'ALL', '4'),
(60, 'Update Playlist(s)', '2017-10-04 18:33:48', 3, 'ALL', '4'),
(61, 'Update Playlist(s)', '2017-10-04 18:33:59', 3, 'ALL', '4'),
(62, 'Upload New File', '2017-10-04 18:39:31', 3, 'ALL', '4'),
(63, 'Update Playlist(s)', '2017-10-04 18:39:40', 3, 'ALL', '4'),
(64, 'Update Playlist(s)', '2017-10-04 18:41:09', 3, 'ALL', '4'),
(65, 'Update Playlist(s)', '2017-10-04 18:41:18', 3, 'ALL', '4'),
(66, 'Update Playlist(s)', '2017-10-04 18:41:25', 3, 'ALL', '4'),
(67, 'Update Playlist(s)', '2017-10-04 18:41:36', 3, 'ALL', '4'),
(68, 'Update Playlist(s)', '2017-10-04 18:41:46', 3, 'ALL', '4'),
(69, 'Update Playlist(s)', '2017-10-04 19:33:21', 3, 'ALL', '4'),
(70, 'Update Playlist(s)', '2017-10-04 19:33:40', 3, 'ALL', '4'),
(71, 'Update Playlist(s)', '2017-10-04 20:06:20', 3, 'ALL', '4'),
(72, 'Update Playlist(s)', '2017-10-04 20:08:55', 3, 'ALL', '4'),
(73, 'Update Playlist(s)', '2017-10-04 20:09:28', 3, 'ALL', '4'),
(74, 'Login', '2017-10-12 15:24:58', 3, NULL, NULL),
(75, 'Remove Video(s)', '2017-10-12 15:32:01', 3, 'ALL', '2'),
(76, 'Login', '2017-10-13 14:18:24', 3, NULL, NULL),
(77, 'Update Playlist(s)', '2017-10-13 14:21:07', 3, 'ALL', '2'),
(78, 'Login', '2017-10-13 16:59:58', 3, NULL, NULL),
(79, 'Login', '2017-10-18 17:03:38', 1, NULL, NULL),
(80, 'Login', '2017-10-23 15:24:24', 3, NULL, NULL),
(81, 'Remove Default Playilist', '2017-10-23 16:06:45', 3, 'ALL', '2'),
(82, 'Remove Default Playilist', '2017-10-23 16:06:52', 3, 'ALL', '2'),
(83, 'Upload New File', '2017-10-23 16:08:58', 3, 'ALL', '2'),
(84, 'Update Playlist(s)', '2017-10-23 16:10:10', 3, 'ALL', '2'),
(85, 'Remove Video(s)', '2017-10-23 16:12:48', 3, 'ALL', '2'),
(86, 'Update Playlist(s)', '2017-10-23 16:15:00', 3, 'ALL', '2'),
(87, 'Login', '2017-10-24 14:10:27', 3, NULL, NULL),
(88, 'Login', '2017-11-06 12:24:51', 1, NULL, NULL),
(89, 'Login', '2017-11-06 15:47:55', 1, NULL, NULL),
(90, 'Login', '2017-11-06 15:56:07', 1, NULL, NULL),
(91, 'Login', '2017-11-08 02:57:15', 1, NULL, NULL),
(92, 'Login', '2017-11-09 12:33:27', 1, NULL, NULL),
(93, 'Login', '2017-11-09 12:35:25', 1, NULL, NULL),
(94, 'Login', '2017-11-13 13:15:53', 1, NULL, NULL),
(95, 'Login', '2017-11-22 14:10:36', 1, NULL, NULL),
(96, 'Login', '2017-11-22 14:37:44', 1, NULL, NULL),
(97, 'Login', '2017-11-22 15:32:52', 1, NULL, NULL),
(98, 'Login', '2017-11-23 13:49:24', 3, NULL, NULL),
(99, 'Login', '2017-11-23 14:00:22', 1, NULL, NULL),
(100, 'Login', '2017-11-23 14:18:10', 3, NULL, NULL),
(101, 'Login', '2017-11-23 14:58:34', 1, NULL, NULL),
(102, 'Login', '2017-11-23 14:58:34', 1, NULL, NULL),
(103, 'Login', '2017-11-23 16:41:16', 3, NULL, NULL),
(104, 'Upload New File', '2017-11-23 16:41:27', 3, 'ALL', '2'),
(105, 'Remove Video(s)', '2017-11-23 16:41:42', 3, 'ALL', '2'),
(106, 'Login', '2017-11-28 13:18:51', 1, NULL, NULL),
(107, 'Upload New File', '2017-11-28 13:20:37', 1, 'ALL', '2'),
(108, 'Login', '2017-12-07 12:50:31', 1, NULL, NULL),
(109, 'Login', '2017-12-07 15:01:42', 1, NULL, NULL),
(110, 'Update Playlist(s)', '2017-12-07 15:15:20', 1, '3151011', '6'),
(111, 'Update Playlist(s)', '2017-12-07 15:21:38', 1, '3151011', '6'),
(112, 'Login', '2017-12-11 15:07:24', 1, NULL, NULL),
(113, 'Login', '2017-12-11 15:12:54', 3, NULL, NULL),
(114, 'Upload New File', '2017-12-11 15:16:05', 3, 'ALL', '2'),
(115, 'Upload New File', '2017-12-11 15:24:46', 3, 'ALL', '2'),
(116, 'Upload New File', '2017-12-11 15:26:17', 3, 'ALL', '7'),
(117, 'Upload New File', '2017-12-11 15:33:32', 3, 'ALL', '2'),
(118, 'Upload New File', '2017-12-11 15:42:47', 3, 'ALL', '7'),
(119, 'Login', '2017-12-11 15:45:10', 1, NULL, NULL),
(120, 'Upload New File', '2017-12-11 15:49:06', 1, '3151011', '6'),
(121, 'Upload New File', '2017-12-11 15:49:48', 1, '3151011', '6'),
(122, 'Upload New File', '2017-12-11 15:59:10', 1, '3151011', '6'),
(123, 'Upload New File', '2017-12-11 16:05:23', 1, '3151011', '6'),
(124, 'Update Playlist(s)', '2017-12-11 16:08:47', 1, '3151011', '6'),
(125, 'Login', '2017-12-11 16:39:35', 1, NULL, NULL),
(126, 'Update Playlist(s)', '2017-12-11 16:45:16', 1, '3151011', '6'),
(127, 'Login', '2017-12-11 18:54:37', 1, NULL, NULL),
(128, 'Update Playlist(s)', '2017-12-11 18:56:32', 1, '3151011', '6'),
(129, 'Update Playlist(s)', '2017-12-11 18:58:35', 1, '3151011', '6'),
(130, 'Update Playlist(s)', '2017-12-11 19:10:29', 1, '3151011', '6'),
(131, 'Update Playlist(s)', '2017-12-11 19:10:40', 1, '3151011', '6'),
(132, 'Login', '2017-12-12 13:00:42', 1, NULL, NULL),
(133, 'Upload New File', '2017-12-12 13:02:49', 1, '3151011', '6'),
(134, 'Update Playlist(s)', '2017-12-12 13:02:56', 1, '3151011', '6'),
(135, 'Update Playlist(s)', '2017-12-12 13:03:34', 1, '3151011', '6'),
(136, 'Update Playlist(s)', '2017-12-12 13:05:37', 1, '3151011', '6'),
(137, 'Update Playlist(s)', '2017-12-12 13:13:26', 1, '3151011', '6'),
(138, 'Remove Video(s)', '2017-12-12 13:16:39', 1, '3151011', '6'),
(139, 'Update Playlist(s)', '2017-12-12 13:16:54', 1, '3151011', '6'),
(140, 'Remove Default Playilist', '2017-12-12 13:19:47', 1, '3151011', '6'),
(141, 'Remove Default Playilist', '2017-12-12 13:19:58', 1, '3151011', '6'),
(142, 'Update Playlist(s)', '2017-12-12 13:20:09', 1, '3151011', '6'),
(143, 'Update Playlist(s)', '2017-12-12 13:23:30', 1, '3151011', '6'),
(144, 'Upload New File', '2017-12-12 13:24:05', 1, '3151011', '6'),
(145, 'Update Playlist(s)', '2017-12-12 13:25:20', 1, '3151011', '6'),
(146, 'Update Playlist(s)', '2017-12-12 13:31:35', 1, '3151011', '6'),
(147, 'Remove Default Playilist', '2017-12-12 13:32:48', 1, '3151011', '6'),
(148, 'Remove Video(s)', '2017-12-12 13:32:54', 1, '3151011', '6'),
(149, 'Update Playlist(s)', '2017-12-12 13:33:22', 1, '3151011', '6'),
(150, 'Upload New File', '2017-12-12 13:38:04', 1, '3151011', '6'),
(151, 'Upload New File', '2017-12-12 13:39:31', 1, '3151011', '6'),
(152, 'Upload New File', '2017-12-12 13:39:35', 1, '3151011', '6'),
(153, 'Upload New File', '2017-12-12 13:39:44', 1, '3151011', '6'),
(154, 'Upload New File', '2017-12-12 13:39:52', 1, '3151011', '6'),
(155, 'Upload New File', '2017-12-12 13:40:01', 1, '3151011', '6'),
(156, 'Upload New File', '2017-12-12 13:40:02', 1, '3151011', '6'),
(157, 'Update Playlist(s)', '2017-12-12 13:41:04', 1, '3151011', '6'),
(158, 'Update Playlist(s)', '2017-12-12 13:45:56', 1, '3151011', '6'),
(159, 'Update Playlist(s)', '2017-12-12 13:50:34', 1, '3151011', '6'),
(160, 'Update Playlist(s)', '2017-12-12 13:51:45', 1, '3151011', '6'),
(161, 'Login', '2017-12-12 14:22:48', 1, NULL, NULL),
(162, 'Update Playlist(s)', '2017-12-12 14:24:46', 1, '3151011', '6'),
(163, 'Update Playlist(s)', '2017-12-12 14:40:17', 1, '3151011', '6'),
(164, 'Remove Video(s)', '2017-12-12 14:42:13', 1, '3151011', '6'),
(165, 'Update Playlist(s)', '2017-12-12 14:43:17', 1, '3151011', '6'),
(166, 'Login', '2017-12-12 15:14:45', 3, NULL, NULL),
(167, 'Update Playlist(s)', '2017-12-12 15:16:04', 3, '3151011', '6'),
(168, 'Login', '2017-12-12 15:17:26', 3, NULL, NULL),
(169, 'Update Playlist(s)', '2017-12-12 15:17:51', 3, '3151011', '6'),
(170, 'Update Playlist(s)', '2017-12-12 15:20:45', 1, '3151011', '6'),
(171, 'Update Playlist(s)', '2017-12-12 15:21:48', 1, '3151011', '6'),
(172, 'Login', '2017-12-12 15:35:40', 1, NULL, NULL),
(173, 'Update Playlist(s)', '2017-12-12 15:37:07', 1, '3151011', '6'),
(174, 'Update Playlist(s)', '2017-12-12 15:47:15', 1, '3151011', '6'),
(175, 'Update Playlist(s)', '2017-12-12 15:52:36', 1, '3151011', '6'),
(176, 'Login', '2017-12-12 16:17:29', 1, NULL, NULL),
(177, 'Update Playlist(s)', '2017-12-12 16:19:30', 1, '3151011', '6'),
(178, 'Login', '2017-12-12 18:05:39', 1, NULL, NULL),
(179, 'Update Playlist(s)', '2017-12-12 18:08:10', 1, '3151011', '6'),
(180, 'Update Playlist(s)', '2017-12-12 18:11:42', 1, '3151011', '6'),
(181, 'Update Playlist(s)', '2017-12-12 18:19:30', 1, '3151011', '6'),
(182, 'Update Playlist(s)', '2017-12-12 18:41:16', 1, '3151011', '6'),
(183, 'Update Playlist(s)', '2017-12-12 18:42:07', 1, '3151011', '6'),
(184, 'Update Playlist(s)', '2017-12-12 18:51:34', 1, '3151011', '6'),
(185, 'Update Playlist(s)', '2017-12-12 19:05:51', 1, '3151011', '6'),
(186, 'Login', '2017-12-12 19:11:44', 1, NULL, NULL),
(187, 'Login', '2017-12-13 17:41:20', 1, NULL, NULL),
(188, 'Upload New File', '2017-12-13 17:44:06', 1, '3151011', '6'),
(189, 'Upload New File', '2017-12-13 17:44:41', 1, '3151011', '6'),
(190, 'Update Playlist(s)', '2017-12-13 17:46:17', 1, '3151011', '6'),
(191, 'Update Playlist(s)', '2017-12-13 17:49:17', 1, '3151011', '6'),
(192, 'Login', '2017-12-13 17:54:51', 1, NULL, NULL),
(193, 'Update Playlist(s)', '2017-12-13 17:57:53', 1, '3151011', '6'),
(194, 'Update Playlist(s)', '2017-12-13 18:07:01', 1, '3151011', '6'),
(195, 'Login', '2017-12-13 18:11:42', 1, NULL, NULL),
(196, 'Login', '2017-12-13 18:15:50', 1, NULL, NULL),
(197, 'Login', '2017-12-13 18:15:50', 1, NULL, NULL),
(198, 'Login', '2017-12-13 18:15:50', 1, NULL, NULL),
(199, 'Update Playlist(s)', '2017-12-13 18:18:07', 1, '3151011', '6'),
(200, 'Update Playlist(s)', '2017-12-13 18:18:44', 1, '3151011', '6'),
(201, 'Update Playlist(s)', '2017-12-13 18:19:01', 1, '3151011', '6'),
(202, 'Login', '2017-12-13 18:25:39', 1, NULL, NULL),
(203, 'Upload New File', '2017-12-13 18:27:56', 1, 'ALL', '6'),
(204, 'Update Playlist(s)', '2017-12-13 18:29:31', 1, '3151011', '6'),
(205, 'Login', '2017-12-13 18:31:19', 1, NULL, NULL),
(206, 'Upload New File', '2017-12-13 18:32:00', 1, '3151011', '6'),
(207, 'Update Playlist(s)', '2017-12-13 18:32:57', 1, 'ALL', '6'),
(208, 'Update Playlist(s)', '2017-12-13 18:34:08', 1, '3151011', '6'),
(209, 'Update Playlist(s)', '2017-12-13 18:34:49', 1, '3151011', '6'),
(210, 'Update Playlist(s)', '2017-12-13 18:35:01', 1, '3151011', '6'),
(211, 'Update Playlist(s)', '2017-12-13 18:35:14', 1, '3151011', '6'),
(212, 'Update Playlist(s)', '2017-12-13 18:37:15', 1, '3151011', '6'),
(213, 'Update Playlist(s)', '2017-12-13 18:51:56', 1, '3151011', '6'),
(214, 'Update Playlist(s)', '2017-12-13 18:52:26', 1, '3151011', '6'),
(215, 'Update Playlist(s)', '2017-12-13 18:52:52', 1, '3151011', '6'),
(216, 'Update Playlist(s)', '2017-12-13 18:53:21', 1, '3151011', '6'),
(217, 'Remove Video(s)', '2017-12-13 18:54:28', 1, '3151011', '6'),
(218, 'Update Playlist(s)', '2017-12-13 18:57:47', 1, 'ALL', '6'),
(219, 'Update Playlist(s)', '2017-12-13 18:58:22', 1, '3151011', '6'),
(220, 'Upload New File', '2017-12-13 19:29:44', 1, 'ALL', '6'),
(221, 'Upload New File', '2017-12-13 19:30:29', 1, 'ALL', '6'),
(222, 'Upload New File', '2017-12-13 19:31:40', 1, 'ALL', '6'),
(223, 'Upload New File', '2017-12-13 19:33:00', 1, 'ALL', '6'),
(224, 'Remove Video(s)', '2017-12-13 19:33:41', 1, 'ALL', '6'),
(225, 'Upload New File', '2017-12-13 19:35:11', 1, 'ALL', '6'),
(226, 'Remove Video(s)', '2017-12-13 19:35:55', 1, 'ALL', '6'),
(227, 'Upload New File', '2017-12-13 19:36:26', 1, 'ALL', '6'),
(228, 'Upload New File', '2017-12-13 19:37:13', 1, 'ALL', '6'),
(229, 'Upload New File', '2017-12-13 19:38:21', 1, 'ALL', '6'),
(230, 'Upload New File', '2017-12-13 19:39:29', 1, 'ALL', '6'),
(231, 'Upload New File', '2017-12-13 19:52:15', 1, 'ALL', '6'),
(232, 'Upload New File', '2017-12-13 19:54:32', 1, 'ALL', '6'),
(233, 'Upload New File', '2017-12-13 19:55:06', 1, 'ALL', '6'),
(234, 'Upload New File', '2017-12-13 19:55:38', 1, 'ALL', '6'),
(235, 'Upload New File', '2017-12-13 19:56:11', 1, 'ALL', '6'),
(236, 'Upload New File', '2017-12-13 19:57:03', 1, 'ALL', '6'),
(237, 'Upload New File', '2017-12-13 19:57:34', 1, 'ALL', '6'),
(238, 'Upload New File', '2017-12-13 19:58:43', 1, 'ALL', '6'),
(239, 'Remove Video(s)', '2017-12-13 19:59:22', 1, 'ALL', '6'),
(240, 'Upload New File', '2017-12-13 20:00:27', 1, 'ALL', '6'),
(241, 'Upload New File', '2017-12-13 20:01:42', 1, 'ALL', '6'),
(242, 'Remove Video(s)', '2017-12-13 20:02:29', 1, 'ALL', '6'),
(243, 'Upload New File', '2017-12-13 20:02:58', 1, 'ALL', '6'),
(244, 'Upload New File', '2017-12-13 20:04:14', 1, 'ALL', '6'),
(245, 'Upload New File', '2017-12-13 20:05:19', 1, 'ALL', '6'),
(246, 'Upload New File', '2017-12-13 21:11:36', 1, 'ALL', '6'),
(247, 'Upload New File', '2017-12-13 21:12:52', 1, 'ALL', '6'),
(248, 'Upload New File', '2017-12-13 21:14:30', 1, 'ALL', '6'),
(249, 'Upload New File', '2017-12-13 21:15:50', 1, 'ALL', '6'),
(250, 'Upload New File', '2017-12-13 21:16:59', 1, 'ALL', '6'),
(251, 'Upload New File', '2017-12-13 21:19:00', 1, 'ALL', '6'),
(252, 'Upload New File', '2017-12-13 21:22:01', 1, 'ALL', '6'),
(253, 'Upload New File', '2017-12-13 21:22:36', 1, 'ALL', '6'),
(254, 'Upload New File', '2017-12-13 21:24:37', 1, 'ALL', '6'),
(255, 'Upload New File', '2017-12-13 21:25:38', 1, 'ALL', '6'),
(256, 'Upload New File', '2017-12-13 21:26:20', 1, 'ALL', '6'),
(257, 'Upload New File', '2017-12-13 21:27:52', 1, 'ALL', '6'),
(258, 'Upload New File', '2017-12-13 21:31:06', 1, 'ALL', '6'),
(259, 'Upload New File', '2017-12-13 21:31:46', 1, 'ALL', '6'),
(260, 'Remove Video(s)', '2017-12-13 21:32:33', 1, 'ALL', '6'),
(261, 'Upload New File', '2017-12-13 21:33:15', 1, 'ALL', '6'),
(262, 'Login', '2017-12-26 13:51:27', 1, NULL, NULL),
(263, 'Update Playlist(s)', '2017-12-26 14:02:15', 1, 'ALL', '6'),
(264, 'Update Playlist(s)', '2017-12-26 14:02:41', 1, 'ALL', '6'),
(265, 'Update Playlist(s)', '2017-12-26 14:05:16', 1, 'ALL', '6'),
(266, 'Upload New File', '2017-12-26 14:07:30', 1, 'ALL', '2'),
(267, 'Remove Video(s)', '2017-12-26 14:07:48', 1, 'ALL', '2'),
(268, 'Update Playlist(s)', '2017-12-26 14:14:53', 1, 'ALL', '2'),
(269, 'Update Playlist(s)', '2017-12-26 14:15:43', 1, 'ALL', '2'),
(270, 'Upload New File', '2017-12-26 14:16:09', 1, 'ALL', '2'),
(271, 'Upload New File', '2017-12-26 14:16:30', 1, 'ALL', '2'),
(272, 'Upload New File', '2017-12-26 14:16:55', 1, 'ALL', '2'),
(273, 'Upload New File', '2017-12-26 14:17:33', 1, 'ALL', '2'),
(274, 'Upload New File', '2017-12-26 14:17:48', 1, 'ALL', '2'),
(275, 'Upload New File', '2017-12-26 14:18:15', 1, 'ALL', '2'),
(276, 'Upload New File', '2017-12-26 14:18:53', 1, 'ALL', '2'),
(277, 'Upload New File', '2017-12-26 14:19:12', 1, 'ALL', '2'),
(278, 'Update Playlist(s)', '2017-12-26 14:19:22', 1, 'ALL', '2'),
(279, 'Update Playlist(s)', '2017-12-26 14:19:47', 1, 'ALL', '2'),
(280, 'Upload New File', '2017-12-26 14:20:09', 1, 'ALL', '2'),
(281, 'Upload New File', '2017-12-26 14:20:34', 1, 'ALL', '2'),
(282, 'Upload New File', '2017-12-26 14:20:59', 1, 'ALL', '2'),
(283, 'Upload New File', '2017-12-26 14:21:15', 1, 'ALL', '2'),
(284, 'Upload New File', '2017-12-26 14:21:53', 1, 'ALL', '2'),
(285, 'Upload New File', '2017-12-26 14:22:15', 1, 'ALL', '2'),
(286, 'Remove Video(s)', '2017-12-26 14:22:56', 1, 'ALL', '2'),
(287, 'Upload New File', '2017-12-26 14:23:53', 1, 'ALL', '2'),
(288, 'Upload New File', '2017-12-26 14:24:05', 1, 'ALL', '2'),
(289, 'Upload New File', '2017-12-26 14:24:45', 1, 'ALL', '2'),
(290, 'Upload New File', '2017-12-26 14:24:51', 1, 'ALL', '2'),
(291, 'Remove Video(s)', '2017-12-26 14:25:42', 1, 'ALL', '2'),
(292, 'Remove Video(s)', '2017-12-26 14:25:58', 1, 'ALL', '2'),
(293, 'Remove Video(s)', '2017-12-26 14:26:21', 1, 'ALL', '2'),
(294, 'Remove Video(s)', '2017-12-26 14:26:24', 1, 'ALL', '2'),
(295, 'Remove Video(s)', '2017-12-26 14:26:27', 1, 'ALL', '2'),
(296, 'Upload New File', '2017-12-26 14:29:10', 1, 'ALL', '2'),
(297, 'Upload New File', '2017-12-26 14:29:36', 1, 'ALL', '2'),
(298, 'Upload New File', '2017-12-26 14:29:52', 1, 'ALL', '2'),
(299, 'Upload New File', '2017-12-26 14:30:09', 1, 'ALL', '2'),
(300, 'Upload New File', '2017-12-26 14:30:20', 1, 'ALL', '2'),
(301, 'Upload New File', '2017-12-26 14:31:03', 1, 'ALL', '2'),
(302, 'Upload New File', '2017-12-26 14:31:36', 1, 'ALL', '2'),
(303, 'Update Playlist(s)', '2017-12-26 14:31:54', 1, 'ALL', '2'),
(304, 'Upload New File', '2017-12-26 14:33:01', 1, 'ALL', '2'),
(305, 'Upload New File', '2017-12-26 14:33:28', 1, 'ALL', '2'),
(306, 'Upload New File', '2017-12-26 14:34:05', 1, 'ALL', '2'),
(307, 'Upload New File', '2017-12-26 14:34:41', 1, 'ALL', '2'),
(308, 'Upload New File', '2017-12-26 14:36:14', 1, 'ALL', '6'),
(309, 'Upload New File', '2017-12-26 14:37:49', 1, 'ALL', '2'),
(310, 'Upload New File', '2017-12-26 14:37:56', 1, 'ALL', '2'),
(311, 'Upload New File', '2017-12-26 14:38:03', 1, 'ALL', '2'),
(312, 'Upload New File', '2017-12-26 14:38:10', 1, 'ALL', '2'),
(313, 'Upload New File', '2017-12-26 14:38:18', 1, 'ALL', '2'),
(314, 'Upload New File', '2017-12-26 14:38:25', 1, 'ALL', '2'),
(315, 'Update Playlist(s)', '2017-12-26 14:38:31', 1, 'ALL', '2'),
(316, 'Update Playlist(s)', '2017-12-26 14:39:38', 1, 'ALL', '2'),
(317, 'Update Playlist(s)', '2017-12-26 14:41:19', 1, 'ALL', '2'),
(318, 'Update Playlist(s)', '2017-12-26 14:41:33', 1, 'ALL', '2'),
(319, 'Update Playlist(s)', '2017-12-26 14:43:13', 1, 'ALL', '2'),
(320, 'Update Playlist(s)', '2017-12-26 14:52:47', 1, 'ALL', '2'),
(321, 'Update Playlist(s)', '2017-12-26 14:53:45', 1, 'ALL', '2'),
(322, 'Update Playlist(s)', '2017-12-26 14:54:20', 1, 'ALL', '2'),
(323, 'Update Playlist(s)', '2017-12-26 14:55:00', 1, 'ALL', '2'),
(324, 'Update Playlist(s)', '2017-12-26 14:55:28', 1, 'ALL', '2'),
(325, 'Upload New File', '2017-12-26 14:57:30', 1, 'ALL', '7'),
(326, 'Upload New File', '2017-12-26 14:57:37', 1, 'ALL', '7'),
(327, 'Upload New File', '2017-12-26 14:57:44', 1, 'ALL', '7'),
(328, 'Upload New File', '2017-12-26 14:57:53', 1, 'ALL', '7'),
(329, 'Upload New File', '2017-12-26 14:58:01', 1, 'ALL', '7'),
(330, 'Upload New File', '2017-12-26 14:58:08', 1, 'ALL', '7'),
(331, 'Upload New File', '2017-12-26 14:58:59', 1, 'ALL', '7'),
(332, 'Upload New File', '2017-12-26 14:59:19', 1, 'ALL', '7'),
(333, 'Upload New File', '2017-12-26 14:59:38', 1, 'ALL', '7'),
(334, 'Upload New File', '2017-12-26 15:00:22', 1, 'ALL', '7'),
(335, 'Upload New File', '2017-12-26 15:01:08', 1, 'ALL', '7'),
(336, 'Upload New File', '2017-12-26 15:02:16', 1, 'ALL', '7'),
(337, 'Upload New File', '2017-12-26 15:06:34', 1, 'ALL', '7'),
(338, 'Upload New File', '2017-12-26 15:06:44', 1, 'ALL', '7'),
(339, 'Upload New File', '2017-12-26 15:06:56', 1, 'ALL', '7'),
(340, 'Upload New File', '2017-12-26 15:07:09', 1, 'ALL', '7'),
(341, 'Upload New File', '2017-12-26 15:07:20', 1, 'ALL', '7'),
(342, 'Upload New File', '2017-12-26 15:07:30', 1, 'ALL', '7'),
(343, 'Upload New File', '2017-12-26 15:07:57', 1, 'ALL', '7'),
(344, 'Upload New File', '2017-12-26 15:08:12', 1, 'ALL', '7'),
(345, 'Upload New File', '2017-12-26 15:09:04', 1, 'ALL', '7'),
(346, 'Upload New File', '2017-12-26 15:09:26', 1, 'ALL', '7'),
(347, 'Upload New File', '2017-12-26 15:09:44', 1, 'ALL', '7'),
(348, 'Upload New File', '2017-12-26 15:10:11', 1, 'ALL', '7'),
(349, 'Upload New File', '2017-12-26 15:10:27', 1, 'ALL', '7'),
(350, 'Upload New File', '2017-12-26 15:10:49', 1, 'ALL', '7'),
(351, 'Remove Video(s)', '2017-12-26 15:12:26', 1, 'ALL', '7'),
(352, 'Update Playlist(s)', '2017-12-26 15:12:33', 1, 'ALL', '7'),
(353, 'Update Playlist(s)', '2017-12-26 15:14:49', 1, 'ALL', '7'),
(354, 'Update Playlist(s)', '2017-12-26 15:15:20', 1, '3013011', '6'),
(355, 'Update Playlist(s)', '2017-12-26 15:16:00', 1, 'ALL', '6'),
(356, 'Update Playlist(s)', '2017-12-26 15:18:51', 1, 'ALL', '7'),
(357, 'Login', '2017-12-26 19:07:12', 1, NULL, NULL),
(358, 'Login', '2018-01-10 13:43:02', 1, NULL, NULL),
(359, 'Login', '2018-01-11 12:47:55', 1, NULL, NULL),
(360, 'Upload New File', '2018-01-11 12:52:52', 1, 'ALL', '9'),
(361, 'Remove Video(s)', '2018-01-11 12:53:42', 1, 'ALL', '9'),
(362, 'Login', '2018-01-17 15:06:12', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sucursal`
--

CREATE TABLE `sucursal` (
  `codigosuc` int(10) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `detalles` varchar(45) NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sucursal`
--

INSERT INTO `sucursal` (`codigosuc`, `nombre`, `detalles`, `pais`, `estado`) VALUES
(0, 'TODOS', '', 'CHILE', 'ACTIVO'),
(516, 'ESCUELA MILITAR', '', 'CHILE', 'ACTIVO'),
(3013011, 'EL GOLF', '', 'CHILE', 'ACTIVO'),
(3017011, 'TEMUCO', '', 'CHILE', 'ACTIVO'),
(3151011, 'BANDERA', '', 'CHILE', 'ACTIVO'),
(3281011, 'VIÑA DEL MAR', '', 'CHILE', 'ACTIVO'),
(3285011, 'CURICO', '', 'CHILE', 'ACTIVO'),
(3419011, 'BOMBERO OSSA', '', 'CHILE', 'ACTIVO'),
(3437011, 'PEDRO DE VALDIVIA', '', 'CHILE', 'ACTIVO'),
(3487011, 'ESTADO', 'RAMON NIETO', 'CHILE', 'ACTIVO');

-- --------------------------------------------------------

--
-- Table structure for table `sucursal_has_dispositivo`
--

CREATE TABLE `sucursal_has_dispositivo` (
  `ID_SUCURSAL` int(10) NOT NULL,
  `ID_DISPOSITIVO` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sucursal_has_dispositivo`
--

INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(3419011, 3),
(3419011, 4),
(3419011, 8),
(3419011, 1),
(3419011, 2),
(3419011, 5),
(3151011, 6),
(3151011, 3),
(3151011, 8),
(3151011, 1),
(3017011, 3),
(3017011, 9),
(3017011, 6),
(3017011, 2),
(3017011, 5),
(3017011, 8),
(3017011, 1),
(3437011, 5),
(3437011, 2),
(3437011, 1),
(3437011, 6),
(3437011, 8),
(3437011, 3),
(3013011, 10),
(3013011, 9),
(3013011, 5),
(3013011, 1),
(3013011, 8),
(3013011, 6),
(3013011, 2),
(3487011, 1),
(3487011, 3),
(3487011, 7),
(3487011, 4),
(3487011, 8),
(3281011, 7),
(3281011, 3),
(3281011, 4),
(3281011, 8),
(3281011, 1),
(3285011, 1),
(3285011, 3),
(3285011, 8),
(3285011, 6),
(3285011, 5),
(3285011, 7),
(516, 3),
(516, 2),
(516, 5),
(516, 1),
(516, 4),
(516, 8);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `IDUSUARIO` int(11) NOT NULL,
  `USUARIO` varchar(45) DEFAULT NULL,
  `CONTRASENA` varchar(45) DEFAULT NULL,
  `PRIVILEGIOS` varchar(10) NOT NULL,
  `ESTADO` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`IDUSUARIO`, `USUARIO`, `CONTRASENA`, `PRIVILEGIOS`, `ESTADO`) VALUES
(1, 'PXG', '3001', '1', 'ACTIVO'),
(2, 'Felipe', '3001', '1', 'ACTIVO'),
(3, 'raop', '3001', '1', 'ACTIVO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contenido`
--
ALTER TABLE `contenido`
  ADD PRIMARY KEY (`idContenido`);

--
-- Indexes for table `dispositivo`
--
ALTER TABLE `dispositivo`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `dispositivo_has_contenido`
--
ALTER TABLE `dispositivo_has_contenido`
  ADD PRIMARY KEY (`ID_DISPOSITIVO`,`ID_CONTENIDO`),
  ADD KEY `fk_idContenido_dhc` (`ID_CONTENIDO`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `idusuario_idx` (`idusuario`),
  ADD KEY `iddispositivo_idx` (`iddispositivo`),
  ADD KEY `idsucursal_idx` (`idsucursal`);

--
-- Indexes for table `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`codigosuc`);

--
-- Indexes for table `sucursal_has_dispositivo`
--
ALTER TABLE `sucursal_has_dispositivo`
  ADD KEY `fk_idSucursal_shd` (`ID_SUCURSAL`),
  ADD KEY `fk_idDispositivo_shd` (`ID_DISPOSITIVO`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`IDUSUARIO`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contenido`
--
ALTER TABLE `contenido`
  MODIFY `idContenido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `idlog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=363;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `IDUSUARIO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dispositivo_has_contenido`
--
ALTER TABLE `dispositivo_has_contenido`
  ADD CONSTRAINT `fk_idContenido_dhc` FOREIGN KEY (`ID_CONTENIDO`) REFERENCES `contenido` (`idContenido`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idDispositivo_dhc` FOREIGN KEY (`ID_DISPOSITIVO`) REFERENCES `dispositivo` (`codigo`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `sucursal_has_dispositivo`
--
ALTER TABLE `sucursal_has_dispositivo`
  ADD CONSTRAINT `fk_idDispositivo_shd` FOREIGN KEY (`ID_DISPOSITIVO`) REFERENCES `dispositivo` (`codigo`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idSucursal_shd` FOREIGN KEY (`ID_SUCURSAL`) REFERENCES `sucursal` (`codigosuc`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
