-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-09-2017 a las 16:39:40
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_administrador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido`
--

CREATE TABLE `contenido` (
  `idContenido` int(11) NOT NULL,
  `Nombre` varchar(45) CHARACTER SET utf8 NOT NULL,
  `Estado` varchar(45) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contenido`
--

INSERT INTO `contenido` (`idContenido`, `Nombre`, `Estado`) VALUES
(1, 'ClimaHoy', 'ACTIVO'),
(2, 'ClimaSemana', 'ACTIVO'),
(3, 'Tasas', 'ACTIVO'),
(4, 'Mensaje', 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivo`
--

CREATE TABLE `dispositivo` (
  `codigo` varchar(45) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `detalles` varchar(45) DEFAULT NULL,
  `playlist` int(3) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATUS` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivo`
--

INSERT INTO `dispositivo` (`codigo`, `nombre`, `detalles`, `playlist`, `timestamp`, `STATUS`) VALUES
('001', 'D-ONE', 'Modulo de consulta', 0, '2017-09-28 22:17:48', 'ACTIVO'),
('002', 'WALL 3X1', 'Videos + Climas', 0, '2017-09-28 21:55:13', 'ACTIVO'),
('003', 'DUALVIEW', 'Videos + Tasas', 0, '2017-09-28 21:56:17', 'ACTIVO'),
('004', 'WALL 1X3', 'Videos + Climas', 0, '2017-09-28 21:55:21', 'ACTIVO'),
('005', 'TOUCH', 'Pantalla interactiva', 0, '2017-09-28 22:17:34', 'ACTIVO'),
('006', 'WALL 2X2', 'Videos + Climas', 0, '2017-09-28 21:55:25', 'ACTIVO'),
('007', 'WALL 2X1', 'Videos + Climas', 0, '2017-09-28 21:55:30', 'ACTIVO'),
('008', 'NOC', 'Dashboard', 0, '2017-09-28 22:16:58', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivo_has_contenido`
--

CREATE TABLE `dispositivo_has_contenido` (
  `ID_DISPOSITIVO` varchar(45) CHARACTER SET utf8 NOT NULL,
  `ID_CONTENIDO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dispositivo_has_contenido`
--

INSERT INTO `dispositivo_has_contenido` (`ID_DISPOSITIVO`, `ID_CONTENIDO`) VALUES
('002', 1),
('002', 2),
('002', 4),
('003', 3),
('003', 4),
('004', 1),
('004', 2),
('004', 4),
('006', 1),
('006', 2),
('006', 4),
('007', 1),
('007', 2),
('007', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `idlog` int(11) NOT NULL,
  `evento` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idusuario` int(11) NOT NULL,
  `idsucursal` varchar(45) DEFAULT NULL,
  `iddispositivo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`idlog`, `evento`, `timestamp`, `idusuario`, `idsucursal`, `iddispositivo`) VALUES
(1, 'Login', '2017-07-25 18:26:49', 1, NULL, NULL),
(2, 'Upload New File', '2017-07-25 20:10:33', 1, 'ALL', '004'),
(3, 'Upload New File', '2017-07-25 20:10:36', 1, 'ALL', '004'),
(4, 'Upload New File', '2017-07-25 20:10:40', 1, 'ALL', '004'),
(5, 'Upload New File', '2017-07-25 20:11:09', 1, 'ALL', '004'),
(6, 'Upload New File', '2017-07-25 20:11:10', 1, 'ALL', '004'),
(7, 'Upload New File', '2017-07-25 20:11:10', 1, 'ALL', '004'),
(8, 'Upload New File', '2017-07-25 20:11:11', 1, 'ALL', '004'),
(9, 'Upload New File', '2017-07-25 20:11:11', 1, 'ALL', '004'),
(10, 'Upload New File', '2017-07-25 20:11:12', 1, 'ALL', '004'),
(11, 'Upload New File', '2017-07-25 20:11:12', 1, 'ALL', '004'),
(12, 'Upload New File', '2017-07-25 20:11:14', 1, 'ALL', '004'),
(13, 'Upload New File', '2017-07-25 20:11:15', 1, 'ALL', '004'),
(14, 'Upload New File', '2017-07-25 20:11:15', 1, 'ALL', '004'),
(15, 'Upload New File', '2017-07-25 20:11:30', 1, 'ALL', '004'),
(16, 'Upload New File', '2017-07-25 20:11:30', 1, 'ALL', '004'),
(17, 'Upload New File', '2017-07-25 20:11:31', 1, 'ALL', '004'),
(18, 'Login', '2017-08-02 15:29:11', 1, NULL, NULL),
(19, 'Upload New File', '2017-08-02 15:30:05', 1, 'ALL', '004'),
(20, 'Upload New File', '2017-08-02 15:30:16', 1, 'ALL', '004'),
(21, 'Upload New File', '2017-08-02 15:30:22', 1, 'ALL', '004'),
(22, 'Upload New File', '2017-08-02 15:30:33', 1, 'ALL', '004'),
(23, 'Upload New File', '2017-08-02 15:30:34', 1, 'ALL', '004'),
(24, 'Login', '2017-08-14 19:27:17', 3, NULL, NULL),
(25, 'Login', '2017-08-14 19:35:08', 3, NULL, NULL),
(26, 'Login', '2017-08-29 17:07:51', 3, NULL, NULL),
(27, 'Login', '2017-08-31 14:37:16', 3, NULL, NULL),
(28, 'Login', '2017-09-05 13:49:43', 3, NULL, NULL),
(29, 'Upload New File', '2017-09-08 16:46:26', 3, '3013011', '003'),
(30, 'Update Playlist(s)', '2017-09-08 16:47:11', 3, '3013011', '003'),
(31, 'Remove Video(s)', '2017-09-08 16:49:38', 3, 'ALL', '003'),
(32, 'Login', '2017-09-28 22:00:55', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `codigosuc` varchar(45) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `detalles` varchar(45) NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`codigosuc`, `nombre`, `detalles`, `pais`, `estado`) VALUES
('3013011', 'EL GOLF', '', 'CHILE', 'ACTIVO'),
('3017011', 'TEMUCO', '', 'CHILE', 'ACTIVO'),
('3151011', 'BANDERA', '', 'CHILE', 'ACTIVO'),
('3281011', 'VIÑA DEL MAR', '', 'CHILE', 'ACTIVO'),
('3285011', 'CURICO', '', 'CHILE', 'ACTIVO'),
('3419011', 'BOMBERO OSSA', '', 'CHILE', 'ACTIVO'),
('3437011', 'PEDRO DE VALDIVIA', '', 'CHILE', 'ACTIVO'),
('3487011', 'ESTADO', 'RAMON NIETO', 'CHILE', 'ACTIVO'),
('516', 'ESCUELA MILITAR', '', 'CHILE', 'ACTIVO'),
('ALL', 'TODOS', '', 'CHILE', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal_has_dispositivo`
--

CREATE TABLE `sucursal_has_dispositivo` (
  `SUCURSAL_IDSUCURSAL` varchar(45) NOT NULL,
  `DISPOSITIVO_IDDISPOSITIVO` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursal_has_dispositivo`
--

INSERT INTO `sucursal_has_dispositivo` (`SUCURSAL_IDSUCURSAL`, `DISPOSITIVO_IDDISPOSITIVO`) VALUES
('3013011', '001'),
('3013011', '002'),
('3013011', '003'),
('3013011', '005'),
('3013011', '006'),
('3013011', '008'),
('3017011', '001'),
('3017011', '002'),
('3017011', '003'),
('3017011', '005'),
('3017011', '006'),
('3017011', '008'),
('3151011', '001'),
('3151011', '003'),
('3151011', '006'),
('3151011', '008'),
('3281011', '001'),
('3281011', '003'),
('3281011', '004'),
('3281011', '007'),
('3281011', '008'),
('3285011', '001'),
('3285011', '003'),
('3285011', '005'),
('3285011', '006'),
('3285011', '007'),
('3285011', '008'),
('3419011', '001'),
('3419011', '002'),
('3419011', '003'),
('3419011', '004'),
('3419011', '005'),
('3419011', '008'),
('3437011', '001'),
('3437011', '002'),
('3437011', '003'),
('3437011', '005'),
('3437011', '006'),
('3437011', '008'),
('3487011', '001'),
('3487011', '003'),
('3487011', '004'),
('3487011', '007'),
('3487011', '008'),
('516', '001'),
('516', '002'),
('516', '003'),
('516', '004'),
('516', '005'),
('516', '008');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `IDUSUARIO` int(11) NOT NULL,
  `USUARIO` varchar(45) DEFAULT NULL,
  `CONTRASENA` varchar(45) DEFAULT NULL,
  `PRIVILEGIOS` varchar(10) NOT NULL,
  `ESTADO` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`IDUSUARIO`, `USUARIO`, `CONTRASENA`, `PRIVILEGIOS`, `ESTADO`) VALUES
(1, 'PXG', '3001', '1', 'ACTIVO'),
(2, 'Felipe', '3001', '1', 'ACTIVO'),
(3, 'raop', '3001', '1', 'ACTIVO');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contenido`
--
ALTER TABLE `contenido`
  ADD PRIMARY KEY (`idContenido`);

--
-- Indices de la tabla `dispositivo`
--
ALTER TABLE `dispositivo`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `dispositivo_has_contenido`
--
ALTER TABLE `dispositivo_has_contenido`
  ADD PRIMARY KEY (`ID_DISPOSITIVO`,`ID_CONTENIDO`),
  ADD KEY `fk_idContenido` (`ID_CONTENIDO`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `idusuario_idx` (`idusuario`),
  ADD KEY `iddispositivo_idx` (`iddispositivo`),
  ADD KEY `idsucursal_idx` (`idsucursal`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`codigosuc`);

--
-- Indices de la tabla `sucursal_has_dispositivo`
--
ALTER TABLE `sucursal_has_dispositivo`
  ADD PRIMARY KEY (`SUCURSAL_IDSUCURSAL`,`DISPOSITIVO_IDDISPOSITIVO`),
  ADD KEY `fk_SUCURSAL_has_DISPOSITIVO_DISPOSITIVO1_idx` (`DISPOSITIVO_IDDISPOSITIVO`),
  ADD KEY `fk_SUCURSAL_has_DISPOSITIVO_SUCURSAL_idx` (`SUCURSAL_IDSUCURSAL`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`IDUSUARIO`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contenido`
--
ALTER TABLE `contenido`
  MODIFY `idContenido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `idlog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `IDUSUARIO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `dispositivo_has_contenido`
--
ALTER TABLE `dispositivo_has_contenido`
  ADD CONSTRAINT `fk_idContenido` FOREIGN KEY (`ID_CONTENIDO`) REFERENCES `contenido` (`idContenido`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idDispositivo` FOREIGN KEY (`ID_DISPOSITIVO`) REFERENCES `dispositivo` (`codigo`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `iddispositivo` FOREIGN KEY (`iddispositivo`) REFERENCES `dispositivo` (`codigo`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `idsucursal` FOREIGN KEY (`idsucursal`) REFERENCES `sucursal` (`codigosuc`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `idusuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`IDUSUARIO`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `sucursal_has_dispositivo`
--
ALTER TABLE `sucursal_has_dispositivo`
  ADD CONSTRAINT `fk_SUCURSAL_has_DISPOSITIVO_DISPOSITIVO1` FOREIGN KEY (`DISPOSITIVO_IDDISPOSITIVO`) REFERENCES `dispositivo` (`codigo`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idsucursal` FOREIGN KEY (`SUCURSAL_IDSUCURSAL`) REFERENCES `sucursal` (`codigosuc`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
