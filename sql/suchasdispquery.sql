
--  BOMBERO OSSA
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(3419011, 9), 
(3419011, 4),
(3419011, 8),
(3419011, 1),
(3419011, 2),
(3419011, 5);

--  Bandera
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(3151011, 8), 
(3151011, 1),
(3151011, 9),
(3151011, 12),
(3151011, 6),
(3151011, 14);

 --  Temuco
 INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(3017011, 6),
(3017011, 2),
(3017011, 5),
(3017011, 8),
(3017011, 9),
(3017011, 12),
(3017011, 15),
(3017011, 16);

--  PEDRO DE VALDIVIA
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(3437011, 5),
(3437011, 2),
(3437011, 1),
(3437011, 6),
(3437011, 8),
(3437011, 9);

--  RAMON NIETO ESTADO
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(3487011, 1),
(3487011, 9),
(3487011, 7),
(3487011, 4),
(3487011, 8);

--  VIÑA
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(3281011, 7),
(3281011, 9),
(3281011, 4),
(3281011, 8),
(3281011, 1);

--  CURICO
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(3285011, 1),
(3285011, 8),
(3285011, 5),
(3285011, 9),
(3285011, 6),
(3285011, 7);


--  ESCULEA MILITAR
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(516, 2),
(516, 5),
(516, 1),
(516, 9),
(516, 12),
(516, 4),
(516, 18),
(516, 19),
(516, 20);

--  CANDELARIA
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(177, 1),
(177, 9),
(177, 6),
(177, 8);

--  LOS DOMINICOS
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(133, 1),
(133, 9),
(133, 4),
(133, 8);

--  IQUIQUE
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(325, 1),
(325, 8),
(325, 4),
(325, 12),
(325, 9);

-- LA REINA
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(184, 1),
(184, 8),
(184, 4),
(184, 12),
(184, 9);

-- BELLAS ARTES
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(185, 1),
(185, 4),
(185, 12),
(185, 9),
(185, 19),
(185, 20);

-- MANUEL MONTT
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(10, 1),
(10, 8),
(10, 6),
(10, 12),
(10, 9);

-- U DE CHILE
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(158, 1),
(158, 8),
(158, 12),
(158, 9),
(158, 6),
(158, 7);

-- VALPARAISO
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(326, 1),
(326, 8),
(326, 12),
(326, 9),
(326, 6);

-- EL GOLF
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(13, 1),
(13, 8),
(13, 12),
(13, 9),
(13, 6),
(13, 2),
(13, 5);

-- EL FARO
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(163, 1),
(163, 8),
(163, 9),
(163, 6);

-- AMUNATEGUI
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(311, 1),
(311, 8),
(311, 9),
(311, 6);

-- AMUNATEGUI
INSERT INTO `sucursal_has_dispositivo` (`ID_SUCURSAL`, `ID_DISPOSITIVO`) VALUES
(138, 1),
(138, 8),
(138, 9),
(138, 12),
(138, 4),
(138, 7),
(138, 5);