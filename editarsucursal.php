<!DOCTYPE html>
<?php 
error_reporting(0);
session_start();

?>
<html>
<head>
	<title>Editar Sucursal</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!-- No escala -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!--<link rel="icon" href="../../favicon.ico">-->
	<!-- Bootstrap core CSS -->
	<link href="lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">

	<!--plugin style css radio button checkboxes-->
	<link rel="stylesheet" href="lib/font-awesome-4.7.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="lib/bootstrap-checkbox-styled/build.css"/>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

	<!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">


	<script src="lib/jquery/jquery-3.2.1.min.js"></script>
	<!--Plugins - Validador de FORMULARIOS JQuery -->
	<script src="lib/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="lib/jquery-form/dist/jquery.form.min.js"></script>
	<script type='text/javascript' src="js/editarsucursal.js"></script>


	<script type="text/javascript">

		$(function(){

			$('#addNewSucursal').validate({ 
			 	errorClass: "error-form",
	    		validClass: "valid-form",
		        rules: {
		        	uid: {
		                required: true
		            }

		        },
		        submitHandler: function(form) { 
		        var checked = $("input[name='dispositivo']:checked").length;
				console.log('checked', checked);
				var array = [];
				if(checked>0){
					$('input[name="dispositivo"]:checked').each(function() {
						   array.push(this.value);
					});
				}
				console.log(array);

				var sendData ={};
					sendData = {
						"codSuc": document.getElementById('codsucu').value,
						"dispositivos":array
					}


					
					$.ajax({  
						type:'POST',
						cache:false,
						url: './php/editDispToSucursal.php',
						contentType: "application/x-www-form-urlencoded; charset=UTF-8",
						dataType: 'json',
						data: sendData,
						success: function(result) {
			            	console.log(result);  
			            	//log

			            	console.log("dispositivos agregados");   
			            	//modal para redireccionar a algun sitio
			            },
			            error: function(result){
			            	console.log(result);
			            	console.log("error datos");  
			            }

					});
				}

		    });

		});


		//on clic funcion
		function deleteModal(){
			//var salida = document.getElementById('nomsuc').value;
			//console.log('codsuc', salida);
			$("#myModalPopUpDesicion").modal();
    	}


    	function editSuc(){
    		var checked = $("input[name='dispositivo']:checked").length;
				console.log('checked', checked);
				var array = [];
				if(checked>0){
					$('input[name="dispositivo"]:checked').each(function() {
						   array.push(this.value);
					});
				}
				console.log(array);

				var sendData ={};
					sendData = {
						"codSuc": document.getElementById('codsucu').value,
						"dispositivos":array
					}


					
					$.ajax({  
						type:'POST',
						cache:false,
						url: './php/editDispToSucursal.php',
						contentType: "application/x-www-form-urlencoded; charset=UTF-8",
						dataType: 'json',
						data: sendData,
						success: function(result) {
							var $idsucursal = document.getElementById('codsucu').value;
			            	console.log(result);     
			            	//log
			            	log("editSucursal", "Edit sucursal ", $idsucursal + " ");  	  
			            	console.log("dispositivos agregados");   
			            	//modal para redireccionar a algun sitio
			            },
			            error: function(result){
			            	console.log(result);
			            	console.log("error datos");  
			            }

					});
    	}


	</script>	
</head>
<body>

	<!--NAV BAR-->
	<?php include('navbar.php') ?>
    <div class="nav-separador"></div>   	
     <!-- -->   
     <!--LOGO WORKCAFE-->
    <?php include('logobar.php') ?>
  	<!-- -->  

	<div class="container">
	
		<div id="contenedorOpcion">
				
			<div id="playlist">
				<h4 class="italic">Editar Sucursal: </h4>
				<div class="cabecera-separator"></div>
				
				<div id="alertMessagePL" class="alert alert-success hidden"></div>
				<div id="newElement"/>		

					<div id="formNewElement" class="formNewElement well">
						<form id="addNewSucursal" method="" action="">
								<input hidden="true" type="text" name="uid" value = <?php echo $_SESSION["IDUSUARIO"] ?>> </input>
								<input hidden="true" type="text" name="idsuc" id="codsucu" value = <?php echo $_SESSION["sucursal"]["codigosuc"] ?>> </input>
						    	<p></p>
								<p></p>
								<label class="control-label" style=''>Nombre Sucursal:</label>
						    	<div><input class="form-control" type="text" readonly name="nomsucursal" value="<?php echo $_SESSION["sucursal"]["nombre"] ?>"></div>
						    	<p></p>
								<p></p>
								<label class="control-label" style=''>Pais Sucursal:</label>
						    	<div><input class="form-control" type="text" readonly name="paisSuc" value="CHILE"></div>
						    	<p></p>
								<p></p>

								<div id="dispositivoCHECK" class="form-group ">
									<label class="control-label" style='color: #868686;'>Dispositivos:</label>
									<div name="listsuc" id="dispositivosList"></div>
								</div>
								<button type="submit" class="btn btn-success" onclick="deleteModal()" >Actualizar</button>
						</form>				
					</div>	

				</div>
			</div>
		</div>
		<br/>


		<!-- Trigger the modal with a button -->
		<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" 
		data-target="#myModal">Open Modal</button> -->

		<!-- Modal Message-->

	
	</div>	
		
		<!--Modal-->
		<div id="myModalPopUpDesicion" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Editar?</h4>
		      </div>
		      <div class="modal-body">
		        <p>¿Estás seguro que deseas guardar los cambios?</p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-success" onclick="onClickAccept()">OK</button>
		        <button type="button" class="btn btn-danger" onclick="onClickDiscard()" data-dismiss="modal">Cancel</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div id="myModalDataSuccess" class="modal fade" role="dialog">
			  <div class="modal-dialog">

			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Mensaje</h4>
			      </div>
			      <div class="modal-body">
			        <p>Sucursal editada</p>
			      </div>
			      <div class="modal-footer">
			      	<button type="button" class="btn btn-success" onclick="onClickDataSuccess()">OK</button>
			      </div>
			    </div>

			  </div>
		</div>
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>

	<script src="lib/file-upload/js/vendor/jquery.ui.widget.js"></script>

	<script src="lib/file-upload/js/jquery.iframe-transport.js"></script>

	<script type="text/javascript">
		function onClickDiscard(){
			window.location.href = 'editarsucursal';
		}

		function onClickDataSuccess(){
			window.location.href = 'administrador';
		}

		function onClickAccept(){
			editSuc();
			$("#myModalDataSuccess").modal();
			//window.location.href = 'administrador';
		}

		function log(type, evento, idsucursal, detalle = "") {
			$.ajax({
		        type: 'POST',
		        cache: false,
		        url: './php/log.php',
		        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
		        data: {"type":type, "evento":evento, "idsucursal":idsucursal, "detalle":detalle},
		        success: function(result) {   	
		    		console.log(result);
		        },
		    });
		}


	</script>
	<!-- The basic File Upload plugin -->

</body>
</html>
