<?php 
error_reporting(0);
session_start(); 
if(!isset($_SESSION["USUARIO"])) {
  $login = "<script>";
  $login .= "window.location.href = 'index'";
  $login .= "</script>";
  echo $login;  
}
?>
<nav class="navbar navbar-default navbar-fixed-top navbar-custom">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
	  <div id="logoSantander">
		<a href="./administrador">
			<img src="./css/images/logo_santander_top.png" alt="logo_santander_top.png"/>
		</a>
	  </div>
      <!--<a class="navbar-brand" href="./administrador">SANTANDER</a>-->
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <!--<ul class="nav navbar-nav">
        <li class="active"><a href="./administrador">Home</a></li>
      </ul>-->
      <ul class="nav navbar-nav navbar-right">
          <li id="showusers" style="visibility: hidden"><a href="./editarUsuarios" ><span class="glyphicon glyphicon-edit"></span> Usuarios</a></li>

		    <li><a href="./profile"><span  class="glyphicon glyphicon-user"></span> <?php echo $_SESSION["USUARIO"] ?></a></li>
        <input hidden="true" type="text" name="privilegio" id="priv" value = <?php echo $_SESSION["PRIVILEGIOS"]?>> </input>
        <li><a href="./index"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
      <script type="text/javascript">
        if(document.getElementById("priv").value > 1){
            document.getElementById("showusers").style = "visibility: visible";
        }
      </script>


    </div><!--/.nav-collapse -->
 </div>
</nav>


