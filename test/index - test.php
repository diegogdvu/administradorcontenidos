<!DOCTYPE html>
<?php 
session_start();
session_destroy(); 
?>
<html>
<head>
	<title>Monitoreo</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!-- No escala -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!--<link rel="icon" href="../../favicon.ico">-->
	<!-- Bootstrap core CSS -->
	<link href="lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

	<!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">


	<script src="lib/jquery/jquery-3.2.1.min.js"></script>
	<!--Plugins - Validador de FORMULARIOS JQuery -->
	<script src="lib/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="lib/jquery-form/dist/jquery.form.min.js"></script>
	<script type="text/javascript">

	$(function(){
		//Ready
		// Initialize the plugin
		 $('#loginForm').validate({ 
		 	errorClass: "error-form",
    		validClass: "valid-form",
	        rules: {
	            u: {
	                required: true,
	            },
	            p: {
	                required: true,
	                minlength: 4
	            }
	        },
	        submitHandler: function(form) { 	           
	           //return false; // for demo
	           var $alert = $("#alertMessage");
	           $alert.removeClass('show').addClass('hidden');

	           /*
	           $(form).ajaxSubmit({
		            type: 'POST',
		            cache: false,
		            url: './php/login.php',
		            //url: 'http://localhost/web/Monitoreo/json/monitor.json',
		            contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
		            dataType : 'json',
		            data: $(form).serialize(),
		            success: function(result) {
		            	console.log(result.result);
		            	var resultado = result.result;
		            	if(resultado == "MATCH") {
		            		window.location.replace("administrador");	
		            	}
		            	else if(resultado == "NOT MATCH"){
		            		//var $alert = $("#alertMessage");
							$alert.html("<strong>ERROR!</strong> Usuario y/o contraseña incorrecta.");
							$alert.removeClass('hidden').addClass('show');
		            	}
		            	else {
		            		$alert.html("<strong>ERROR!</strong> Usuario no registrado.");
							$alert.removeClass('hidden').addClass('show');
		            	}	            	
		            	//window.location.href = "administrador";           
		            },
		        });
				*/

				window.location.replace("administrador");	
        	}
	    });
	});

	</script>
</head>
<body class="body-login">
	<div class="login">
	    <h1>Welcome</h1>
	    <br/>
	    <form id="loginForm" method="" action="">
	    	<input type="text" name="u" placeholder="Username" required/>
	        <input type="password" name="p" placeholder="Password"/>
	        <button type="submit" class="btn btn-primary btn-block btn-large"><span class="glyphicon glyphicon-log-in"></span> Login</button>
	    </form>
	    <br>
	    <div id="alertMessage" class="alert alert-danger hidden"></div>
	</div>
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
</html>
