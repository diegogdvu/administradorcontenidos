<?php 
session_start(); 
$_SESSION["USUARIO"] = "Administrador";
if(!isset($_SESSION["USUARIO"])) {
  $login = "<script>";
  $login .= "window.location.href = 'index'";
  $login .= "</script>";
  echo $login;  
}
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="./administrador">ADMINISTRADOR</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="./administrador">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
		    <li><a><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION["USUARIO"] ?></a></li>
        <li><a href="./index"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div><!--/.nav-collapse -->
 </div>
</nav>


