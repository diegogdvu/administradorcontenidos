<!DOCTYPE html>
<html>
<head>
	<title>DISPOSITIVO</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!-- No escala -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!--<link rel="icon" href="../../favicon.ico">-->
	<!-- Bootstrap core CSS -->
	<link href="lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

	<!--plugin style css radio button checkboxes-->
	<link rel="stylesheet" href="lib/font-awesome-4.7.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="lib/bootstrap-checkbox-styled/build.css"/>
	

	<!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">
	<!--file uploader -->
	<link href="lib/file-upload/css/jquery.fileupload.css" rel="stylesheet">
	<!--jquery-->
	<script src="lib/jquery/jquery-3.2.1.min.js"></script>	

	<script src="lib/jquery/jquery-latest.js"></script>
	<script src="js/jquery.easyPaginate.js"></script>
	<!--overlay-->
	<script src="lib/jquery-loading-overlay-1.5.3/js/loadingoverlay.min.js"></script>
	<script src="lib/jquery-loading-overlay-1.5.3/js/loadingoverlay_progress.min.js"></script>
	<!--controller-->
	<script type='text/javascript' src="js/dispositivo.js"></script>	
</head>

<body>

	<!--NAV BAR-->
	<?php include('navbar.php') ?>
    <div class="nav-separador"></div>   
     <!-- -->   
     <!--LOGO WORKCAFE-->
    <?php include('logobar.php') ?>
  	<!-- -->  

	<div class="container">



		<div id="titulo">
			<h4 id="tituloSucursal" class="italic">Sucursal:</h4>
			<div class="cabecera-separator"></div>
		</div>
		<br/>
		<div id="tituloDispositivo" class="subCabecera fontLight">Tecnología:</div>
		<br/>		
		<div id="contenedorOpcion">
			<div id="upload">
			    <!-- The fileinput-button span is used to style the file input field as button -->
			    <span class="btn btn-primary fileinput-button">
			        <!--<i class="glyphicon glyphicon-upload"></i>-->
			        <span>Add cont...</span>
			        <!-- The file input field used as target for the file upload widget -->
			        <!--<input id="fileupload" type="file" name="files[]" accept="image/png" multiple>-->
			        <input id="fileupload" type="file" name="files[]" accept="video/*"/>
			    </span>
			    <br>
			    <br>
			    <!-- The global progress bar -->
			    <div id="progress" class="progress">
			        <div class="progress-bar progress-bar-success"></div>
			    </div>
			    <!-- The container for the uploaded files -->
			    <div id="files" class="files"></div>
			    <br>
			</div>		
			<div id="playlist">
				<h4 class="italic">Playlist:</h4>
				<div class="cabecera-separator"></div>
				<br/>
				<select id="sucursalSELECT" class="form-control hidden listeado">
					<option value="NUEVO" selected>Nuevo playlist...</option>
					<!--<option value="" disabled>──────────</option>-->
					<option value="" disabled>Aplicar playlist existente...</option>
				</select>

				<br/>
				<select id="playlistSELECT" class="form-control listeado">
					<option value="NUEVO" selected>Elegir playlist guardado...</option>
					<option value="" disabled>Aplicar playlist existente...</option>

				</select>
				<br/>
				<div id="listaXML" class="well">
					<div id="listaPlaylist" ></div>
					<!--<div id="btnUpdate" class="btn btn-info hidden">-->
					<div id="sucursalCHECK" class="form-group hidden">
						<label class="control-label" style='color: #868686;'>Sucursales:</label>
						<div id="sucursalesList"></div>
					</div>
					<div id="btnUpdate" class="btn btn-success">
						<i class="glyphicon glyphicon-repeat"></i>
						Update
					</div>
					<div id="btnSave" class="btn btn-info">
						<i class="glyphicon glyphicon-upload"></i>
						Save as Default...
					</div>

					<div id="btnDeleteDefault"  class="btn btn-danger">
						<i class="glyphicon glyphicon-remove"></i>
						Delete Default...
					</div>

					<div id="btnClear" class="btn btn-danger">
						<i class="glyphicon glyphicon-remove"></i>
						Clear 
					</div>
				</div>
				

				<div id="alertMessagePL" class="alert alert-success hidden"></div>
				<div id="newElement"/>					
					<div id="formNewElement" class="formNewElement well">
							<div class="form-group">
								<label style="font-size: 14px;">Agregar Contenido</label>
							</div>
							<div class="form-group">
								<label class="control-label" style=''>Tipo:</label>
								<select id="tipoSELECT" class="form-control">
								  <option value="VIDEO">VIDEO</option>
								  <option value="CONTENIDO">CONTENIDO</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label" style=''>Nombre:</label>
								<div id="divSelect" class="form-group">									
									<div class="row">
									    <div class="form-group col-xs-6 col-sm-4">
											<label id="labelSortAlpha" class="control-label" style='color: #868686;'>Orden alafabetico:</label>
			                                <select id="tipoSORT" class="form-control">
											  <option value="" disabled selected>Elige una opcion...</option>
											  <option value="Ascendente">Ascendente</option>
											  <option value="Descendente">Descendente</option>
											</select>
									    </div>
									    <div class="form-group col-xs-6 col-sm-4">
											<label id="labelSortDate" class="control-label" style='color: #868686;'>Fecha:</label>
			                                <select id="tipoSORTDATE" class="form-control">
			                                  <option value="" disabled selected>Elige una opcion...</option>
											  <option value="Ascendente">Ascendente</option>
											  <option value="Descendente">Descendente</option>
											</select>
									    </div>
									    <div class="form-group col-xs-12 col-sm-4">
											<label class="control-label" style='color: #868686;'>Display:</label>
											<br/>
									        <div id="btnListVideos" class="btn btn-primary">
												<i class="glyphicon glyphicon-th-list"></i>	
												List											
											</div>
											<div id="btnGridVideos" class="btn btn-primary">
												<i class="glyphicon glyphicon-th"></i>	
												Grid											
											</div>									        
									    </div>
									</div>
									<div id="nombreINPUT" class="show">			
									</div>	
									<div id="paginationNav">
										<div class="pages"></div>
									</div>							
								</div>
								<select id="nombreSELECT" class="hidden form-control">
									<option value="ELIGE" disabled selected>Elige contenido...</option>
								</select>
							</div>
							<div id="infoMensaje" class="hidden form-group">
								<label class="control-label">Descripción:</label>
								<input id="tituloINPUT" class="form-control" type="text" name="titulo" placeholder="Titulo" maxlength="25">
								<br/>
								<input id="mensajeINPUT" class="form-control" type="text" name="mensaje" placeholder="Mensaje" maxlength="140">
								<br/>
								<select id="fondoSELECT" class="form-control">
									<option value="" disabled selected>Elige fondo...</option>
									<option value="CYAN_FONDO.mov">CYAN</option>
									<option value="NARANJO_FONDO.mov" >NARANJO</option>
								</select>
							</div>	

							<div class="form-horizontal">
								<div class="form-group">									
									<div class="col-xs-12 col-sm-12">
										<label class="control-label">Tiempo:</label>
										<span class="checkbox checkbox-danger maxWidth">			
											<input type="checkbox" value="Indefinido" id="checkTiempo" checked>							
											<label for="checkTiempo">Indefinido</label>
										</span>
									</div>

								</div>
								<div class="form-group">
									<div class="col-sm-1">
										<label class="control-label" style='color: #868686;'>Inicio:</label>
									</div>
									<div class="col-sm-11">
										<input id="inicioDATE" class="form-control" type="date" name="inicio">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-1">
										<label class="control-label" style='color: #868686;'>Fin:</label>
									</div>
									<div class="col-sm-11">
										<input id="finDATE" class="form-control" type="date" name="fin">
									</div>
								</div>
							</div>						

							<div id="addElement" class="btn btn-success">
								<i class="glyphicon glyphicon-plus"></i>
								Add to Playlist
							</div>
							<div id="removeElement" class="btn btn-danger">
								<i class="glyphicon glyphicon-remove"></i>
								Remove Video
							</div>								
					</div>				
					<div id="alertMessage" class="alert alert-danger hidden"></div>
				</div>
			</div>
		</div>
		<br/>


		<!-- Trigger the modal with a button -->
		<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" 
		data-target="#myModal">Open Modal</button> -->

		<!-- Modal Message-->
		<div id="myModalSave" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Grabar?</h4>
		      </div>
		      <div class="modal-body">
		        <p>¿Con que nombre deseas grabar está lista?</p>
		        <input required="required" maxlength="50" id="nameList" type="text" value=""><br>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" id="btnOk" class="btn btn-success">OK</button>
		        <button type="button" id="btnCancel"class="btn btn-danger" data-dismiss="modal">Cancel</button>
		      </div>
		    </div>

		  </div>
		</div>

		

		<!-- Modal Video-->
		<div id="myModalVideo" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        
		        <div class="modal-title">
		        	<div class="title text-overflow">
		        		Video Name
		       		</div>
		        </div>
		        
		      </div>
		      <div class="modal-body">
		        <video id="videoPopup" class="videopopup" controls preload="metadata">
				  <source type="video/mp4" src="" />
				  Your browser does not support the video tag.
				</video>
		      </div>

		    </div>

		  </div>
		</div>
	</div>	

	<!-- Modal Video Remove-->
		<div id="myModalVideoRemove" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Eliminar?</h4>
		      </div>
		      <div class="modal-body">
		        <p>¿Estás seguro que deseas eliminar definitivamente este(os) video(s) del servidor?</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" id="btnOk" class="btn btn-success">OK</button>
		        <button type="button" id="btnCancel"class="btn btn-danger" data-dismiss="modal">Cancel</button>
		      </div>
		    </div>

		  </div>
		</div>

		

		<!-- Modal Message-->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Eliminar?</h4>
		      </div>
		      <div class="modal-body">
		        <p>¿Estás seguro que deseas eliminar este elemento?</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" id="btnOk" class="btn btn-success">OK</button>
		        <button type="button" id="btnCancel"class="btn btn-danger" data-dismiss="modal">Cancel</button>
		      </div>
		    </div>

		  </div>
		</div>

	

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->


	<!-- SCRIPTS PARA LA SUBIDA DE ARCHIVOS/LOAD IMAGE -->
	<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
	<script src="lib/file-upload/js/vendor/jquery.ui.widget.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="lib/load-image/js/load-image.all.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
				<!--<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>-->
	<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
				<!--<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->

	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="lib/file-upload/js/jquery.iframe-transport.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="lib/file-upload/js/jquery.fileupload.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="lib/file-upload/js/jquery.fileupload-process.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="lib/file-upload/js/jquery.fileupload-image.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="lib/file-upload/js/jquery.fileupload-audio.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="lib/file-upload/js/jquery.fileupload-video.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="lib/file-upload/js/jquery.fileupload-validate.js"></script>

	<script>
		var $videoThumbnail;
		/*jslint unparam: true, regexp: true */
		/*global window, $ */
		$(function () {
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = 'php/',
		        uploadButton = $('<button/>')
		            .addClass('btn btn-primary')
		            .prop('disabled', true)
		            .text('Processing...')
		            .on('click', function () {
		                var $this = $(this),
		                    data = $this.data();
		                $this
		                    .off('click')
		                    .text('Abort')
		                    .on('click', function () {
		                        $this.remove();
		                        data.abort();
		                    });
		                data.submit().always(function () {
		                    $this.remove();
		                });
		            });
		    $('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
		        autoUpload: false,
		        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mov|mp4)$/i,
		        //maxFileSize: 999000,
		        maxFileSize: 200000000, //500MB
		        // Enable image resizing, except for Android and Opera,
		        // which actually support image resizing, but fail to
		        // send Blob objects via XHR requests:
		        disableImageResize: /Android(?!.*Chrome)|Opera/
		            .test(window.navigator.userAgent),
		        previewMaxWidth: 100,
		        previewMaxHeight: 100,
		        previewCrop: true
		    }).on('fileuploadadd', function (e, data) {
		    	console.log("Archivos >>",data);
		        data.context = $('<div/>').appendTo('#files');

		        $.each(data.files, function (index, file) {
		            var node = $('<p/>').append($('<span/>').text(file.name));
		            if (!index) {
		                node.append('<br>')
		                    .append(uploadButton.clone(true).data(data));
		            }
		            node.appendTo(data.context);


		        });
		    }).on('fileuploadprocessalways', function (e, data) {
		        	var index = data.index,
		            file = data.files[index],
		            node = $(data.context.children()[index]);
		            var techNameElement = document.getElementById('tituloDispositivo');
	            	var techName = techNameElement.textContent;
	            	techName = techName.substr(techName.indexOf(":")+2);
	            	techName = techName.replace(' ','_');  //Comentar esta linea para quitar guion bajo nombre
      				console.log(file.name); 
      				console.log(techName); 
      				
		        if (file.preview) {
		        	console.log("file.preview",file.preview); 	
	            	console.log("file.name",file.name); 
      				
	            	//console.log(getNameTechnology()); 
	            	var name = file.name;
		            var extension = name.split(".").pop();
	            	if(extension == "mp4" || extension == "mov") {
	            		node
		                .prepend('<br>')
		                //.prepend(file.preview);
		            }
		            else {
		            	node
		                .prepend('<br>')
		                .prepend(file.preview);
		            }		            
		        }
		        if (file.error) {
		        	console.log(file.error);
		            node
		                .append('<br>')
		                .append($('<span class="text-danger"/>').text(file.error));
		        }
		        if (index + 1 === data.files.length) {
		            data.context.find('button')
		                .text('Upload')
		                .prop('disabled', !!data.files.error);
		            //Volvemos a 0% la progress bar
		            $('#progress .progress-bar').css('width','0%');
		        }
		        console.log("NOMBRE ANTES " +techName);
		        if(techName[techName.length -1]=='1'){
		        	techName= techName.slice(0, -2);
		        	console.log("NOMBRE DESPUES " +techName);
		        }
		        if(!(file.name.toLowerCase().indexOf(techName.toLowerCase()) >= 0)  ){

		       		var error = $('<span class="text-danger"/>').text('Formato de nombre de video incorrecto, debe ser ' + techName);
		           	$(data.context.children()[index]).append('<br>').append(error);
		           	data.context.find('button').remove();
		           	//$(data.context.children()[index]).remove();
		            return;
		        	
      					
      			}


		    }).on('fileuploadprogressall', function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#progress .progress-bar').css(
		            'width',
		            progress + '%'
		        );
		    }).on('fileuploaddone', function (e, data) {
		        $.each(data.result.files, function (index, file) {
		            if (file.url) {
		            	var link;
		            	var url = file.url;
		            	var extension = url.split(".").pop();
		            	if(extension == "mp4" || extension == "mov") {
		            		link = $('<video id="videoUpload" class="videoupdate" controls preload="metadata"><source src="'+file.url+'" type="video/mp4"></video>');
		            		$(data.context.children()[index]).append(link);
		            		var padre = $(data.context.children()[index]).parent();
		            		padre.addClass("inlineBlock videoupdateCont");
		            		//console.log("padre--->",padre);

		            		//Variable para crear thumbnail		            		
		            		getThumbnail(file.name, file.url);
		            	}
		            	else {
		            		link = $('<a>').attr('target', '_blank').prop('href', file.url);
		            		$(data.context.children()[index]).wrap(link);
		            	}     		                
		                console.log(file.url);		                
		                log("upload", "Upload New File ", file.name + " " );
		            } else if (file.error) {
		            	console.log(file.error);
		                var error = $('<span class="text-danger"/>').text(file.error);
		                $(data.context.children()[index]).append('<br>').append(error);
		            }		            
		        });
		    }).on('fileuploadfail', function (e, data) {
		        $.each(data.files, function (index) {
		            var error = $('<span class="text-danger"/>').text('File upload failed.');
		            $(data.context.children()[index]).append('<br>').append(error);
		        });
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});

	//CREATE VIDEO THUMBNAIL
	function getThumbnail(name, src) {
		//console.log("getThumbnail",jsonData);
		//INFO DISPOSITIVO
		var jsonDis = jsonData.dispositivo;
		var codigo = jsonDis.codigo;
		var nombreDis = jsonDis.nombre;

		var canvas_elem = $('<canvas class="snapshot-generator hidden"></canvas>')
							.appendTo(document.body)[0];
		var $video = $('<video muted class="snapshot-generator hidden"></video>')
							.appendTo(document.body);
		var step_2_events_fired = 0;

		$video.one('loadedmetadata loadeddata suspend', function() {
			if (++step_2_events_fired == 3) {
				$video.one('seeked', function() {

					canvas_elem.height = this.videoHeight;
					canvas_elem.width = this.videoWidth;
					canvas_elem.getContext('2d').drawImage(this, 0, 0);
					var snapshot = canvas_elem.toDataURL();
					
					$.ajax({
						type: "POST",
						url: "./php/saveVideoThumbnail.php",
						/*data: {
								img: snapshot,
								dir: "./files/"+nombreDis+"/CONTENIDOS/thumbnail",
								path: "./files/"+nombreDis+"/CONTENIDOS/thumbnail/"+name,
								size: 30
							  },*/
						data: {
								img: snapshot,
								dir: "./files/"+nombreDis+"/CONTENIDOS/thumbnail",
								path: "./files/"+nombreDis+"/CONTENIDOS/thumbnail/"+name.split(".")[0]+".png",
								size: 100
							  },
						success: function(result) {
					     	console.log(result);
					     	addToVideoList(name);
					    }
					});					

					$video.remove();
					$(canvas_elem).remove();

					//var image = document.createElement("img");
					//var imageParent = document.getElementById("body");
					//image.src = snapshot;
					//imageParent.appendChild(image);

				}).prop('currentTime', 3);
			}
		//}).prop('src', "videos/small.mp4");
		}).prop('src', src);
	}

	var $saved;
	//ADD TO VIDEO LIST
	function addToVideoList(name) {
		//INFO DISPOSITIVO
		var jsonDis = jsonData.dispositivo;
		var codigo = jsonDis.codigo;
		var nombreDis = jsonDis.nombre;

		var $input = $("#nombreINPUT");
		var i = $input.children().length;
		var videoURL = "./php/files/"+nombreDis+"/CONTENIDOS/"+name;



		//lo agregamos al arreglo::
		var dir = "";
		var name = name;
		var date = "Now";
		var hour = "Now";
		var videoURL = videoURL;

		var obj = new Object();
		obj["dir"] = dir;
		obj["name"] = name;
		obj["date"] = date;
		obj["hour"] = hour;
		obj["videoURL"] = videoURL;

		arrayVideos.push(obj);

		//---------------------
		//Agregamos nuevo elemento la lista de videos
		//newVideoListElement(i, name, "now", "now", videoURL, "reverse");		

		setTimeout(function() {
		   showPageTo(1, "descendente");
		}, 150);
	}
	</script>
	
	<script>
	//grilla a lista / viceversa//
	$('div#btnListVideos').on('click', function(){
		 console.log("btlist");
		 toList();
	});

	$('div#btnGridVideos').on('click', function(){  
		 console.log("btgrid");
		 toGrid(); 
	});

	function toList() {
		visualList = "list";
		console.log("visualList",visualList);
		$('.videoThumbnail').addClass('Block');
		$('.videoThumbnail').removeClass('inlineBlock');
		$('.videoThumbnail').addClass('videoThumbnailList');
		$('.labelDate').removeClass('hidden');
		$('.videoThumbnail .checkbox').removeClass('maxWidth');
		$('img[id^="videoThumbnail"]').hide();
	}

	function toGrid() {
		visualList = "grid";
		console.log("visualList",visualList);
		$('.videoThumbnail').addClass('inlineBlock');
		$('.videoThumbnail').removeClass('Block');
		$('.videoThumbnail').removeClass('videoThumbnailList');
		$('.labelDate').addClass('hidden');
		$('.videoThumbnail .checkbox').addClass('maxWidth');
		$('img[id^="videoThumbnail"]').show();
	}

	</script>
</body>
</html>
