<!DOCTYPE html>
<?php 
error_reporting(0);
session_start();

?>
<html>
<head>
	<title>DISPOSITIVO</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!-- No escala -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!--<link rel="icon" href="../../favicon.ico">-->
	<!-- Bootstrap core CSS -->
	<link href="lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

	<!-- Custom styles for this template -->
	<link href="css/style.css" rel="stylesheet">	
	<!--jquery-->
	<script src="lib/jquery/jquery-3.2.1.min.js"></script>	
	<!--overlay-->
	<script src="lib/jquery-loading-overlay-1.5.3/js/loadingoverlay.min.js"></script>
	<script src="lib/jquery-loading-overlay-1.5.3/js/loadingoverlay_progress.min.js"></script>
	<!--pag-->
	<script type='text/javascript' src="js/dispositivo.js"></script>
	<script src="js/jquery.easyPaginate.js"></script>
	<!--controller-->
	
</head>

<body onload="first()">

	<!--NAV BAR-->
	<?php include('navbar.php') ?>
    <div class="nav-separador"></div>   
     <!-- -->   
     <!--LOGO WORKCAFE-->
    <?php include('logobar.php') ?>
  	<!-- -->     
	
	<div class="container">
		<input hidden="true" type="text" name="nomsuc" id="nomsuc" value = <?php echo $_SESSION["sucursal"]["nombre"] ?>> </input>
		<input hidden="true" type="text" name="codsuc" id="codsucu" value = <?php echo $_SESSION["sucursal"]["codigosuc"] ?>> </input>
		<input hidden="true" type="text" name="coddisp" id="coddisp" value = <?php echo $_SESSION["dispositivo"]["codigo"] ?>> </input>
        <input hidden="true" type="text" name="privilegio" id="priv" value = <?php echo $_SESSION["PRIVILEGIOS"]?>> </input>
		<h4 id="tituloDispositivo" class="italic">Dispositivo: <?php echo $_SESSION["dispositivo"]["nombre"] ?></h4>
		<div class="cabecera-separator"></div>
		<br/>
		<div>
			<h4 class="italic">Contenidos:</h4>
			<div class="cabecera-separator"></div>
			<br/>
			<div id="dispositivos">
				<div class="listaCabecera">
					<div class="listaCabecera">
						<div class="row">
							<div class='col-xs-2 col-xs-offset-1'>ID</div>
							<div class='col-xs-4'>Contenido</div>
						</div>
					</div>
				</div>
				<div id="listaContenidos"></div>
			</div>
			<div id="editBtn" style="visibility: hidden">
			<button type="submit" class="btn btn-success" onclick="deleteModal()" >Eliminar</button>
			<button type="submit" class="btn btn-success" onclick="gotoEditarDispositivo()" >Editar</button>
			<button type="submit" class="btn btn-success" onclick="gotoAddContent()" >+Playlist</button>
			</div>
		</div>
	</div>	



	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>
	<script defer>
		function first(){
			setTimeout(function() { paginateDispositivos(); }, 100);
		}

		function paginateDispositivos() {
			console.log("test1");
			$('#listaContainerDispositivos').easyPaginate({
	        paginateElement: 'span',
	        elementsPerPage: 10,
	        effect: 'default',
	        elemId: '#listaContainerDispositivos'
	    	});
		}

		if(document.getElementById("priv").value > 1){
    		document.getElementById("editBtn").style = "visibility: visible";
    	}


    </script>

    <script type="text/javascript">
    	

    	function deleteDisp(){
    		//var checked = document.getElementById("tituloSucursal");
			//console.log('checked', checked);
			var sendData ={};
			sendData = {
				"codDisp": document.getElementById('coddisp').value,
			}

			$.ajax({  
						type:'POST',
						cache:false,
						url: './php/deleteTecnologia.php',
						contentType: "application/x-www-form-urlencoded; charset=UTF-8",
						dataType: 'json',
						data: sendData,
						success: function(result) {
			            	console.log(result);   
			            	//log
			            	//var $idsucursal = document.getElementById('codsucu').value;
			            	//nuevo log
			            	log("deleteDisp", "Delete dispositivo ");

			            	console.log("tecnologia eliminada");   
			            	//modal para redireccionar a algun sitio
			            },
			            error: function(result){
			            	console.log(result);
			            	console.log("error datos");  
			            }

					});
    	}

    	function deleteModal(){
			//var salida = document.getElementById('nomsuc').value;
			//console.log('codsuc', salida);
			$("#myModalPopUpDesicion").modal();
    	}

    	function gotoEditarDispositivo(){
    		window.location.href = 'editarDispositivo';
    	}

    	function gotoAddContent(){
    		window.location.href = 'dispositivo'
    	}

    </script>

   <div id="myModalPopUpDesicion" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Eliminar?</h4>
		      </div>
		      <div class="modal-body">
		        <p>¿Estás seguro que deseas eliminar este elemento?</p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-success" onclick="onClickAccept()">OK</button>
		        <button type="button" class="btn btn-danger" onclick="onClickDiscard()" data-dismiss="modal">Cancel</button>
		      </div>
		    </div>

		  </div>
	</div>

	<div id="myModalDataSuccess" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Mensaje</h4>
		      </div>
		      <div class="modal-body">
		        <p>Tecnologia eliminada</p>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-success" onclick="onClickDataSuccess()">OK</button>
		      </div>
		    </div>

		  </div>
	</div>


	<script type="text/javascript">
		function onClickAccept(){
			deleteDisp();
			$("#myModalDataSuccess").modal();
			//window.location.href = 'administrador';
		}

		function onClickDiscard(){
			window.location.href = 'mainDispositivo';
		}

		function onClickDataSuccess(){
			window.location.href = 'administrador';
		}

		function log(type, evento, detalle = "") {
			$.ajax({
		        type: 'POST',
		        cache: false,
		        url: './php/log.php',
		        contentType: "application/x-www-form-urlencoded; charset=UTF-8",	            
		        data: {"type":type, "evento":evento, "detalle":detalle},
		        success: function(result) {   	
		    		console.log(result);
		        },
		    });
		}
	</script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
</html>
