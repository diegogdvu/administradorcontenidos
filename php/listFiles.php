<?php 
set_time_limit(0);
error_reporting(0);

//header('Content-type: text/plain; charset=utf-8');
date_default_timezone_set('America/Lima');
//date_default_timezone_set('America/Santiago');
//date_default_timezone_set('UTC');

class fileNamexDate{
	public $dir = "";
	public $name = "";
	public $date = "";
	public $hour = "";
	public $videoURL = "";
};

//REPLACE CARACTERES RAROS
function cleanString($str) {
	$caracteres_input = array("Á","É","Í","Ó","Ú","á","é","í","ó","ú",
						"Ä","Ë","Ï","Ö","Ü","ä","ë","ï","ö","ü",
						"Ê","ê","Ñ","ñ");
	$caracteres_output = array("A","E","I","O","U","a","e","i","o","u",
						 "A","E","I","O","U","a","e","i","o","u",
						 "E","e","N","n");

	$string = str_replace($caracteres_input, $caracteres_output, $str);
	return $string;		
}

function getFileList($filter = ""){
	$dir = $_POST["dir"];
	//eliminamos los caracteres extraños (no !"#$%&/()=?¡)
	$dir = cleanString($dir);

	$folder = getcwd()."/files/".$dir."/CONTENIDOS";
	$listFiles = scan_dir($folder);
	$array = array();
	$listFinalFiles = array();
	//print_r($listFiles);
	//exit;
	$ii = 0;
	for($i=0; $i < sizeof($listFiles);$i++) {		
		$aux = new fileNamexDate();		
		$aux->dir = $folder;
		$aux->name = $listFiles[$i];
		$aux->date = date ("F d Y", filemtime($aux->dir.'/'.$aux->name));
		$aux->hour = date ("H:i:s", filemtime($aux->dir.'/'.$aux->name));
		$aux->videoURL = "./php/files/".$dir."/CONTENIDOS/".$aux->name;
		
		if(file_exists($aux->dir.'/'.$aux->name)) {
			if($filter == "" or is_null($filter)) {
				$listFinalFiles[$ii] = $aux;
			}
			else if(strpos($aux->name, $filter) !== false) {
				$listFinalFiles[$ii] = $aux;
			}				
			$ii++;
		}
	}
	$array["videos"] = $listFinalFiles;
	return $array;
}

function scan_dir($dir) {
    $ignored = array('.', '..', '.svn', '.htaccess', '.txt', 'thumbnail');
    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }
    arsort($files);
    $files = array_keys($files);
    return ($files) ? $files : false;
}

echo json_encode(getFileList());
//getFileList();
?>