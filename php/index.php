<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');

// Starting session
session_start();

//REPLACE CARACTERES RAROS
function cleanString($str) {
	$caracteres_input = array("Á","É","Í","Ó","Ú","á","é","í","ó","ú",
						"Ä","Ë","Ï","Ö","Ü","ä","ë","ï","ö","ü",
						"Ê","ê","Ñ","ñ");
	$caracteres_output = array("A","E","I","O","U","a","e","i","o","u",
						 "A","E","I","O","U","a","e","i","o","u",
						 "E","e","N","n");
	/*$caracteres_output = array("&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;",
						 "&aacute;","&eacute;","&iacute;","&oacute;","&uacute;",
						 "&Auml;","&Euml;","&Iuml;","&Ouml;","&Uuml;",
						 "&auml;","&euml;","&iuml;","&ouml;","&uuml;",
						 "&Ecirc;","&ecirc;","&Ntilde;","&ntilde;");*/

	$string = str_replace($caracteres_input, $caracteres_output, $str);
	return $string;		
}

//Variables de "SUCURSAL"
$sucursal_id = $_SESSION["sucursal"]["codigosuc"];
$sucursal_nombre = $_SESSION["sucursal"]["nombre"];
//Variables de "DISPOSITIVO" 
$dispositivo_id = $_SESSION["dispositivo"]["codigo"];
$dispositivo_nombre = $_SESSION["dispositivo"]["nombre"]; 
//Variable "OPCION"
//$opcion = $_SESSION["dispositivo"]["opcion"]; 
//Variable de FOLDER estatica
$folder = "CONTENIDOS";

//$upload_handler = new UploadHandler();
//$options = array('upload_dir'=>'files/'.$dispositivo_nombre.'/'.$sucursal_nombre.'/', 'upload_url'=>'php/files/'.$dispositivo_nombre.'/'.$sucursal_nombre.'/');
$upload_dir = cleanString('files/'.$dispositivo_nombre.'/'.$folder .'/');
$upload_url = cleanString('php/files/'.$dispositivo_nombre.'/'.$folder .'/');
$options = array('upload_dir'=>$upload_dir, 'upload_url'=>$upload_url);
$upload_handler = new UploadHandler($options);
