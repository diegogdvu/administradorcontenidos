<?php
// Starting session
session_start();

// Storing session data
$_SESSION["firstname"] = "Peter";
$_SESSION["lastname"] = "Parker";

// Accessing session data
echo 'Hi, ' . $_SESSION["firstname"] . ' ' . $_SESSION["lastname"];

// Removing session data
/*if(isset($_SESSION["lastname"])){
    unset($_SESSION["lastname"]);
}*/

// Destroying session
//session_destroy();

?>