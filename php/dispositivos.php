<?php 
error_reporting(0);
include('database.php');

getDispositivos($conn);

function getDispositivos($conn) {        
	$sql = "SELECT * FROM dispositivo WHERE STATUS = 'ACTIVO'";
	$result = $conn->query($sql);
	
	if ($result->num_rows > 0) {
		$i = 0;
		// output data of each row
		while($row = $result->fetch_assoc()) {
			$array["dispositivos"][$i]["codigo"] = $row["codigo"];
			$array["dispositivos"][$i]["nombre"] = $row["nombre"];
			$array["dispositivos"][$i]["detalles"] = $row["detalles"];
			$array["dispositivos"][$i]["playlist"] = $row["playlist"];
			$array["dispositivos"][$i]["timestamp"] = $row["timestamp"];
			$array["dispositivos"][$i]["STATUS"] = $row["STATUS"];
			$array["dispositivos"][$i]["contenidos"] = getContenidosXDispositivo($conn, $row["codigo"]);
			$i++;					
		}		
		//print_r($array);
		echo json_encode($array);
	}    
}

function getContenidosXDispositivo($conn, $idDis) {  
	$sql = "SELECT contenido.idContenido, contenido.Nombre, contenido.Estado
			FROM contenido
			INNER JOIN dispositivo_has_contenido
			ON contenido.idContenido = dispositivo_has_contenido.ID_CONTENIDO
			WHERE dispositivo_has_contenido.ID_DISPOSITIVO = ". $idDis;
			//echo $sql;
	$result = $conn->query($sql);
	
	if ($result->num_rows > 0) {
		$i = 0;
		// output data of each row
		while($row = $result->fetch_assoc()) {
			$subArray[$i]["idContenido"] = $row["idContenido"];
			$subArray[$i]["Nombre"] = $row["Nombre"];
			$subArray[$i]["Estado"] = $row["Estado"];
			$i++;	
		}		
		//print_r($array);
		return $subArray;
	}  
}

?>