<?php
	
	$img64 = $_POST["img"];
	$path = $_POST["path"];
	$resize = $_POST["size"];

	$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img64));
	file_put_contents($path, $data);

	//RESIZE
	list($width, $height, $type) = getimagesize($path);

	if($type == IMAGETYPE_JPEG){
		$src = imagecreatefromjpeg($path);
	}
	else if ($type == IMAGETYPE_PNG){
		$src = imagecreatefrompng($path);
	}

	$x = $width * $resize / 100;
	$y = $height * $resize / 100;

	$dest = imagecreatetruecolor($x, $y);
	//echo $dest;

	imagecopyresized ($dest, $src, 0, 0, 0, 0, $x, $y, $width, $height);
	imagepng($dest, $path);

	echo "Video Thumbnail Saved";

?>

