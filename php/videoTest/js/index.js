
function onload(){

	var canvas_elem = $( '<canvas class="snapshot-generator"></canvas>' ).appendTo(document.body)[0];
	var $video = $( '<video muted class="snapshot-generator"></video>' ).appendTo(document.body);

	var step_2_events_fired = 0;

	$video.one('loadedmetadata loadeddata suspend', function() {

		if (++step_2_events_fired == 3) {

			$video.one('seeked', function() {

				canvas_elem.height = this.videoHeight;
				canvas_elem.width = this.videoWidth;
				canvas_elem.getContext('2d').drawImage(this, 0, 0);
				var snapshot = canvas_elem.toDataURL();

				
				$.ajax({
					type: "POST",
					url: "saveImage.php",
					data: {
							img: snapshot,
							path: "img.png",
							size: 30
						  },
					success: function(result) {
				     	console.log(result);
				    }
				});
				

				$video.remove();
				$(canvas_elem).remove();


				//console.log(snapshot);

				var image = document.createElement("img");
				var imageParent = document.getElementById("body");

				image.src = snapshot;

				imageParent.appendChild(image);

			}).prop('currentTime', 3);

		}

	}).prop('src', "videos/small.mp4");
}