<?php
error_reporting(0);
include('database.php');

//debug

$debug = false;

if($debug) {
	$_METHOD = $_GET;
}
else {
	$_METHOD = $_POST;
}

//echo "user id " . $id_user;

$sql = "SELECT idContenido, NOMBRE FROM contenido";

$result = $conn->query($sql);
$array =[];

if ($result->num_rows > 0) {
	$i = 0;
	//echo "select realizado ";
	while($row = $result->fetch_assoc()) {
		$name = $row["NOMBRE"];
		$id = $row["idContenido"];
		$subarray = [];
		$subarray[0]=$id;
		$subarray[1]=$name;
		$array[$i]=$subarray;
		$i++;
	}
	echo json_encode($array);
}
else {
	//no rows on select
    echo "Error: " . $sql . "<br>" . $conn->error . $id_user;
}

$conn->close();

?>