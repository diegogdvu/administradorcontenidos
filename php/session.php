<?php
error_reporting(0);
// Starting session
session_start();

$type = $_POST["type"];
$dataKey = $_POST["dataKey"];
$dataValue = $_POST["dataValue"];

if($type == "set") {
	// Storing session data
	$_SESSION["type"] = $type;
	$_SESSION[$dataKey] = $dataValue;
	echo '{"status":"OK"}';
}
else if($type == "get") {
	echo json_encode($_SESSION);
}
else {
	echo '{"status":"ERROR"}';
}

// Removing session data
/*if(isset($_SESSION["lastname"])){
    unset($_SESSION["lastname"]);
}*/

// Destroying session
//session_destroy();

?>