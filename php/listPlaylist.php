<?php 

set_time_limit(0);
error_reporting(0);

//header('Content-type: text/plain; charset=utf-8');
date_default_timezone_set('America/Lima');
//date_default_timezone_set('America/Santiago');
//date_default_timezone_set('UTC');

//REPLACE CARACTERES RAROS
function cleanString($str) {
	$caracteres_input = array("Á","É","Í","Ó","Ú","á","é","í","ó","ú",
						"Ä","Ë","Ï","Ö","Ü","ä","ë","ï","ö","ü",
						"Ê","ê","Ñ","ñ");
	$caracteres_output = array("A","E","I","O","U","a","e","i","o","u",
						 "A","E","I","O","U","a","e","i","o","u",
						 "E","e","N","n");

	$string = str_replace($caracteres_input, $caracteres_output, $str);
	return $string;		
}

function getFileList($filter = ""){
	$dir = $_POST["dir"];
	//eliminamos los caracteres extraños (no !"#$%&/()=?¡)
	$dir = cleanString($dir);

	$folder = getcwd()."/files/".$dir."/PREDETERMINADO";
	$listFiles = scan_dir($folder);
	$array = array();
	$listFinalFiles = array();
	return $listFiles;

}

function scan_dir($dir) {
    $ignored = array('.', '..', '.svn', '.htaccess', '.txt', 'thumbnail');
    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }
    arsort($files);
    $files = array_keys($files);
    return ($files) ? $files : false;
}


echo json_encode(getFileList());
//getFileList();
?>