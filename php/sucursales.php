<?php 
error_reporting(0);
include('database.php');

getSucursales($conn);

function getSucursales($conn) {        
	$sql = "SELECT * FROM sucursal";
	$result = $conn->query($sql);
	
	if ($result->num_rows > 0) {
		$i = 0;
		// output data of each row
		while($row = $result->fetch_assoc()) {
			$array["sucursales"][$i]["codigosuc"] = $row["codigosuc"];
			$array["sucursales"][$i]["nombre"] = $row["nombre"];
			$array["sucursales"][$i]["pais"] = $row["pais"];
			$array["sucursales"][$i]["estado"] = $row["estado"];
			$array["sucursales"][$i]["dispositivos"] = getDispositivosXSucursal($conn, $row["codigosuc"]);
			$i++;			
			//json_encode($row);		
			//echo json_encode($row);
			//echo json_encode($row, JSON_PRETTY_PRINT)
			
		}		
		//print_r($array);
		echo json_encode($array);
	} 

	$conn->close();
}

function getDispositivosXSucursal($conn, $idSuc) {  
	/*$sql = "SELECT dispositivo.codigo AS codigo, dispositivo.nombre AS nombre, 
				   dispositivo.detalles AS detalles, dispositivo.playlist AS playlist, 
				   dispositivo.timestamp AS timestamp, dispositivo.STATUS AS STATUS
			FROM dispositivo, sucursal_has_dispositivo 
			WHERE SUCURSAL_IDSUCURSAL = " .$idSuc;*/
			//echo $sql;
	$sql = "SELECT dispositivo.codigo AS codigo, dispositivo.nombre AS nombre, 
				   dispositivo.detalles AS detalles, dispositivo.playlist AS playlist, 
				   dispositivo.timestamp AS timestamp, dispositivo.STATUS AS STATUS
			FROM dispositivo
			INNER JOIN sucursal_has_dispositivo
			ON dispositivo.codigo = sucursal_has_dispositivo.ID_DISPOSITIVO
			WHERE sucursal_has_dispositivo.ID_SUCURSAL = " .$idSuc."
			AND STATUS = 'ACTIVO'" ;

	$result = $conn->query($sql);
	
	if ($result->num_rows > 0) {
		$i = 0;
		// output data of each row
		while($row = $result->fetch_assoc()) {
			$subArray[$i]["codigo"] = $row["codigo"];
			$subArray[$i]["nombre"] = $row["nombre"];
			$subArray[$i]["detalles"] = $row["detalles"];
			$subArray[$i]["playlist"] = $row["playlist"];
			$subArray[$i]["timestamp"] = $row["timestamp"];
			$subArray[$i]["STATUS"] = $row["STATUS"];
			$subArray[$i]["contenidos"] = getContenidosXDispositivo($conn, $row["codigo"]);
			$i++;	
		}		
		//print_r($array);
		return $subArray;
	}  
}

function getContenidosXDispositivo($conn, $idDis) {  
	$sql = "SELECT contenido.idContenido, contenido.Nombre, contenido.Estado
			FROM contenido
			INNER JOIN dispositivo_has_contenido
			ON contenido.idContenido = dispositivo_has_contenido.ID_CONTENIDO
			WHERE dispositivo_has_contenido.ID_DISPOSITIVO = ". $idDis;
			//echo $sql;
	$result = $conn->query($sql);
	
	if ($result->num_rows > 0) {
		$i = 0;
		// output data of each row
		while($row = $result->fetch_assoc()) {
			$subArray[$i]["idContenido"] = $row["idContenido"];
			$subArray[$i]["Nombre"] = $row["Nombre"];
			$subArray[$i]["Estado"] = $row["Estado"];
			$i++;	
		}		
		//print_r($array);
		return $subArray;
	}  
}
?>
