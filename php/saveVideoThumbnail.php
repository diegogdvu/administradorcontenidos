<?php
set_time_limit(0);
error_reporting(0);

//REPLACE CARACTERES RAROS
function cleanString($str) {
	$caracteres_input = array("Á","É","Í","Ó","Ú","á","é","í","ó","ú",
						"Ä","Ë","Ï","Ö","Ü","ä","ë","ï","ö","ü",
						"Ê","ê","Ñ","ñ");
	$caracteres_output = array("A","E","I","O","U","a","e","i","o","u",
						 "A","E","I","O","U","a","e","i","o","u",
						 "E","e","N","n");
	$string = str_replace($caracteres_input, $caracteres_output, $str);
	return $string;		
}

//CREATE DIR
function create_dir($path) {
	$path = cleanString($path);
	if(!is_dir($path)) { 
	   mkdir($path, 0755, true);
	}
	return true;
}

start();

function start() {
	$img64 = $_POST["img"];
	$dir = $_POST["dir"];
	$path = $_POST["path"];
	$resize = $_POST["size"];	
	
	//eliminamos los caracteres extraños (no !"#$%&/()=?¡)
	$path = cleanString($path);

	//Directorio / Folder
	create_dir($dir);

	$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img64));
	file_put_contents($path, $data);

	//RESIZE
	list($width, $height, $type) = getimagesize($path);

	if($type == IMAGETYPE_JPEG){
		$src = imagecreatefromjpeg($path);
	}
	else if ($type == IMAGETYPE_PNG){
		$src = imagecreatefrompng($path);
	}

	$x = $width * $resize / 100;
	$y = $height * $resize / 100;

	$dest = imagecreatetruecolor($x, $y);
	//echo $dest;

	imagecopyresized ($dest, $src, 0, 0, 0, 0, $x, $y, $width, $height);
	imagepng($dest, $path);

	echo "Video Thumbnail Saved <br/>";
}

?>

