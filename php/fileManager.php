<?php
set_time_limit(0);
error_reporting(0);

//REPLACE CARACTERES RAROS
function cleanString($str) {
	$caracteres_input = array("Á","É","Í","Ó","Ú","á","é","í","ó","ú",
						"Ä","Ë","Ï","Ö","Ü","ä","ë","ï","ö","ü",
						"Ê","ê","Ñ","ñ");
	$caracteres_output = array("A","E","I","O","U","a","e","i","o","u",
						 "A","E","I","O","U","a","e","i","o","u",
						 "E","e","N","n");
	/*$caracteres_output = array("&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;",
						 "&aacute;","&eacute;","&iacute;","&oacute;","&uacute;",
						 "&Auml;","&Euml;","&Iuml;","&Ouml;","&Uuml;",
						 "&auml;","&euml;","&iuml;","&ouml;","&uuml;",
						 "&Ecirc;","&ecirc;","&Ntilde;","&ntilde;");*/

	$string = str_replace($caracteres_input, $caracteres_output, $str);
	return $string;		
}

//CREATE DIR
function create_dir($path) {
	$path = cleanString($path);
	if(!is_dir($path)) { 
	   mkdir($path, 0755, true);
	}
	return true;
}
//CREATE
function create_file($dir, $path, $content){	
	create_dir($dir);
	$path = cleanString($path);
	$myfile = fopen($path, "w") or die("Unable to create the file ".$path);				
	fwrite($myfile, $content);
	fclose($myfile);

	echo json_encode("File created!");
	exit;
}

function batch_create_file($dirList, $pathList, $content){		
	$num = count($pathList);
	
	for($i = 0; $i < $num; $i++){	
		create_dir($dirList[$i]);
		$path = cleanString($pathList[$i]);
		$myfile = fopen($path, "w") or die("Unable to create the file ".$path);			
		fwrite($myfile, $content);
		fclose($myfile);			
	}
	
	echo json_encode("Files created!");		
	exit;
}	
//APPEND
function append_file($dir, $path, $content){		
	$myfile = fopen($path, "a") or die("Unable to append the file ".$path);
	
	fwrite($myfile, $content);
	fclose($myfile);

	echo json_encode("File modified!");
	exit;
}
//RENAME
function rename_file($dir, $path, $newPath){
	rename($path, $newPath);

	echo json_encode("File renamed!");
	exit;
}
//COPY
function copy_file($dir, $path, $newPath){
	create_dir($dir);
	$path = cleanString($path);
	$newPath = cleanString($newPath);
	if (!copy($path, $newPath)) {
		echo "Error copying file ".$path." to ".$newPath." \n";
		exit;
	}

	echo json_encode("File copied!");
	exit;
}

function batch_copy_file($dirList, $pathList, $newPathList){		
	$num = count($pathList);
	
	for($i = 0; $i < $num; $i++){	
		create_dir($dirList[$i]);
		$path = cleanString($pathList[$i]);
		$newPath = cleanString($newPathList[$i]);
		if (!copy($path, $newPath)) {				
			echo "Error copying file ".$path." to ".$newPath." \n";
		}
	}		

	echo json_encode("Files copied!");
	exit;
}

//REMOVE
function batch_remove_file($pathList){		
	$num = count($pathList);
	
	for($i = 0; $i < $num; $i++){	
		$path = cleanString($pathList[$i]);
		if (is_file($path)) {
			unlink($path);
		}
		else {
			echo "Error removing file ".$path." \n";
		}
	}		

	echo json_encode("Files removed!");
	exit;
}

function remove_file($path){		
	

	$path = cleanString($path);
	if (is_file($path)) {
		unlink($path);
	}
		else {
		echo "Error removing file ".$path." \n";
	}
	
	echo json_encode("Files removed!");
	exit;
}


//ENTRADA DE PARAMETROS //$_GET or $_POST
$_PARAM = $_POST; 

if(isset($_PARAM["type"])){
	$type = $_PARAM["type"];
	
	if($type == "new"){	
		$dir = $_PARAM["dir"];
		$path = $_PARAM["path"];
		$content = $_PARAM["content"];	
		if(isset($path) && isset($content)){							
			create_file($dir, $path, $content);
		}
		else{
			echo "'new' Missing parameters";
		}			
	}		
	else if($type == "batchNew"){		
		$dirList = $_PARAM["dirList"];
		$pathList = $_PARAM["pathList"];
		$content = $_PARAM["content"];
		if(isset($pathList) && isset($content)){				
			batch_create_file($dirList, $pathList, $content);
		}
		else{
			echo "'batchNew' Missing parameters";
		}			
	}		
	else if($type == "append"){	
		$dir = $_PARAM["dir"];
		$path = $_PARAM["path"];
		$content = $_PARAM["content"];		
		if(isset($path) && isset($content)){				
			append_file($dir, $path, $content);			
		}
		else{
			echo "'append' Missing parameters";
		}
	}		
	else if($type == "rename"){
		$dir = $_PARAM["dir"];
		$path = $_PARAM["path"];
		$newPath = $_PARAM["newPath"];
		if(isset($path) && isset($newPath)){				
			rename_file($dir, $path, $newPath);		
		}
		else{
			echo "'rename' Missing parameters";
		}
	}		
	else if($type == "copy"){	
		$dir = $_PARAM["dir"];
		$path = $_PARAM["path"];
		$newPath = $_PARAM["newPath"];
		if(isset($path) && isset($newPath)){				
			copy_file($dir, $path, $newPath);
		}
		else{
			echo "'copy' Missing parameters";
		}
	}		
	else if($type == "batchCopy"){
		$dirList = $_PARAM["dirList"];
		$pathList = $_PARAM["pathList"];		
		$newPathList = $_PARAM["newPathList"];		
		if(isset($pathList) && isset($newPathList)){				
			batch_copy_file($dirList, $pathList, $newPathList);				
		}
		else{
			echo "'batchCopy' Missing parameters";
		}
	}
	else if($type == "batchRemove"){
			$pathList = $_PARAM["pathList"];	
			if(isset($pathList)){				
				batch_remove_file($pathList);				
			}
			else{
				echo "'batchRemove' Missing parameters";
			}
	}	
	else if($type == "remove"){
			$path = $_PARAM["path"];	
			if(isset($path)){				
				remove_file($path);				
			}
			else{
				echo "'Remove' Missing parameters";
			}
	}	
	else{
		echo "'type' not recognized";
	}
}
else{
	echo "'type' is not set";
}
?>
